<?php btn_add_action('CATEVAL'); ?>
<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des catégories d'évaluations</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th style="...">Code categorie évaluation</th>
                        <th style="...">Libellé catégorie évaluation</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>
                            <td><?php echo $value->code_categorie_eval; ?></td>
                            <td><?php echo $value->libelle_categorie_evaluation; ?></td>

                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->id_categorie_evaluation, 'CATEVAL'); ?>
                                <?php btn_delete_action($value->id_categorie_evaluation, 'CATEVAL'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="id_categorie_evaluation" name="id_categorie_evaluation"/>

                    <div class="form-group">
                        <label class="control-label col-md-3">Code categorie évaluation <i
                                class='text-danger'>*</i></label>

                        <div class="col-md-9">
                            <label for="code_categorie_eval" class="field prepend-icon">
                                <input type="text" id="code_categorie_eval" name="code_categorie_eval"/>
                            </label>
                        </div>

                    </div>

                    <div class="form-body  admin-form">

                        <div class="form-group">
                            <label class="control-label col-md-3">Libelle categorie èvaluation<i
                                    class='text-danger'>*</i></label>

                            <div class="col-md-9">
                                <label for="libelle_categorie_evaluation" class="field prepend-icon">
                                    <input type="text" name="libelle_categorie_evaluation"
                                           id="libelle_categorie_evaluation"
                                           class="form-control gui-input"/>
                                </label>
                            </div>

                        </div>


                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>
        <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</form>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire
            // id_view_form: 'modal_view', //id du modal qui contient le formulaire

            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('C_categorie_evaluation/save')?>", //url du save (donn?es envoy?s par post)
            // id_show: 'fonction_test',
            title_modal_add: 'Nouvelle catégorie d\'évaluation', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'code_categorie_eval', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition de la catégorie d\'évaluation', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'code_categorie_eval',//l'emplacement du focus en mode edit
            //colonne_show: 'libelle_fonction', //la colonne a affciher lorsqu'on regarde une categorie

            // title_modal_show: 'Visualisation de fonctions', //Title du modal ? l'ouverture en mode visualisation
            //focus_show: 'libelle_annee',//l'emplacement du focus en mode edit

            url_edit: "<?php echo site_url('C_categorie_evaluation/get_record')?>", //url le fonction qui recup?re la donn?es de la ligne
            url_delete: "<?php echo site_url('C_categorie_evaluation/delete')?>", //url de la fonction supprim?
            url_show: "<?php echo site_url('C_categorie_evaluation/show')?>", //url de la fonction visualiser?
        });
    });
</script>
