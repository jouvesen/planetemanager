<?php btn_add_action(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des structures</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>

                        <th>Libell� structure</th>
                        <th>date_creation</th>
                        <th>email</th>
                        <th>tel</th>
                        <th>date_fermeture</th>
                        <th>arrete_ferme</th>
                        <th>date_insert</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>

                            <td><?php echo $value->libelle_structure; ?></td>
                            <td><?php echo $value->date_creation_str; ?></td>
                            <td><?php echo $value->email_str; ?></td>
                            <td><?php echo $value->tel_str; ?></td>
                            <td><?php echo $value->date_fermeture; ?></td>
                            <td><?php echo $value->arrete_ferme; ?></td>
                            <td><?php echo $value->date_insert; ?></td>
                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->code_str); ?>
                                <?php btn_delete_action($value->code_str); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="code_grade" name="code_grade"/>
                    <div class="form-body  admin-form">
                        <div class="form-group">
                            <label class="control-label col-md-3">Libell� structure <i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="libelle_structure" class="field prepend-icon">
                                    <input type="text" name="libelle_structure" id="libelle_structure"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">date_creation </label>

                            <div class="col-md-9">
                                <label for="date_creation" class="field prepend-icon">
                                    <input  type="text" name="date_creation_str" id="date_creation_str"
                                              class="form-control gui-input">
                                </label>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">email </label>

                            <div class="col-md-9">
                                <label for="email_str" class="field prepend-icon">
                                    <input  type="text" name="email_str" id="email_str"
                                              class="form-control gui-input">
                                </label>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tel </label>

                            <div class="col-md-9">
                                <label for="tel_str" class="field prepend-icon">
                                    <input  type="text" name="tel_str" id="tel_str"
                                              class="form-control gui-input">
                                </label>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Date fermeture </label>

                            <div class="col-md-9">
                                <label for="date_fermeture" class="field prepend-icon">
                                    <input  type="text" name="date_fermeture" id="date_fermeture"
                                              class="form-control gui-input">
                                </label>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Arret ferme </label>

                            <div class="col-md-9">
                                <label for="arrete_ferme" class="field prepend-icon">
                                    <input  type="text" name="arrete_ferme" id="arrete_ferme"
                                            class="form-control gui-input">
                                </label>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Date insert </label>

                            <div class="col-md-9">
                                <label for="date_insert" class="field prepend-icon">
                                    <input  type="text" name="date_insert" id="date_insert"
                                            class="form-control gui-input">
                                </label>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->

<div id="modal_view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="view" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">

                    <div class="form-body  admin-form">

                        <label class="control-label col-md-3">Corps</label>

                        <div class="col-md-9">
                            <label for="Corps" class="field prepend-icon">
                                <ul id="grade_ver">
                                </ul>
                            </label>
                        </div>
                    </div>
                    <div class="form-body  admin-form">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>

            </div>

    </form>

</div>


<script type="text/javascript">


    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire
            id_view_form: 'modal_view', //id du modal qui contient le formulaire

            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('C_structure/save')?>", //url du save (donn?es envoy?s par post)

            title_modal_add: 'Nouveau ', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'libelle_structure', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition ', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'libelle_structure',//l'emplacement du focus en mode edit
            focus_show: 'libelle_structure',//l'emplacement du focus en mode edit

            url_edit: "<?php echo site_url('C_structure/get_record')?>", //url le fonction qui recup?re la donn?es de la ligne
            url_delete: "<?php echo site_url('C_structure/delete')?>", //url de la fonction supprim?
        });

        
    });

</script>
