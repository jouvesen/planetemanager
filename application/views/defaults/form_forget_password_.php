<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>R&eacute;initialisation mot de passe</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(''); ?>assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(''); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(''); ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url(''); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(''); ?>assets/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo" style="color:#FFFF00;">
			<a href="javascript:void(0);">FCL | R&eacute;initialisation mot de passe</a>
		</div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" >				
                    <div class="msg">
						Adresse e-mail de r&eacute;cup&eacute;ration
					</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" id="email_address" name="email_address" placeholder="E-mail" autofocus />
							<?php echo form_error('email_address');	?>
                        </div>
                    </div>
                    
                    <div class="row">						
                        <div class="col-xs-12">
							<input class="btn btn-block bg-pink waves-effect" type="submit" value=" Valider ">
                        </div>
                    </div>
					
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(''); ?>assets/js/admin.js"></script>
    <script src="<?php echo base_url(''); ?>assets/js/pages/examples/sign-in.js"></script>
</body>

</html>