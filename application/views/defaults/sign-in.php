<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | FCL | Dotations Collectivités</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(''); ?>assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">	

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(''); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(''); ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url(''); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(''); ?>assets/css/style.css" rel="stylesheet">
	
	<!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
	
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo" style="color:#FFFF00;">
            <a href="javascript:void(0);">  FCL | Dotations</a></div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="<?php echo site_url() ?>/verif-connexion">
				
						   <?php 
								//$attributes = array('name' => 'form_add_bloc',
								//					'id'   => 'form_add_bloc' );
								//echo form_open('', $attributes); 
							?>
				
                    <div class="msg">Connectez-vous pour commencer votre session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" id="username" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                        </div>
                    </div>
					<!-- CAPTCHA 
					<?php echo form_error('g-recaptcha-response','<div style="color:red;">','</div>'); ?>
					<div class="g-recaptcha" data-sitekey="6LeelCsUAAAAACiqSmSVuhIjYX3ee1Kn22kh4j4D">
						
					</div>-->
                    <!-- <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Se souvenir  de moi</label>
                        </div>
						
					</div> -->
					<p>&nbsp;</p>
					
					
                    <div class="row">
						
                        <div class="col-xs-12">
							<input class="btn btn-block bg-pink waves-effect" type="submit" value=" se connecter ">
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
<!--                        <div class="col-xs-6">    
			            <?php echo anchor('sign-up', 'Inscrire maintenant!',' title="Register Now!" id="" class=""');?>
			               </a>
                        </div>-->
                        <div class="col-xs-6 align-right">
							<a>
			            <?php echo anchor('forgot-password', 'Mot de passe oublié?',' title="Forgot Password?" id="" class=""');?>
			               </a>
                        </div>
                    </div>
					<?php
					if(isset($_GET['erreur']) && ($_GET['erreur'] == "login")) 
				    {  // Affiche l'erreur
				    ?>
					    <div class="alert alert-warning" role="alert">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Erreur!</strong> Login ou mot de passe incorrect!!
						</div>
					<?php 
					}   
					?>
					<?php
					if(isset($_GET['erreur']) && ($_GET['erreur'] == "profil")) 
				    {  // Affiche l'erreur
				    ?>
					    <div class="alert alert-danger" role="alert">
						  <strong>Danger!</strong>Votre profil n'est pas autorisé!!
						</div>
					<?php 
					}   
					?>


					<?php
					if(@$more_than_3>3) 
				    {  // Affiche l'erreur
				    ?>
					    <div class="alert alert-danger" role="alert">
						  <strong>Danger!</strong> Tentative robot(plus de 5 connexions)!
						</div>
					    <?php 
					}   
					?>
					
					
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(''); ?>assets/js/admin.js"></script>
    <script src="<?php echo base_url(''); ?>assets/js/pages/examples/sign-in.js"></script>
</body>

</html>