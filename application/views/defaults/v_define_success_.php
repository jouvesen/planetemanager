<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>D&eacute;finition de mot de passe r&eacute;ussie</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(''); ?>assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(''); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(''); ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url(''); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(''); ?>assets/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo" style="color:#FFFF00;">
			<a href="javascript:void(0);">FCL | D&eacute;finition mot de passe</a>
		</div>
        <div class="card">
            <div class="body">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-12">
					<div class="body bg-cyan">
						<p>Votre mot de passe a &eacute;t&eacute; d&eacute;fini avec succ&egrave;s !!!</p>
						<p>Vous pouvez maintenant acc&eacute;der aux fonctionnalit&eacute; de la plateforme lors de votre prochaine connexion !!!</p>
						<p>Vous serez d&eacute;connect&eacute; sous peu !!! Merci de bien vouloir  patienter...</p>
					</div>
					
				</div>
				<?php 
					//refresh the page after 5 seconds								
					header('Refresh: 10; URL=se_deconnecter');								 
				?>
				
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(''); ?>assets/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(''); ?>assets/js/admin.js"></script>
    <script src="<?php echo base_url(''); ?>assets/js/pages/examples/sign-in.js"></script>
</body>

</html>