<?php btn_add_action('GRADE'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des grades</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>

                        <th style="width: 90%">Libellé grade</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>

                        <td><?php echo $value->nom_grade; ?></td>
                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->code_grade, 'GRADE'); ?>
                                <?php btn_delete_action($value->code_grade, 'GRADE'); ?>
                                <?php btn_show_action($value->code_grade, 'GRADE'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="code_grade" name="code_grade"/>
                    <div class="form-body  admin-form">
                        <div class="form-group">
                            <label class="control-label col-md-3">Libellé grade <i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="nom_grade" class="field prepend-icon">
                                    <input type="text" name="nom_grade" id="nom_grade"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Description </label>

                            <div class="col-md-9">
                                <label for="description" class="field prepend-icon">
                                    <textarea type="text" name="description" id="description"
                                              class="form-control gui-input"> </textarea>
                                </label>

                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">

                                <table style="width: 100%" id="datatable-Corps" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <td>corps</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($all_data_corps as $value_corps) { ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="<?php echo $value_corps->code_corps; ?>"
                                                       id="<?php echo $value_corps->code_corps; ?>"
                                                       value="<?php echo $value_corps->code_corps; ?>"><?php echo $value_corps->nom_corps; ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>

                                </table>
                            </div>

                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->

<div id="modal_view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="view" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">

                    <div class="form-body  admin-form">

                        <label class="control-label col-md-3">Corps</label>

                        <div class="col-md-9">
                            <label for="Corps" class="field prepend-icon">
                                <ul id="grade_ver">
                                </ul>
                            </label>
                        </div>
                    </div>
                    <div class="form-body  admin-form">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>

            </div>

    </form>

</div>


<script type="text/javascript">


        $(document).ready(function () {
            $('#datatable-buttons').managing_ajax({
                id_modal_form: 'modal_form', //id du modal qui contient le formulaire
                id_view_form: 'modal_view', //id du modal qui contient le formulaire

                id_form: 'form', //id du formulaire
                url_submit: "<?php echo site_url('C_grade/save')?>", //url du save (donn�es envoy�s par post)
                id_show: 'grade_ver',

                title_modal_add: 'Nouveau ', //Title du modal � l'ouverture en mode ajout
                focus_add: 'nom_grade', //l'emplacement du focus en mode ajout

                title_modal_edit: 'Edition ', //Title du modal � l'ouverture en mode edit
                focus_edit: 'nom_grade',//l'emplacement du focus en mode edit
                colonne_show: 'nom_corps', //la colonne a affciher lorsqu'on regarde une categorie
                title_modal_show: 'Visualisation des corps', //Title du modal ? l'ouverture en mode visualisation
                focus_show: 'nom_grade',//l'emplacement du focus en mode edit

                url_edit: "<?php echo site_url('C_grade/get_record')?>", //url le fonction qui recupére la données de la ligne
                url_delete: "<?php echo site_url('C_grade/delete')?>", //url de la fonction supprimé
                url_show: "<?php echo site_url('C_grade/show')?>", //url de la fonction supprimé
            });


            $('#datatable-Corps').DataTable({
                searching: false,
                //"bInfo" : false ,
                //"bLengthChange" : false,
                language: langageFrDataTable()
            });

        });

</script>
