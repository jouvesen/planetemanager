<?php btn_add_action('SECTION'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des sections</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th style="width: 90%">Code section</th>
                        <th style="width: 90%">Libelle Section</th>
                        <th style="...">Effectif Enseignant</th>
                        <th style="...">Ordre Section</th>
                        <th style="width: 90%">Nom Cycle</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>
                            <td><?php echo $value->codesection; ?></td>
                            <td><?php echo $value->libelle_section; ?></td>
                            <td><?php echo $value->effectif_enseignant; ?></td>
                            <td><?php echo $value->ordre_section; ?></td>
                            <td><?php foreach ($all_data_cycle as $value_cycle) {
                                    if ($value->code_cycle == $value_cycle->code_cycle) {
                                        echo $value_cycle->nom_cycle;
                                    }
                                } ?></td>


                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php echo btn_edit_action($value->code_section, 'SECTION'); ?>
                                <?php echo btn_delete_action($value->code_section, 'SECTION'); ?>
                                <?php echo btn_show_action($value->code_section); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id=" code_section" name="section_code" value=" "/>
                    <div class="form-body  admin-form">

                        <div class="form-group">
                            <label class="control-label col-md-3">Libelle Section <i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="libelle_section" class="field prepend-icon">
                                    <input type="text" name="libelle_section" id="libelle_section"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Code Section</label>

                            <div class="col-md-9">
                                <label for="codesection" class="field prepend-icon">
                                    <input type="text" name="codesection" id="codesection"
                                           class="form-control gui-input">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Effectif Enseignant</label>

                            <div class="col-md-9">
                                <label for="effectif_enseignant" class="field prepend-icon">
                                    <input type="text" name="effectif_enseignant" id="effectif_enseignant"
                                           class="form-control gui-input">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Ordre Section</label>
                            <div class="col-md-9">
                                <label for="ordre_section" class="field prepend-icon">
                                    <input type="text" name="ordre_section" id="ordre_section"
                                           class="form-control gui-input">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Cycle </label>

                            <div class="col-md-9">

                                <label for="code_cycle" class="field prepend-icon">
                                    <select name="code_cycle" id="code_cycle"
                                            class="form-control gui-input">
                                        <?php foreach ($all_data_cycle as $value_cycle) { ?>
                                            <option value="<?php echo $value_cycle->code_cycle; ?>"
                                                    label="<?php echo $value_cycle->nom_cycle; ?>"/>
                                        <?php } ?>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">

                            <label class="control-label col-md-3">Description</label>

                            <div class="col-md-9">
                                <label for="description" class="field prepend-icon">
                                    <textarea type="text" name="description" id="description"
                                              class="form-control gui-input"> </textarea>
                                </label>
                            </div>
                        </div>

                        </div>
                    </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

    </form>
</div><!-- /.modal -->


<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire

            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('C_section/save')?>", //url du save (donn?es envoy?s par post)

            title_modal_add: 'Nouvelle section', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'libelle_section', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition de section', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'libelle_section',//l'emplacement du focus en mode edit

            url_edit: "<?php echo site_url('C_section/get_record')?>", //url le fonction qui recup?re la donn?es de la ligne
            url_delete: "<?php echo site_url('C_section/delete')?>", //url de la fonction supprim?
        });
    });
</script>
