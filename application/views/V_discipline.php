<?php btn_add_action('DISC'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des Disciplines</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 30%">code</th>
                        <th style="width: 30%">libelle</th>
                        <th style="width: 40%">couleur</th>
                        <th style="...">composition</th>
                        <th style="...">Description</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>
                            <td><?php echo $value->code_discipline; ?></td>
                            <td><?php echo $value->libelle_discipline; ?></td>
                            <td><?php echo $value->couleur_discipline; ?></td>
                            <td><?php if ($value->composition_matiere == '1') {
                                    echo "OUI";
                                } else {
                                    echo "NON";
                                } ?></td>
                            <td><?php echo $value->description; ?></td>
                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->id_discipline, 'DISC'); ?>
                                <?php btn_delete_action($value->id_discipline, 'DISC'); ?>
                                <?php btn_show_action($value->id_discipline, 'DISC'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_discipline" name="id_discipline"/>
                    <div class="form-body  admin-form">
                        <div class="form-group">
                            <label class="control-label col-md-3">code discipline <i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="code_discipline" class="field prepend-icon">
                                    <input type="text" name="code_discipline" id="code_discipline"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Libelle Discipline</label>

                            <div class="col-md-9">
                                <label for="libelle_discipline" class="field prepend-icon">
                                    <input type="text" name="libelle_discipline" id="libelle_discipline"
                                           class="form-control gui-input">
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Couleur Discipline</label>

                            <div class="col-md-9">
                                <label for="couleur_discipline" class="field prepend-icon">
                                    <input type="text" name="couleur_discipline" id="couleur_discipline"
                                           class="form-control gui-input">
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Composition Matière </label>

                            <div class="col-md-9">
                                <label for="composition_matiere" class="field prepend-icon">
                                    <select name="composition_matiere" id="composition_matiere"
                                            class="form-control gui-input">
                                        <option value="1" selected label="Oui"/>
                                        <option value="-1" label="Non"/>
                                    </select>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>

                            <div class="col-md-9">
                                <label for="description" class="field prepend-icon">
                                    <textarea type="text" name="description" id="description"
                                              class="form-control gui-input"> </textarea>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                                <tr>
                                    <td><label class="control-label col-md-3">Cycle </label></td>

                                <label for="code_cycle" class="field prepend-icon">
                                    <?php foreach ($cycle as $value_cycle) { ?>
                                    <td><input type="checkbox" name="<?php echo $value_cycle->nom_cycle; ?>"
                                               id="<?php echo $value_cycle->nom_cycle; ?>"
                                               value="<?php echo $value_cycle->code_cycle; ?>"/><?php echo $value_cycle->nom_cycle; ?>
                                    </td>
                                        <br>
                                    <?php } ?></tr>
                                </label>
                            </table>

                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
                        <!-- /.modal-dialog -->
        </div>
    </form>
</div><!-- /.modal -->
<div id="modal_view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="view" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">

                    <div class="form-body  admin-form">

                        <label class="control-label col-md-3">matiere</label>

                        <div class="col-md-9">
                            <label for="matiere" class="field prepend-icon">
                                <ul id="matiere_test">
                                </ul>
                            </label>
                        </div>
                    </div>
                    <div class="form-body  admin-form">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>

            </div>

    </form>

</div>



<script type="text/javascript">
    $(document).ready(function () {
        //TableManageButtons.init();
        $(document).ready(function () {
            $('#datatable-buttons').managing_ajax({
                id_modal_form: 'modal_form', //id du modal qui contient le formulaire
                id_view_form: 'modal_view', //id du modal qui contient le formulaire


                id_form: 'form', //id du formulaire
                url_submit: "<?php echo site_url('C_discipline/save')?>", //url du save (donn�es envoy�s par post)
                id_show: 'matiere_test',

                title_modal_add: 'Nouveau ', //Title du modal � l'ouverture en mode ajout
                focus_add: 'libelle_discipline', //l'emplacement du focus en mode ajout

                title_modal_edit: 'Edition de discipline', //Title du modal � l'ouverture en mode edit
                focus_edit: 'libelle_discipline',//l'emplacement du focus en mode edit
                colonne_show: 'libelle_matiere', //la colonne a affciher lorsqu'on regarde une categorie
                title_modal_show: 'Visualisation de matiere', //Title du modal ? l'ouverture en mode visualisation
                focus_show: 'libelle_discipline',//l'emplacement du focus en mode edit



                url_edit: "<?php echo site_url('C_discipline/get_record')?>", //url le fonction qui recup�re la donn�es de la ligne
                url_delete: "<?php echo site_url('C_discipline/delete')?>", //url de la fonction supprim�
                url_show: "<?php echo site_url('C_discipline/show')?>", //url de la fonction supprim�
            });
        });
    });
</script>
