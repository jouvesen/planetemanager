<?php btn_add_action('FCT'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des types de fonctions</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th style="width: 90%">Libelle fonction</th>
                        <th style="width: 90%">Description fonction</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>
                            <td><?php echo $value->nom_fonction; ?></td>
                            <td><?php echo $value->description; ?></td>

                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->code_fonction, 'FCT'); ?>
                                <?php btn_delete_action($value->code_fonction, 'FCT'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="code_fonction" name="code_fonction"/>

                    <div class="form-body  admin-form">

                        <div class="form-group">
                            <label class="control-label col-md-3">libelle fonction </label>

                            <div class="col-md-9">
                                <label for="nom_fonction" class="field prepend-icon">
                                    <input type="text" name="nom_fonction" id="nom_fonction"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                            <label class="control-label col-md-3">Description fonction</label>

                            <div class="col-md-9">
                                <label for="description_fonction" class="field prepend-icon">
                                    <textarea name="description" id="description"></textarea>
                                </label>
                            </div>
                            <label class="control-label col-md-3">Choix categorie</label>

                            <div class="col-md-9">
                                <label for="categorie" class="field prepend-icon">
                                    <?php foreach ($data_categorie as $value_categorie) { ?>
                                        <input type="checkbox" name="<?php echo $value_categorie->libelle_categorie ?>"
                                               id="<?php echo $value_categorie->libelle_categorie ?>"
                                               value="<?php echo $value_categorie->code_categorie ?>"><?php echo $value_categorie->libelle_categorie ?>
                                        <br>
                                    <?php } ?>
                                </label>
                            </div>

                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->


<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire

            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('C_types_fonctions/save')?>", //url du save (donn?es envoy?s par post)

            title_modal_add: 'Nouvelle fonction', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'Libelle_fonction', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition de fonctions', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'nom_fonction',//l'emplacement du focus en mode edit

            url_edit: "<?php echo site_url('C_types_fonctions/get_record')?>", //url le fonction qui recupère la donn?es de la ligne
            url_delete: "<?php echo site_url('C_types_fonctions/delete')?>", //url de la fonction supprim?
        });
    });
</script>
