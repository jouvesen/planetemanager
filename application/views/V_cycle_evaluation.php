<?php btn_add_action('CYCLE_EVAL'); ?>
<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des évaluations par cycle</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th style="...">Nom cycle</th>
                        <th style="...">Type Evaluation</th>
                        <th style="...">Note maximale</th>
                        <th style="...">type Val</th>
                        <th style="...">Nombre</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>
                            <td><?php echo $value->Cycle; ?></td>
                            <td><?php echo $value->type_evaluation; ?></td>
                            <td><?php echo $value->note_max; ?></td>
                            <td><?php echo $value->type_val; ?></td>
                            <td><?php echo $value->nombre; ?></td>

                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->ID, 'CYCLE_EVAL'); ?>
                                <?php btn_delete_action($value->ID, 'CYCLE_EVAL'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="id_evaluation_cycle" name="id_evaluation_cycle"/>


                    <div class="form-body  admin-form">


                        <div class="form-group">
                            <label class="control-label col-md-3">Nom Cycle<i
                                    class='text-danger'>*</i></label>

                            <div class="col-md-9">
                                <label for="code_cycle" class="field prepend-icon">
                                    <select type="text" id="code_cycle" name="code_cycle">
                                    <?php foreach($all_data_cycle as $cycle){?>
                                        <option value="<?php echo $cycle->code_cycle ;?>" selected> <?php echo $cycle->nom_cycle; ?></option>
                                        <?php } ?>
                                    </select>
                                </label>
                            </div>
                            </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Type évaluation<i
                                    class='text-danger'>*</i></label>

                            <div class="col-md-9">
                                <label for="code_type_evaluation" class="field prepend-icon">
                                    <select type="text" id="code_type_evaluation" name="code_type_evaluation">
                                    <?php foreach($all_data_type_evaluation as $type_evaluation){?>
                                        <option value="<?php echo $type_evaluation->code_type_evaluation; ?>" selected> <?php echo $type_evaluation->type_evaluation; ?></option>
                                        <?php } ?>
                                    </select>
                                </label>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Note maximale</label>

                            <div class="col-md-9">
                                <label for="note_max" class="field prepend-icon">
                                    <input  type="text" id="note_max" name="note_max" />

                                </label>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Type val</label>

                            <div class="col-md-9">
                                <label for="type_val" class="field prepend-icon">
                                    <select  type="text" id="type_val" name="type_val" >
                                    <option value="D">D</option>
                                        <option value="C">C</option>
                                    </select>

                                </label>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre d'évaluation</label>

                            <div class="col-md-9">
                                <label for="nombre" class="field prepend-icon">
                                    <select  type="text" id="nombre" name="nombre" >
                                    <option value="1">une seule</option>
                                        <option value="+1">Plusieurs</option>
                                    </select>

                                </label>
                            </div>

                        </div>


                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>
        <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</form>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire
            // id_view_form: 'modal_view', //id du modal qui contient le formulaire

            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('C_cycle_evaluation/save')?>", //url du save (donn?es envoy?s par post)
            // id_show: 'fonction_test',
            title_modal_add: 'Nouvelle evaluation d\'un cycle', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'code_cycle', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition du type de période', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'code_cycle',//l'emplacement du focus en mode edit
            //colonne_show: 'libelle_fonction', //la colonne a affciher lorsqu'on regarde une categorie

            // title_modal_show: 'Visualisation de fonctions', //Title du modal ? l'ouverture en mode visualisation
            //focus_show: 'libelle_annee',//l'emplacement du focus en mode edit

            url_edit: "<?php echo site_url('C_cycle_evaluation/get_record')?>", //url le fonction qui édite une ligne
            url_delete: "<?php echo site_url('C_cycle_evaluation/delete')?>", //url de la fonction supprimée
            url_show: "<?php echo site_url('C_cycle_evaluation/show')?>", //url de la fonction visualiser
        });
    });
</script>
