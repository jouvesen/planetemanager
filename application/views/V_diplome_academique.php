<?php btn_add_action('DIPAC'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des diplômes académiques</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th style="width: 50%">Libelle</th>
                        <th style="width: 90%">description</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>
                            <td><?php echo $value->libelle_diplome_ac; ?></td>
                            <td><?php echo $value->desc_diplome_ac; ?></td>
                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->code_diplome_ac, 'DIPAC'); ?>
                                <?php btn_delete_action($value->code_diplome_ac, 'DIPAC'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="code_diplome_ac" name="code_diplome_ac"/>
                    <div class="form-body  admin-form">
                        <div class="form-group">
                            <label class="control-label col-md-3">Libellé diplôme<i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="libelle_diplome_ac" class="field prepend-icon">
                                    <input type="text" name="libelle_diplome_ac" id="libelle_diplome_ac"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                            <div class="form-body  admin-form">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-9">
                                        <label for="desc_diplome_ac" class="field prepend-icon">
                                            <textarea type="text" name="desc_diplome_ac" id="desc_diplome_ac"
                                                      class="form-control gui-input"> </textarea>
                                        </label>
                                    </div>

                                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->


<script type="text/javascript">
    $(document).ready(function () {
        //TableManageButtons.init();
        $(document).ready(function () {
            $('#datatable-buttons').managing_ajax({
                id_modal_form: 'modal_form', //id du modal qui contient le formulaire

                id_form: 'form', //id du formulaire
                url_submit: "<?php echo site_url('C_diplome_academique/save')?>", //url du save (donn�es envoy�s par post)

                title_modal_add: 'Nouveau ', //Title du modal � l'ouverture en mode ajout
                focus_add: 'libelle_diplome_ac', //l'emplacement du focus en mode ajout

                title_modal_edit: 'Edition', //Title du modal � l'ouverture en mode edit
                focus_edit: 'libelle_diplome_ac',//l'emplacement du focus en mode edit

                url_edit: "<?php echo site_url('C_diplome_academique/get_record')?>", //url le fonction qui recup�re la donn�es de la ligne
                url_delete: "<?php echo site_url('C_diplome_academique/delete')?>", //url de la fonction supprim�
            });
        });
    });
</script>
