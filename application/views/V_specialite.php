<?php btn_add_action('SPEC'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des spécialités</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th style="...">Nom</th>
                        <th style="...">code_specialite</th>
                        <th style="width: 90%">Description</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>

                        <td><?php echo $value->nom_specialite; ?></td>
                            <td><?php echo $value->code_spec; ?></td>
                            <td><?php echo $value->description; ?></td>



                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php echo btn_edit_action($value->code_specialite, 'SPEC'); ?>
                                <?php echo btn_delete_action($value->code_specialite, $value->etat_specialite, 'SPEC'); ?>
                                <?php echo btn_show_action($value->code_specialite); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="code_specialite" name="code_specialite"/>
                    <div class="form-body  admin-form">

                        <div class="form-group">
                            <label class="control-label col-md-3">Nom <i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="nom_specialite" class="field prepend-icon">
                                    <input type="text" name="nom_specialite" id="nom_specialite"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Code Spécialité</label>

                            <div class="col-md-9">
                                <label for="code_spec" class="field prepend-icon">
                                    <input type="text" name="code_spec" id="code_spec"
                                           class="form-control gui-input">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>

                            <div class="col-md-9">
                                <label for="description" class="field prepend-icon">
                                    <textarea name="description" id="description"
                                              class="form-control gui-input"> </textarea>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Cycle  </label>

                            <div class="col-md-9">

                                <div class="col-md-9">
                                    <label for="code_cycle" class="field prepend-icon">
                                        <?php foreach ($all_data_cycle as $value_cycle) { ?>
                                            <input type="checkbox" name="<?php echo $value_cycle->nom_cycle; ?>" id="<?php echo $value_cycle->nom_cycle; ?>"
                                                   value="<?php echo $value_cycle->code_cycle; ?>"/><?php echo $value_cycle->nom_cycle; ?>
                                            <br/>

                                        <?php } ?>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Discipline </label>

                            <div class="col-md-9">

                                <div class="col-md-9">


                                            <table style="width: 100%" id="datatable-Discipline" class="table table-striped table-bordered table-responsive">
                                                <thead>
                                                <tr>
                                                    <td>discipline</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($all_data_discipline as $value_discipline) { ?>
                                                <tr>
                                                    <td><input type="checkbox"
                                                               name="<?php echo $value_discipline->code_discipline; ?>"
                                                               id="<?php echo $value_discipline->code_discipline; ?>"
                                                   value="<?php echo $value_discipline->id_discipline; ?>"/><?php echo $value_discipline->libelle_discipline; ?></td>
                                                </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>



                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->
<div id="modal_view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="view" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">

                    <div class="form-body  admin-form">

                        <label class="control-label col-md-3">matiere</label>

                        <div class="col-md-9">
                            <label for="matiere" class="field prepend-icon">
                                <ul id="matiere_test">
                                </ul>
                            </label>
                        </div>
                    </div>
                    <div class="form-body  admin-form">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>

            </div>

    </form>

</div>



<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire
            id_view_form: 'modal_view', //id du modal qui contient le formulaire


            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('C_specialite/save')?>", //url du save (donn?es envoy?s par post)
            id_show: 'matiere_test',


            title_modal_add: 'Nouvelle specialite', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'nom_specialite', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition de specialite', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'nom_specialite',//l'emplacement du focus en mode edit
            colonne_show: 'libelle_matiere', //la colonne a affciher lorsqu'on regarde une categorie
            title_modal_show: 'Visualisation des matiere', //Title du modal ? l'ouverture en mode visualisation
            focus_show: 'nom_specialite',//l'emplacement du focus en mode edit

            url_edit: "<?php echo site_url('C_specialite/get_record')?>", //url le fonction qui recup?re la donn?es de la ligne
            url_delete: "<?php echo site_url('C_specialite/delete')?>", //url de la fonction supprim?
            url_show: "<?php echo site_url('C_specialite/show')?>", //url de la fonction supprim?
        });
        $('#datatable-Discipline').DataTable({
            searching: false,
            //"bInfo" : false ,
            //"bLengthChange" : false,
            language: langageFrDataTable()
        });
    });
</script>
