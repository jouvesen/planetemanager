<script>
    var resizefunc = [];
</script>

<!-- CUSTOM JS -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>
<!-- jQuery  moment -->
<script src="<?php echo base_url(); ?>assets/plugins/moment/moment.js"></script>

<script src="<?php echo base_url(); ?>assets/js/managing_menu.js"></script>
<script src="<?php echo base_url(); ?>assets/js/managing_ajax.js"></script>

<!-- jQuery
<script src="<php echo base_url(); ?>assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="<php echo base_url(); ?>assets/plugins/counterup/jquery.counterup.min.js"></script>-->
<!-- flot Chart
<script src="<php echo base_url(); ?>assets/plugins/flot-chart/jquery.flot.js"></script>
<script src="<php echo base_url(); ?>assets/plugins/flot-chart/jquery.flot.time.js"></script>
<script src="<php echo base_url(); ?>assets/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<php echo base_url(); ?>assets/plugins/flot-chart/jquery.flot.resize.js"></script>
<script src="<php echo base_url(); ?>assets/plugins/flot-chart/jquery.flot.pie.js"></script>
<script src="<php echo base_url(); ?>assets/plugins/flot-chart/jquery.flot.selection.js"></script>
<script src="<php echo base_url(); ?>assets/plugins/flot-chart/jquery.flot.stack.js"></script>
<script src="<php echo base_url(); ?>assets/plugins/flot-chart/jquery.flot.crosshair.js"></script>-->
<!-- jQuery
<script src="<php echo base_url(); ?>assets/pages/jquery.todo.js"></script>-->
<!-- jQuery
<script src="<php echo base_url(); ?>assets/pages/jquery.chat.js"></script>-->
<!-- jQuery
<script src="<php echo base_url(); ?>assets/pages/jquery.dashboard.js"></script>-->

<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">

<!-- sweetalert  -->
<link href="<?php echo base_url(); ?>assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert/dist/sweetalert.min.js"></script>

<!-- Modal Plugins css -->
<link href="<?php echo base_url(); ?>assets/plugins/modal-effect/css/component.css" rel="stylesheet">
<!--<script src="<php echo base_url(); ?>assets/plugins/modal-effect/js/classie.js"></script>-->

<!-- DataTables js && css old
<link href="<php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<script src="<php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>-->
<!-- JQuery DataTable Css && js 10-->
<link href="<?php echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>


<!-- jQuery Validate Plugin && Admin Forms CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin-forms/css/admin-forms.css">
<script src="<?php echo base_url(); ?>assets/admin-forms/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin-forms/js/jquery.validate.translations.fr-FR.js"></script>

<!-- inputmask js-->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>

<!-- notification js-->
<link href="<?php echo base_url(); ?>assets/plugins/notifications/notification.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugins/notifyjs/dist/notify.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/notifications/notify-metro.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/notifications/notifications.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>

<!--calendar css-->
<link href="<?php echo base_url(); ?>assets/plugins/fullcalendar/dist/fullcalendar.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/dist/fullcalendar.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/dist/lang/fr.js"></script>


</body>
</html>