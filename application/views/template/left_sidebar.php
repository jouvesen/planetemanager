<!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-details">
                        <div class="pull-left">
                            <img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg" alt="" class="thumb-md img-circle">
                        </div>
                        <div class="user-info">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <?php
                                    // Nom de l'utilisateur
                                    $tab_data_ses = $this->session->all_userdata();
                                    echo $tab_data_ses['lfc_jafr12_s']['nom'];
                                    ?>
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)"><i class="md md-face-unlock"></i> Profile<div class="ripple-wrapper"></div></a></li>
                                    <li><a href="javascript:void(0)"><i class="md md-settings"></i> Settings</a></li>
                                    <li><a href="javascript:void(0)"><i class="md md-lock"></i> Lock screen</a></li>
                                    <li><a href="<?php  echo site_url() ?>se_deconnecter"><i class="md md-settings-power"></i> Logout</a></li>
                                </ul>
                            </div>
                            
                            <p class="text-muted m-0"><?php
                                  echo $tab_data_ses['lfc_jafr12_s']['profil'];
                                ?></p>
                        </div>
                    </div>
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
                            <li>
                                <a href="<?php echo base_url(); ?>C_dashboard" class="menu" id="menu_dashbord"><i
                                        class="md md-home"></i><span> Dashboard </span></a>
                            </li>
                            <?php
                            //Recuperation du tableau des roles menus enregistré dans la session
                            $menu_roles = $this->session->menu_roles;
                            $smenu_roles = $this->session->smenu_roles;

                            ?><?php if (!isset($menu_roles['PARAM']['d_read'])): ?>
                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i
                                        class="md md-account-balance"></i><span>PARAMETRAGE </span><span
                                        class="pull-right"><i class="md md-add"></i></span></a>
                                <ul class="list-unstyled">
                                    <?php if (isset($smenu_roles['FCT']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_types_fonctions" class="menu"
                                           id="menu_types_fonctions">Fonctions</a></li>
                                    <?php endif; ?>
                                    <?php if (isset($smenu_roles['CSTR']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_categorie" class="menu" id="menu_categorie">catégorie
                                            de structures</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['SPEC']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_specialite" class="menu"
                                               id="menu_scialite">specialite</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['DISC']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_discipline" class="menu" id="menu_matiere">discipline</a>
                                        </li><?php endif; ?>

                                    <?php if (isset($smenu_roles['MAT']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_matiere" class="menu" id="menu_discipline">matière</a>
                                        </li><?php endif; ?>

                                    <?php if (isset($smenu_roles['CATEVAL']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_categorie_evaluation" class="menu"
                                           id="menu_categorie_evaluation">catégorie d'évaluation</a></li><?php endif; ?>

                                    <?php if (isset($smenu_roles['TYPEEVAL']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_type_evaluation" class="menu"
                                           id="menu_type_evaluation">type d'évaluation</a></li><?php endif; ?>

                                    <?php if (isset($smenu_roles['TYPEPER']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_type_periode" class="menu"
                                           id="menu_type_periode">type de periode</a></li><?php endif; ?>

                                    <?php if (isset($smenu_roles['CYCLE']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_cycle" class="menu"
                                           id="menu_cycle">cycle</a></li><?php endif; ?>

                                    <?php if (isset($smenu_roles['CYCLE_EVAL']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_cycle_evaluation" class="menu"
                                           id="menu_evaluation_cycle">Evaluation cycle</a></li><?php endif; ?>

                                    <?php if (isset($smenu_roles['CORPS']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_corps" class="menu"
                                           id="menu_corps">corps</a></li><?php endif; ?>

                                    <?php if (isset($smenu_roles['GRADE']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_grade" class="menu" id="menu_grade">grade</a>
                                        </li><?php endif; ?>

                                    <?php if (isset($smenu_roles['SECTION']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_section" class="menu" id="menu_section">section</a>
                                        </li><?php endif; ?>

                                    <?php if (isset($smenu_roles['DIPAC']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_diplome_academique" class="menu"
                                           id="menu_diplome_ac">diplome academique</a></li><?php endif; ?>

                                    <?php if (isset($smenu_roles['DIPPRO']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_diplome_professionnel" class="menu"
                                           id="menu_diplome_pro">diplome professionnel</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['ANS']['d_read'])): ?>
                                    <li><a href="<?php echo base_url(); ?>C_annee_scolaire" class="menu"
                                           id="menu_annee_scolaire">Annee Scolaire</a></li><?php endif; ?>

                                    <?php if (isset($smenu_roles['ORGPAT']['d_read'])): ?>

                                        <li><a href="<?php echo base_url(); ?>C_organisation_patronale" class="menu"
                                               id="menu_op">organisation patronale</a></li><?php endif; ?>

                                    <?php if (isset($smenu_roles['PAYS']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_pays" class="menu"
                                               id="menu_pays">pays</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['STATJUR']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_statut_juridique" class="menu"
                                               id="menu_statut_juridique">statut juridique</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['STRUCTURE']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_structure" class="menu"
                                               id="menu_structure">structure</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['LANGUEMATER']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_type_languematernelle" class="menu"
                                               id="menu_languemater">langue maternelle</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['ORPHELIN']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_type_orphelin" class="menu"
                                               id="menu_orphelin">type orphelin</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['REGROUPEMENT']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_type_regroupement" class="menu"
                                               id="menu_regroupement">type de regroupement</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['ACTE']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_type_acte" class="menu"
                                               id="menu_type_acte">acte</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['GROUPESANGUIN']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_type_groupesanguin" class="menu"
                                               id="menu_type_gsanguin">groupe sanguin</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['HANDICAP']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_type_handicap" class="menu"
                                               id="menu_type_handicap">type handicap</a></li><?php endif; ?>
                                    <?php if (isset($smenu_roles['PROG']['d_read'])): ?>
                                        <li><a href="<?php echo base_url(); ?>C_programmes" class="menu"
                                               id="menu_programme">Programmes</a></li><?php endif; ?>


                                </ul>
                            </li>
                            <?php endif; ?>
                            <li class="has_sub">
                                <a href="<?php echo base_url(); ?>front-office" class="waves-effect"><i
                                        class="md md-account-balance"></i><span> Menu établissement </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url(); ?>menu_etablissement/C_menu_etablissement"
                                           class="menu" id="menu_etablissement_ia">IA</a>
                                    </li>
                                    <li><a href="<?php echo base_url(); ?>tableau/C_tableau" class="menu"
                                           id="menu_etablissement_tableau">Tableau</a>
                                    </li>
                                </ul>
                            </li>

                            <?php if (isset($menu_roles['SECURITE'])): ?>
                                <li class="has_sub">
                                    <a href="#" class="waves-effect"><i class="md md-settings"></i><span> Sécurité </span><span class="pull-right"><i class="md md-add"></i></span></a>
                                    <ul class="list-unstyled">

                                        <?php if (isset($smenu_roles['USR']['d_read'])): ?>
                                            <li><a href="<?php echo base_url(); ?>sys/C_sys_user" class="menu"
                                                   id="menu_sys_users">Utilisateurs</a></li>
                                        <?php endif; ?>

                                        <?php if (isset($smenu_roles['MENU']['d_read'])): ?>
                                            <li><a href="<?php echo base_url(); ?>sys/C_sys_menu" class="menu"
                                                   id="menu_sys_menu">Menus</a></li>
                                        <?php endif; ?>
                                        <?php if (isset($smenu_roles['LST_MENU']['d_read'])): ?>
                                            <li><a href="<?php echo base_url(); ?>sys/C_sys_menu/list_menu" class="menu"
                                                   id="menu_list_menu">Liste Menu</a></li>
                                        <?php endif; ?>
                                        <?php if (isset($smenu_roles['LST_S_MENU']['d_read'])): ?>
                                            <li><a href="<?php echo base_url(); ?>sys/C_sys_menu/list_sous_menu"
                                                   class="menu" id="menu_list_sous_menu">Liste Sous Menus</a></li>
                                        <?php endif; ?>

                                        <?php if (isset($smenu_roles['PROFIL']['d_read'])): ?>
                                            <li><a href="<?php echo base_url(); ?>sys/C_sys_profil" class="menu"
                                                   id="menu_sys_profils">Profils</a></li>
                                        <?php endif; ?>

                                    </ul>
                                </li>
                            <?php endif; ?>

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End --> 