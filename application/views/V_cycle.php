<?php btn_add_action('CYCLE'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des Cycles</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th style="...">Libelle Cycle</th>
                        <th style="width: 90%">description</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>
                            <td><?php echo $value->nom_cycle; ?></td>
                            <td><?php echo $value->description; ?></td>

                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->code_cycle, 'CYCLE'); ?>
                                <?php btn_delete_action($value->code_cycle, 'CYCLE'); ?>
                                <?php btn_show_action($value->code_cycle); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="code_cycle" name="code_cycle"/>
                    <div class="form-body  admin-form">

                        <div class="form-group">
                            <label class="control-label col-md-3">Libelle Cycle <i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="nom_cycle" class="field prepend-icon">
                                    <input type="text" name="nom_cycle" id="nom_cycle"
                                           class="form-control gui-input" required>
                                </label>
                            </div>

                            </div>
                        <div class="form-group">
                            </label>
                            <label class="control-label col-md-3">description</label>
                            <div class="col-md-9">
                                <label for="description" class="field prepend-icon">
                                    <input type="text" name="description" id="description"
                                           class="form-control gui-input">
                                </label>
                            </div>
                        </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->
<div id="modal_view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="view" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">

                    <div class="form-body  admin-form">

                        <label class="control-label col-md-3">specialite</label>

                        <div class="col-md-9">
                            <label for="specialite" class="field prepend-icon">
                                <ul id="specialite_test">
                                </ul>
                            </label>
                        </div>
                    </div>
                    <div class="form-body  admin-form">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>

            </div>

    </form>

</div>




<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire
            id_view_form: 'modal_view', //id du modal qui contient le formulaire


            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('C_cycle/save')?>", //url du save (donn?es envoy?s par post)
            id_show: 'specialite_test',


            title_modal_add: 'Nouvelle cycle', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'nom_cycle', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition de cycle', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'nom_cycle',//l'emplacement du focus en mode edit
            colonne_show: 'nom_specialite', //la colonne a affciher lorsqu'on regarde une categorie
            title_modal_show: 'Visualisation d specialite', //Title du modal ? l'ouverture en mode visualisation
            focus_show: 'nom_cycle',//l'emplacement du focus en mode edit


            url_edit: "<?php echo site_url('C_cycle/get_record')?>", //url le fonction qui recup?re la donn?es de la ligne
            url_delete: "<?php echo site_url('C_cycle/delete')?>", //url de la fonction supprim?
            url_show: "<?php echo site_url('C_cycle/show')?>", //url de la fonction supprim?
        });
    });
</script>
