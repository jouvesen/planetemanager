<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo site_url(); ?>assets/blue/js/jquery.min.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/js/bootstrap.min.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/js/detect.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/js/fastclick.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/js/jquery.slimscroll.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/js/jquery.blockUI.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/js/waves.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/js/wow.min.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/js/jquery.nicescroll.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/js/jquery.scrollTo.min.js"></script>

<script src="<?php echo site_url(); ?>assets/blue/js/jquery.app.js"></script>

<!-- jQuery  -->
<script src="<?php echo site_url(); ?>assets/blue/plugins/moment/moment.js"></script>

<!-- jQuery  -->
<script src="<?php echo site_url(); ?>assets/blue/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/plugins/counterup/jquery.counterup.min.js"></script>

<!-- jQuery  -->
<script src="<?php echo site_url(); ?>assets/blue/plugins/sweetalert/dist/sweetalert.min.js"></script>


<!-- flot Chart -->
<script src="<?php echo site_url(); ?>assets/blue/plugins/flot-chart/jquery.flot.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/plugins/flot-chart/jquery.flot.time.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/plugins/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/plugins/flot-chart/jquery.flot.pie.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/plugins/flot-chart/jquery.flot.selection.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/plugins/flot-chart/jquery.flot.stack.js"></script>
<script src="<?php echo site_url(); ?>assets/blue/plugins/flot-chart/jquery.flot.crosshair.js"></script>

<!-- jQuery  -->
<script src="<?php echo site_url(); ?>assets/blue/pages/jquery.todo.js"></script>

<!-- jQuery  -->
<script src="<?php echo site_url(); ?>assets/blue/pages/jquery.chat.js"></script>

<!-- jQuery  -->
<script src="<?php echo site_url(); ?>assets/blue/pages/jquery.dashboard.js"></script>

<script type="text/javascript">
    /* ==============================================
     Counter Up
     =============================================== */
    jQuery(document).ready(function ($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });
    });
</script>


</body>
</html>