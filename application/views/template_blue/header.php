<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Moltran - Responsive Admin Dashboard Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="shortcut icon" href="<?php echo site_url(); ?>assets/blue/images/favicon_1.ico">

    <link href="<?php echo site_url(); ?>assets/blue/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet"
          type="text/css">

    <link href="<?php echo site_url(); ?>assets/blue/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/blue/css/core.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/blue/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/blue/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/blue/css/pages.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/blue/css/menu.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/blue/css/responsive.css" rel="stylesheet" type="text/css">

    <script src="<?php echo site_url(); ?>assets/blue/js/modernizr.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>


<body class="fixed-left">