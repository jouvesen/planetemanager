<!-- Begin page -->
<div id="wrapper">
    <?php
    $this->load->view('template_blue/header');
    $this->load->view('template_blue/top');
    $this->load->view('template/left_sidebar');
    $this->load->view('template_blue/content_page');
    $this->load->view('template_blue/right');
    $this->load->view('template_blue/footer');
    ?>
</div>
<!-- END wrapper -->