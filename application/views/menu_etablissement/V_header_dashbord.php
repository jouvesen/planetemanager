<?php foreach ($cycle as $value_cycle => $value) { ?>
    <?php if ($value['nom_cycle'] == "Prescolaire") { ?>
        <div class="col-sm-2">
            <div class="mini-stat clearfix bx-shadow ">
                                <span class="mini-stat-icon"><i style="background-color: #080808"
                                                                class="<?= $value['image_cycle'] ?>"></i></span>

                <div class="mini-stat-info text-white">
                    <span class="counter"></span>
                    <h4>Prescolaire</h4>

                </div>

                <div class="tiles-progress">
                    <div class="m-t-20">
                        <h5 class="text-uppercase m-0">Publics
                            <span
                                class="pull-right"><?= array_key_exists("presc_pub", $value) ? $value["presc_pub"] : "0" ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Privés
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("presc_priv", $value) ? $value['presc_priv'] : "0" ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Associations
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("presc_comm_ass", $value) ? $value['presc_comm_ass'] : "0" ?></span>
                        </h5>

                    </div>
                </div>

            </div>
        </div>
    <?php } elseif ($value['nom_cycle'] == "Elementaire") { ?>

        <div class="col-sm-2">
            <div class="mini-stat clearfix bx-shadow ">
                                <span class="mini-stat-icon"><i style="background-color: #080808"
                                                                class="<?= $value['image_cycle'] ?>"></i></span>

                <div class="mini-stat-info text-white">
                    <span class="counter"></span>
                    <h4>Elémentaire</h4>

                </div>

                <div class="tiles-progress">
                    <div class="m-t-20">
                        <h5 class="text-uppercase m-0">Publics
                            <span
                                class="pull-right"><?= array_key_exists("elem_pub", $value) ? $value['elem_pub'] : "0" ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Privés
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("elem_priv", $value) ? $value['elem_priv'] : "0" ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Associations
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("elem_comm_ass", $value) ? $value['elem_comm_ass'] : "0" ?></span>
                        </h5>

                    </div>
                </div>

            </div>
        </div>
    <?php } elseif ($value['nom_cycle'] == "Moyen") { ?>

        <div class="col-sm-2">
            <div class="mini-stat clearfix bx-shadow ">
                                <span class="mini-stat-icon"><i style="background-color: #080808"
                                                                class="<?= $value['image_cycle'] ?>"></i></span>

                <div class="mini-stat-info text-white">
                    <span class="counter"></span>
                    <h4>Moyen</h4>

                </div>

                <div class="tiles-progress">
                    <div class="m-t-20">
                        <h5 class="text-uppercase m-0">Publics
                            <span
                                class="pull-right"><?= array_key_exists("moy_pub", $value) ? $value['moy_pub'] : "0" ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Privés
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("moy_priv", $value) ? $value['moy_priv'] : "0" ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Associations
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("moy_comm_ass", $value) ? $value['moy_comm_ass'] : "0" ?></span>
                        </h5>

                    </div>
                </div>

            </div>
        </div>
    <?php } elseif ($value['nom_cycle'] == "Secondaire") { ?>

        <div class="col-sm-2">
            <div class="mini-stat clearfix bx-shadow ">
                                <span class="mini-stat-icon"><i style="background-color: #080808"
                                                                class="<?= $value['image_cycle'] ?>>"</i></span>

                <div class="mini-stat-info text-white">
                    <span class="counter"></span>
                    <h4>Secondaire</h4>

                </div>

                <div class="tiles-progress">
                    <div class="m-t-20">
                        <h5 class="text-uppercase m-0">Publics
                            <span
                                class="pull-right"><?= array_key_exists("sec_pub", $value) ? $value['sec_pub'] : 0 ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Privés
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("sec_priv", $value) ? $value['sec_priv'] : 0 ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Associations
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("sec_comm_ass", $value) ? $value['sec_comm_ass'] : 0 ?></span>
                        </h5>

                    </div>
                </div>

            </div>
        </div>


    <?php } elseif ($value['nom_cycle'] == "Moyen secondaire") { ?>

        <div class="col-sm-2">
            <div class="mini-stat clearfix bx-shadow ">
                                <span class="mini-stat-icon"><i style="background-color: #080808"
                                                                class="<?= $value['image_cycle'] ?>>"</i></span>

                <div class="mini-stat-info text-white">
                    <span class="counter"></span>
                    <h4>Moyen Secondaire</h4>

                </div>

                <div class="tiles-progress">
                    <div class="m-t-20">
                        <h5 class="text-uppercase m-0">Publics
                            <span
                                class="pull-right"><?= array_key_exists("moy_sec_pub", $value) ? $value['moy_sec_pub'] : 0 ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Privés
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("moy_sec_priv", $value) ? $value['moy_sec_priv'] : 0 ?></span>
                        </h5>

                    </div>
                    <div class="m-t-20">

                        <h5 class="text-uppercase m-0">Associations
                            <span class="pull-right"
                                  id="prive"><?= array_key_exists("moy_sec_comm_ass", $value) ? $value['moy_sec_comm_ass'] : 0 ?></span>
                        </h5>

                    </div>
                </div>

            </div>
        </div>


    <?php } elseif ($value['nom_cycle'] == "Daara") { ?>

        <div class="col-sm-2">
        <div class="mini-stat clearfix bx-shadow ">
                                <span class="mini-stat-icon"><i style="background-color: #080808"
                                                                class="<?= $value['image_cycle'] ?>>"</i></span>

            <div class="mini-stat-info text-white">
                <span class="counter"></span>
                <h4>Daaras</h4>

            </div>

            <div class="tiles-progress">
                <div class="m-t-20">
                    <h5 class="text-uppercase m-0">Publics
                        <span
                            class="pull-right"><?= array_key_exists("daara_pub", $value) ? $value['daara_pub'] : 0 ?></span>
                    </h5>

                </div>
                <div class="m-t-20">

                    <h5 class="text-uppercase m-0">Privés
                        <span class="pull-right"
                              id="prive"><?= array_key_exists("daara_priv", $value) ? $value['daara_priv'] : 0 ?></span>
                    </h5>

                </div>
                <div class="m-t-20">

                    <h5 class="text-uppercase m-0">Associations
                        <span class="pull-right"
                              id="prive"><?= array_key_exists("daara_comm_ass", $value) ? $value['daara_comm_ass'] : 0 ?></span>
                    </h5>

                </div>
            </div>

        </div>

    <?php }
} ?>