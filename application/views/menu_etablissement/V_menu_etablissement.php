<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Planete 2.0</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link href="<?php echo base_url(); ?>assets/images/favicon_1.ico" rel="shortcut icon">
    <link href="<?php echo base_url(); ?>assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet"
          type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/pages.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/menu.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.chained.min.js"></script>
    <script type="text/javascript">$(function () {
            $("#ief").chained("#ia");
            $("#commune").chained("#ief");
            $('#ia').change(function () {
                ia_choice = $(this).val();
                $.ajax({
                    url: "<?php echo site_url('menu_etablissement/C_menu_etablissement/get_relaod_data_ia')?>/" + ia_choice,
                    type: "GET",
                    dataType: "TEXT",
                    success: function (data) {

                        $('#ia_chosen').val(ia_choice);
                        $('#header_dashboard').empty().html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error get data from ajax');
                    }
            });
            });
            $('#ief').change(function () {
                ia = $('#ief_chosen').val();
                ief_choice = $(this).val();
                $.ajax({
                    url: "<?php echo site_url('menu_etablissement/C_menu_etablissement/get_relaod_data_ief')?>/" + ief_choice,
                    type: "GET",
                    dataType: "TEXT",
                    success: function (data) {

                        $('#ia_chosen').val(ief_choice);
                        $('#header_dashboard').empty().html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error get data from ajax');
                    }
                });
            });
            $('#commune').change(function () {
                commune_choice = $(this).val();
                $.ajax({
                    url: "<?php echo site_url('menu_etablissement/C_menu_etablissement/get_relaod_data_commune')?>/" + commune_choice,
                    type: "GET",
                    dataType: "TEXT",
                    success: function (data) {

                        $('#commune_chosen').val(commune_choice);
                        $('#header_dashboard').empty().html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error get data from ajax');
                    }
                });
            });

        });
    </script>


    <style type="text/css">
        .text-default {
            color: #CCCCCC;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>

<body class="fixed-left">
<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Menu des Etablissements</h3>
            </div>
            <div>
                <div class="row" id="header_dashboard">




                </div>
                <!-- end row -->
            </div>

            <div class="col-lg-12">
                <form action="#" id="form" class="form-vertical">
                    <input type="hidden" id="ia_chosen" name="ia_chosen">
                    <input type="hidden" id="ief_chosen" name="ief_chosen">
                    <input type="hidden" id="commune_chosen" name="commune_chosen">
                        <div class="modal-content">
                            <div class="col-md-4">
                                <label class="control-label col-md-2"><h4>I A</h4></label>

                                <label class="col-md-6">

                                    <select name="ia" id="ia" class="form-control ">
                                        <option value="">Sélectionner un IA</option>

                                        <div class="col-md-12">

                                            <?php foreach ($ia_value as $ia) { ?>
                                                <label for="ia" class="field prepend-icon col-md-2">
                                                    <option
                                                        value="<?= $ia['CODE_REGROUPEMENT'] ?>"> <?= $ia['LIBELLE_REGROUPEMENT'] ?>
                                                    </option>
                                                </label>

                                            <?php } ?>
                                        </div>

                                    </select>
                                </label>

                            </div>
                            <div class="col-md-4">
                                <label class="control-label col-md-2"><h4>I E F</h4></label>

                                <label class="control-label col-md-6">
                                    <select name="ief" id="ief" class="form-control col-md-4">
                                        <option value="">Sélectionner un IEF</option>
                                        <div class="col-md-12">
                                            <?php foreach ($ief as $ief_value) { ?>
                                                <label for="ia" class="field prepend-icon">
                                                    <option class="<?php echo $ief_value['id_parent'] ?>"
                                                            value="<?php echo $ief_value['id_ief'] ?>"><?php echo $ief_value['IEF'] ?></option>

                                                </label>
                                                <?php
                                            } ?>
                                        </div>
                                    </select>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label col-md-3"><h4>Commune</h4></label>
                                <label class="control-label col-md-6">
                                    <select name="commune" id="commune" class="form-control">
                                        <option value="">Sélectionner une commune</option>
                                        <div class="col-md-12">
                                            <?php foreach ($commune as $value_commune) { ?>
                                                <label for="ia" class="field prepend-icon">
                                                    <option class="<?php echo $value_commune['id_parent'] ?>"
                                                            value="<?php echo $value_commune['id_commune'] ?>"><?php echo $value_commune['Commune'] ?></option>

                                                </label>
                                                <?php
                                            } ?>

                                        </div>
                                    </select>
                                </label>
                            </div>
                        </div>

                    <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->

        </div>
        <!-- /.modal -->


        <!-- view modal content -->

    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire
            id_view_form: 'modal_view', //id du modal qui contient le formulaire

            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('menu_etablissement/C_menu_etablissement/save')?>", //url du save (donn?es envoy?s par post)
            id_show: 'fonction_test',
            title_modal_add: 'Nouvelle categorie', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'libelle_categorie', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition de categorie', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'libelle_categorie',//l'emplacement du focus en mode edit
            colonne_show: 'nom_fonction', //la colonne a affciher lorsqu'on regarde une categorie

            title_modal_show: 'Visualisation de fonctions', //Title du modal ? l'ouverture en mode visualisation
            focus_show: 'libelle_categorie',//l'emplacement du focus en mode edit

            url_edit: "<?php echo site_url('menu_etablissement/C_menu_etablissement/get_record')?>", //url le fonction qui recup?re la donn?es de la ligne
            url_delete: "<?php echo site_url('menu_etablissement/C_menu_etablissement/delete')?>", //url de la fonction supprim?
            url_show: "<?php echo site_url('menu_etablissement/C_menu_etablissement/show')?>", //url de la fonction visualiser?
        });


    });
</script>

