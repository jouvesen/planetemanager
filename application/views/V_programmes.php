<?php btn_add_action('PROG'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des programmes</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th style="...">code_programme</th>
                        <th style="...">libelle_programme</th>
                        <th style="width: 90%">code_type_serie</th>
                        <th style="width: 90%">code_section</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>

                            <td><?php echo $value->code_programme; ?></td>
                            <td><?php echo $value->libelle_programme; ?></td>
                            <td><?php echo $value->libelle_serie; ?></td>
                            <td><?php echo $value->libelle_section; ?></td>




                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php echo btn_edit_action($value->id_programme, 'PROG'); ?>
                                <?php echo btn_delete_action($value->id_programme, 'PROG'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_programme" name="id_programme"/>
                    <div class="form-body  admin-form">

                        <div class="form-group">
                            <label class="control-label col-md-3">code programme <i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="code_programme" class="field prepend-icon">
                                    <input type="text" name="code_programme" id="code_programme"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">libelle programme</label>

                            <div class="col-md-9">
                                <label for="libelle_programme" class="field prepend-icon">
                                    <input type="text" name="libelle_programme" id="libelle_programme"
                                           class="form-control gui-input">
                                </label>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3">section</label>
                            <select  name="code section" id="code_section"
                                                 class="">
                                <option value=""></option>
                                            <?php

                                            foreach( $all_datta_section as $section)
                                            {?>
                                                <option value="<?php echo $section->code_section ;?>"><?=$section->libelle_section?> </option>

                                                <?php
                                            }
                                            ?>
                                    </select>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">serie  </label>
                            <select  name="code_type_serie" id="code_type_serie"
                                     class="">
                                <option value=""></option>
                                <?php

                                foreach( $all_data_serie as $serie)
                                {?>
                                    <option value="<?= $serie->code_type_serie?>"><?=$serie->libelle_serie?> </option>

                                    <?php
                                }
                                ?>
                            </select>

                                  </label>
                                </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
            </div>



        </div>

        </div>
        <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</form>
</div><!-- /.modal -->



<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire
            id_view_form: 'modal_view', //id du modal qui contient le formulaire


            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('C_programmes/save')?>", //url du save (donn?es envoy?s par post)
            id_show: 'cycle_test',


            title_modal_add: 'Nouvelle programme', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'libelle_programme', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition des programmes', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'libelle_programme',//l'emplacement du focus en mode edit
            colonne_show: 'nom_cycle', //la colonne a affciher lorsqu'on regarde une categorie
            title_modal_show: 'Visualisation des cycles', //Title du modal ? l'ouverture en mode visualisation
            focus_show: 'libelle_programme',//l'emplacement du focus en mode edit

            url_edit: "<?php echo site_url('C_programmes/get_record')?>", //url le fonction qui recup?re la donn?es de la ligne
            url_delete: "<?php echo site_url('C_programmes/delete')?>", //url de la fonction supprim?
            url_show: "<?php echo site_url('C_programmes/show')?>", //url de la fonction supprim?
        });
        $('#datatable-section').DataTable({
            searching: false,
            //"bInfo" : false ,
            //"bLengthChange" : false,
            language: langageFrDataTable()
        });
    });
</script>
