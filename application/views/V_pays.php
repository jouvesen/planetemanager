<?php btn_add_action('PAYS'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des pays</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th style="width: 30%">code pays</3th>
                        <th style="width: 40%">Libellé Pays (fr)</th></2th>
                        <th style="width: 40%">Nom Pays (en)</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>
                            <td><?php echo $value->code_pays; ?></td>
                            <td><?php echo $value->fr; ?></td>
                            <td><?php echo $value->en; ?></td>
                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->id_pays, 'PAYS'); ?>
                                <?php btn_delete_action($value->id_pays, 'PAYS'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_pays" name="id_pays"/>
                    <div class="form-body  admin-form">
                        <div class="form-group">
                            <label class="control-label col-md-3">code pays<i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="code_pays" class="field prepend-icon">
                                    <input type="text" name="code_pays" id="code_pays"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nom pays (fr)</label>
                                    <div class="col-md-9">
                                        <label for="fr" class="field prepend-icon">
                                            <input type="text" name="fr" id="fr"
                                                   class="form-control gui-input" required>
                                        </label>
                                    </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nom pays (en)</label>
                                            <div class="col-md-9">
                                                <label for="en_pays" class="field prepend-icon">
                                                    <input type="text" name="en" id="en"
                                                           class="form-control gui-input" required>
                                                </label>
                                            </div>




                                        </div>
                    </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->


<script type="text/javascript">
    $(document).ready(function () {
        //TableManageButtons.init();
        $(document).ready(function () {
            $('#datatable-buttons').managing_ajax({
                id_modal_form: 'modal_form', //id du modal qui contient le formulaire

                id_form: 'form', //id du formulaire
                url_submit: "<?php echo site_url('C_pays/save')?>", //url du save (donn�es envoy�s par post)

                title_modal_add: 'Nouveau pays', //Title du modal � l'ouverture en mode ajout
                focus_add: 'code_pays', //l'emplacement du focus en mode ajout

                title_modal_edit: 'Edition pays', //Title du modal � l'ouverture en mode edit
                focus_edit: 'code_pays',//l'emplacement du focus en mode edit

                url_edit: "<?php echo site_url('C_pays/get_record')?>", //url le fonction qui recup�re la donn�es de la ligne
                url_delete: "<?php echo site_url('C_pays/delete')?>", //url de la fonction supprim�
            });
        });
    });
</script>
