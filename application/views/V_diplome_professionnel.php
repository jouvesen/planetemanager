<?php btn_add_action('DIPPRO'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des diplômes professionnels</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th style="width: 50%">Libelle diplôme académique</th>
                        <th style="width: 90%">description diplôme académique</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>
                            <td><?php echo $value->libelle_diplome_pro; ?></td>
                            <td><?php echo $value->desc_diplome_pro; ?></td>
                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php btn_edit_action($value->code_diplome_pro, 'DIPPRO'); ?>
                                <?php btn_delete_action($value->code_diplome_pro, 'DIPPRO'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="code_diplome_pro" name="code_diplome_pro"/>
                    <div class="form-body  admin-form">
                        <div class="form-group">
                            <label class="control-label col-md-3">Libellé diplôme académique<i class='text-danger'>*</i></label>
                            <div class="col-md-9">
                                <label for="libelle_diplome_pro" class="field prepend-icon">
                                    <input type="text" name="libelle_diplome_pro" id="libelle_diplome_pro"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                        </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description diplôme académique</label>
                                    <div class="col-md-9">
                                        <label for="desc_diplome_pro" class="field prepend-icon">
                                            <textarea type="text" name="desc_diplome_pro" id="desc_diplome_pro"
                                                      class="form-control gui-input"> </textarea>
                                        </label>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                </div>

                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->


<script type="text/javascript">
    $(document).ready(function () {
        //TableManageButtons.init();
        $(document).ready(function () {
            $('#datatable-buttons').managing_ajax({
                id_modal_form: 'modal_form', //id du modal qui contient le formulaire

                id_form: 'form', //id du formulaire
                url_submit: "<?php echo site_url('C_diplome_professionnel/save')?>", //url du save (donn�es envoy�s par post)

                title_modal_add: 'Nouveau ', //Title du modal � l'ouverture en mode ajout
                focus_add: 'libelle_diplome_pro', //l'emplacement du focus en mode ajout

                title_modal_edit: 'Edition', //Title du modal � l'ouverture en mode edit
                focus_edit: 'libelle_diplome_pro',//l'emplacement du focus en mode edit

                url_edit: "<?php echo site_url('C_diplome_professionnel/get_record')?>", //url le fonction qui recup�re la donn�es de la ligne
                url_delete: "<?php echo site_url('C_diplome_professionnel/delete')?>", //url de la fonction supprim�
            });
        });
    });
</script>
