<?php btn_add_action('MAT'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des matieres </h3>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th style="...">Libelle matiere</th>
                        <th style="...">Description</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($all_data as $value) { ?>
                        <tr>

                            <td><?php echo $value->libelle_matiere; ?></td>
                            <td><?php echo $value->description_matiere; ?></td>



                            <td class="actions" style="width: 1%; text-align: center; white-space: nowrap">
                                <?php echo btn_edit_action($value->id_matiere, 'MAT'); ?>
                                <?php echo btn_delete_action($value->id_matiere, 'MAT'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->


<!-- sample modal content -->
<div id="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_formLabel"
     aria-hidden="true">
    <form action="#" id="form" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="modal_formLabel">Title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_matiere" name="id_matiere"/>

                    <div class="form-body  admin-form">

                        <div class="form-group">
                            <label class="control-label col-md-3">Nom <i class='text-danger'>*</i></label>

                            <div class="col-md-9">
                                <label for="nom" class="field prepend-icon">
                                    <input type="text" name="libelle_matiere" id="libelle_matiere"
                                           class="form-control gui-input" required>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>

                            <div class="col-md-9">
                                <label for="description_matiere" class="field prepend-icon">
                                    <textarea type="text" name="description_matiere" id="description_matiere"
                                              class="form-control gui-input"> </textarea>
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Discipline </label>

                            <div class="col-md-9">

                                        <table style="width: 100%" id="datatable-Discipline" class="table table-striped table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <td>discipline</td>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php foreach ($all_data_discipline as $value_discipline) { ?>
                                                <tr>
                                                    <td><input type="checkbox"
                                                               name="<?php echo $value_discipline->libelle_discipline; ?>"
                                                               id="<?php echo $value_discipline->libelle_discipline; ?>"
                                                               value="<?php echo $value_discipline->id_discipline; ?>"/><?php echo $value_discipline->libelle_discipline; ?>
                                                    </td>
                                                </tr>

                                                    <?php } ?>
                                            </tbody>

                                        </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>
        <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</form>
</div><!-- /.modal -->


<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-buttons').managing_ajax({
            id_modal_form: 'modal_form', //id du modal qui contient le formulaire

            id_form: 'form', //id du formulaire
            url_submit: "<?php echo site_url('C_matiere/save')?>", //url du save (donn?es envoy?s par post)

            title_modal_add: 'Nouvelle matiere', //Title du modal ? l'ouverture en mode ajout
            focus_add: 'libelle_matiere', //l'emplacement du focus en mode ajout

            title_modal_edit: 'Edition de matière', //Title du modal ? l'ouverture en mode edit
            focus_edit: 'libelle_matiere',//l'emplacement du focus en mode edit

            url_edit: "<?php echo site_url('C_matiere/get_record')?>", //url le fonction qui recup?re la donn?es de la ligne
            url_delete: "<?php echo site_url('C_matiere/delete')?>", //url de la fonction supprim?
        });
        $('#datatable-Discipline').DataTable({
            searching: false,
            //"bInfo" : false ,
            //"bLengthChange" : false,
            language: langageFrDataTable()
        });
    });
</script>
