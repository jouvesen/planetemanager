<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_diplome_professionnel extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_diplome_professionnel', 'diplome_professionnel');
    }

    public function index()
    {
        $data['all_data'] = $this->diplome_professionnel->get_active_data();
        $this->load->view('V_diplome_professionnel', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->diplome_professionnel->code_diplome_pro = $args[0];
        $this->diplome_professionnel->get_record();
        echo json_encode($this->diplome_professionnel, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->diplome_professionnel->code_diplome_pro = $args[0];
        echo json_encode($this->diplome_professionnel->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_diplome_pro') != '')
            $this->diplome_professionnel->code_diplome_pro = $this->input->post('code_diplome_pro');
        else
            check_unique_field('diplome_professionnel','libelle_diplome_pro', $this->input->post('libelle_diplome_pro'));


        $this->diplome_professionnel->libelle_diplome_pro = $this->input->post('libelle_diplome_pro');
        $this->diplome_professionnel->desc_diplome_pro = $this->input->post('desc_diplome_pro');
        $this->diplome_professionnel->etat_diplome_pro = '1';
        // $this->diplome_professionnel->statut_diplome_professionnel = 1;

        $result = $this->diplome_professionnel->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
