<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_programmes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_programmes', 'programme');
        $this->load->model('M_type_serie', 'serie');
        $this->load->model('M_section', 'section');
    }

    public function index()
    {
        $data['all_data'] = $this->programme->get_active_data();
        $data['all_datta_section'] = $this->section->get_active_data();
        $data['all_data_serie']= $this->serie->get_active_data();

        $this->load->view('V_programmes', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->programme->id_programme = $args[0];
        $this->programme->get_active_record();
        $s = $this->programme->get_serie_by_id($args[0]);
        $sec = $this->programme->get_section_by_id($args[0]);

        foreach($this->programme as $params=>$value_programme){
            $resultat[$params]=$value_programme;

        }


        foreach ($s as $value)
        {
            foreach ($value as $libelle => $p)
            {

                if ($libelle == "code_type_serie")
                {
                    $resultat["$p"] = 'selected';
                }


            }
        }

        foreach ($sec as $valuesec) {
            foreach ($valuesec as $nom => $p) {

                if ($nom == "code_section") {
                    $resultat["$p"] = 1;
                }


            }
        }


        echo json_encode($resultat, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->programme->id_programme = $args[0];
        echo json_encode($this->programme->delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {

            if ($this->input->post('id_programme') != '')

                $this->programme->id_programme = $this->input->post('id_programme');
            else
                check_unique_field('programmes', 'code_programme', 'code_section', 'code_type_serie','libelle_programme','', $this->input->post('code_programme'), $this->input->post('code_section'),
                    $this->input->post('code_type_serie'),$this->input->post('libelle_programme'));

            $this->programme->code_programme = $this->input->post('code_programme');
            $this->programme->code_section = $this->input->post('code_section');
            $this->programme->code_type_serie = $this->input->post('code_type_serie');
            $this->programme->libelle_programme = $this->input->post('libelle_programme');
            $this->programme->etat_programme = '1';

            $result = $this->programme->save();

            echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
}


