<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_type_equipement extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_type_equipement', 'type_equipement');
    }

    public function index()
    {
        $data['all_data'] = $this->type_equipement->get_data();
        $this->load->view('V_type_equipement', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->type_equipement->id_type_equipement = $args[0];
        $this->type_equipement->get_record();
        echo json_encode($this->type_equipement, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->type_equipement->id_type_equipement = $args[0];
        echo json_encode($this->type_equipement->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_type_equipement') != '')
            $this->type_equipement->id_type_equipement = $this->input->post('id_type_equipement');
        else
            check_unique_field('type_equipement','libelle_type_equipement', $this->input->post('libelle_type_equipement'));


        $this->type_equipement->libelle_type_equipement = $this->input->post('libelle_type_equipement');
        $this->type_equipement->etat_type_equipement = 1;

        $result = $this->type_equipement->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
