<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_matiere extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_matiere', 'matiere');
        $this->load->model('M_discipline', 'discipline');
        $this->load->model('M_discipline_matiere', 'discipline_matiere');
    }

    public function index()
    {
        $data['all_data'] = $this->matiere->get_active_data();
        $data['all_data_discipline'] = $this->discipline->get_active_data();
        $this->load->view('V_matiere', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->matiere->id_matiere = $args[0];
        $discipline = $this->discipline_matiere->get_discipline_by_id($args[0]);
        $this->matiere->get_active_record();
        foreach ($this->matiere as $param => $value_matiere) {
            $resultat[$param] = $value_matiere;
        }

        foreach ($discipline as $disciplie_value) {
            foreach ($disciplie_value as $nom => $value) {
                if ($nom == "libelle_discipline")
                    $resultat[$value] = 1;
            }
        }


        echo json_encode($resultat, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->matiere->id_matiere = $args[0];
        $this->discipline_matiere->id_matiere = $args[0];
        $this->discipline_matiere->delete();
        echo json_encode($this->matiere->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_matiere') != '')

            $this->matiere->id_matiere = $this->input->post('id_matiere');
        else
            check_unique_field($this->matiere->get_db_table(), 'libelle_matiere', 'description_matiere', $this->input->post('nom'),
                $this->input->post('description_matiere'));

        $this->matiere->libelle_matiere = $this->input->post('libelle_matiere');
        $this->matiere->etat_matiere = '1';
        $this->matiere->description_matiere = $this->input->post('description_matiere');


        $result = $this->matiere->save();

        $id = $result['id'];


        $discipline_array = $this->discipline->get_active_data();
        foreach ($discipline_array as $value_discipline) {
            $test_discipline = $this->input->post($value_discipline->libelle_discipline);
            if (isset($test_discipline)) {
                $this->discipline_matiere->id_matieredisc = Null;
                $this->discipline_matiere->id_discipline = $test_discipline;
                $this->discipline_matiere->id_matiere = $id;
                $this->discipline_matiere->save();
            }

        }

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
