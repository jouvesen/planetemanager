<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_section extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_section', 'section');
        $this->load->model('M_cycle', 'cycle');
    }

    public function index()
    {
        $data['all_data'] = $this->section->get_active_data();

        $data['all_data_cycle'] = $this->cycle->get_active_data();
        $this->load->view('V_section', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->section->code_section = $args[0];
        $this->section->get_record();
        echo json_encode($this->section, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->section->code_section = $args[0];
        echo json_encode($this->section->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_section') != '')
            $this->section->code_section = $this->input->post('code_section');
        else
            check_unique_field('param_section', 'libelle_section', $this->input->post('libelle_section'));


        $this->section->codesection = $this->input->post('codesection');
        $this->section->code_cycle = $this->input->post('code_cycle');
        $this->section->libelle_section = $this->input->post('libelle_section');
        $this->section->effectif_enseignant = $this->input->post('effectif_enseignant');
        $this->section->ordre_section = $this->input->post('ordre_section');
        $this->section->etat_section = '1';
        $this->section->description = $this->input->post('description');
        $result = $this->section->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
