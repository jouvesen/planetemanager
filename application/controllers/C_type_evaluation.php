<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_type_evaluation extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_type_evaluation', 'type_evaluation');
    }

    public function index()
    {
        $data['all_data'] = $this->type_evaluation->get_active_data();

        $this->load->view('V_type_evaluation', $data);
    }


    public function get_record()
    {
        $args = func_get_args();
        $this->type_evaluation->code_type_evaluation = $args[0];
        $this->type_evaluation->get_record();
        echo json_encode($this->type_evaluation, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    /*public function show()
    {
        $args = func_get_args();
        $id = $args[0];
        $result = $this->fonction_categorie->get_fonction_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
*/
    public function delete()
    {
        $args = func_get_args();
        $this->type_evaluation->code_type_evaluation = $args[0];
        echo json_encode($this->type_evaluation->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_type_evaluation') != '')
            $this->type_evaluation->code_type_evaluation = $this->input->post('code_type_evaluation');
        else
            check_unique_field('type_evaluation', 'type_evaluation', $this->input->post('type_evaluation'));

        $this->type_evaluation->type_evaluation = $this->input->post('type_evaluation');

        $this->type_evaluation->etat_etype_evaluation = '1';

        $result = $this->type_evaluation->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
