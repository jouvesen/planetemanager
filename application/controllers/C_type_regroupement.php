<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_type_regroupement extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_type_regroupement', 'type_regroupement');
    }

    public function index()
    {
        $data['all_data'] = $this->type_regroupement->get_active_data();
        $this->load->view('V_type_regroupement', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->type_regroupement->id_regroupement = $args[0];
        $this->type_regroupement->get_record();
        echo json_encode($this->type_regroupement, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->type_regroupement->id_regroupement = $args[0];
        echo json_encode($this->type_regroupement->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_regroupement') != '')
            $this->type_regroupement->code_type_regroupement = $this->input->post('id_regroupement');
        else
            check_unique_field('type_regroupement','regroupement', $this->input->post('regroupement'));


        $this->type_regroupement->regroupement = $this->input->post('regroupement');
        $this->type_regroupement->ordre = $this->input->post('ordre');
        $this->type_regroupement->statut_type_regroupement = '1';

        $result = $this->type_regroupement->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
