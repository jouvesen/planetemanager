<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_discipline extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_discipline', 'discipline');
        $this->load->model('M_discipline_cycle', 'discipline_cycle');
        $this->load->model('M_discipline_matiere', 'discipline_matiere');
        $this->load->model('M_cycle', 'cycle');
    }

    public function index()
    {
        $data['all_data'] = $this->discipline->get_active_data();
        $data['cycle'] = $this->cycle->get_active_data();
        $this->load->view('V_discipline', $data);

    }

    public function get_record()
    {
        $args = func_get_args();
        $this->discipline->id_discipline = $args[0];
        $this->discipline->get_active_record();
        $cycle = $this->discipline_cycle->get_discipline_by_id($args[0]);
        foreach ($this->discipline as $param => $value_discipline) {
            $resultat[$param] = $value_discipline;
        }

        foreach ($cycle as $value_cycle) {
            foreach ($value_cycle as $nom => $value) {
                if ($nom == "nom_cycle") {
                    $resultat[$value] = 1;
            }
            }

        }

        echo json_encode($resultat, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
    public function show(){

        $args = func_get_args();
        $id = $args[0];

        $result = $this->discipline_matiere->get_data_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

    }

    public function delete()
    {
        $args = func_get_args();
        $this->discipline->id_discipline = $args[0];
        $this->discipline_cycle->id_discipline = $args[0];
        $this->discipline_cycle->delete();

        echo json_encode($this->discipline->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_discipline') != '')
            $this->discipline->id_discipline = $this->input->post('id_discipline');


        $this->discipline->code_discipline = $this->input->post('code_discipline');
        $this->discipline->libelle_discipline = $this->input->post('libelle_discipline');
        $this->discipline->couleur_discipline = $this->input->post('couleur_discipline');
        $this->discipline->composition_matiere = $this->input->post('composition_matiere');
        $this->discipline->description = $this->input->post('description');
        $this->discipline->etat_discipline = "1";
        $result = $this->discipline->save();
        $id = $result['id'];

        $cycle_array = $this->cycle->get_active_data();
        foreach ($cycle_array as $value_cycle) {
            $test_cycle = $this->input->post($value_cycle->nom_cycle);
            if (isset($test_cycle)) {
                $this->discipline_cycle->id_discipline_cycle = Null;
                $this->discipline_cycle->id_discipline = $id;
                $this->discipline_cycle->code_cycle = $test_cycle;
                $this->discipline_cycle->save();
            }


        }

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
