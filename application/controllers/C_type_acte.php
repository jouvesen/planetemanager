<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_type_acte extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_type_acte', 'type_acte');
    }

    public function index()
    {
        $data['all_data'] = $this->type_acte->get_active_data();
        $this->load->view('V_type_acte', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->type_acte->id_type_acte = $args[0];
        $this->type_acte->get_record();
        echo json_encode($this->type_acte, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->type_acte->id_type_acte = $args[0];
        echo json_encode($this->type_acte->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_type_acte') != '')
            $this->type_acte->id_type_acte = $this->input->post('id_type_acte');
        else
            check_unique_field('type_acte', 'libelle_type_acte', 'description', $this->input->post('libelle_type_acte'), $this->input->post('description'));


        $this->type_acte->libelle_type_acte = $this->input->post('libelle_type_acte');
        $this->type_acte->etat_type_acte = '1';
        $this->type_acte->description = $this->input->post('description');

        $result = $this->type_acte->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
