<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_type_orphelin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_type_orphelin', 'type_orphelin');
    }

    public function index()
    {
        $data['all_data'] = $this->type_orphelin->get_active_data();
        $this->load->view('V_type_orphelin', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->type_orphelin->id_orphelin = $args[0];
        $this->type_orphelin->get_record();
        echo json_encode($this->type_orphelin, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->type_orphelin->id_orphelin = $args[0];
        echo json_encode($this->type_orphelin->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_orphelin') != '')
            $this->type_orphelin->id_orphelin = $this->input->post('id_orphelin');
        else
            check_unique_field('type_orphelin','libelle_orphelin', $this->input->post('libelle_orphelin'));


        $this->type_orphelin->libelle_orphelin = $this->input->post('libelle_orphelin');
        $this->type_orphelin->etat_orphelin = '1';
        $result = $this->type_orphelin->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
