<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_diplome_academique extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_diplome_academique', 'diplome_academique');
    }

    public function index()
    {
        $data['all_data'] = $this->diplome_academique->get_active_data();
        $this->load->view('V_diplome_academique', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->diplome_academique->code_diplome_ac = $args[0];
        $this->diplome_academique->get_record();
        echo json_encode($this->diplome_academique, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->diplome_academique->code_diplome_ac = $args[0];
        echo json_encode($this->diplome_academique->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_diplome_ac') != '')
            $this->diplome_academique->code_diplome_ac = $this->input->post('code_diplome_ac');
        else
            check_unique_field('diplome_academique','libelle_diplome_ac', $this->input->post('libelle_diplome_ac'));


        $this->diplome_academique->libelle_diplome_ac = $this->input->post('libelle_diplome_ac');
        $this->diplome_academique->desc_diplome_ac = $this->input->post('desc_diplome_ac');
        $this->diplome_academique->etat_diplome_ac = '1';
        // $this->diplome_academique->statut_diplome_academique = 1;

        $result = $this->diplome_academique->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
