<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_grade extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_grade', 'grade');
        $this->load->model('M_corps', 'corps');
        $this->load->model('M_corps_grade', 'corps_grade');
    }

    public function index()
    {
        $data['all_data'] = $this->grade->get_active_data();
        $data['all_data_corps'] = $this->corps->get_active_data();

        $this->load->view('V_grade', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->grade->code_grade = $args[0];
        $this->grade->get_active_record();
        $resultat=array();
        $lst_corps = $this->corps_grade->get_grade_by_id($args[0]);
        foreach ($this->grade as $param=>$value) {
            $resultat[$param] = $value;
        }

        foreach($lst_corps as $values)
        {
            foreach($values as $params=>$k)
            {
                if ($params == "code_corps")
                {
                    $resultat[$k] = 1;
                }

            }

        }


        echo json_encode($resultat, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function show()
    {

        $args = func_get_args();
        $id = $args[0];
        $result = $this->corps_grade->get_grade_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

    }

    public function delete()
    {
        $args = func_get_args();
        $this->grade->code_grade = $args[0];
        echo json_encode($this->grade->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }



       public function save()
    {
        if ($this->input->post('code_grade') != '')
            $this->grade->code_grade = $this->input->post('code_grade');
        else
            check_unique_field($this->grade->get_db_table(), 'nom_grade', 'code_corps', $this->input->post('nom_grade'), $this->input->post('code_corps'));

        $this->grade->nom_grade = $this->input->post('nom_grade');

        $this->grade->etat_grade = '1';
        $this->grade->description = $this->input->post('description');

        $result = $this->grade->save();

        $id = $result['id'];



        //save checkbox
        $list_corps = $this->corps->get_active_data();
        //suprimer toute entrées de la base avec un code_corps coché
        $this->corps_grade->inactive_entries_grade($id);
        if(count($list_corps)!=0)
            foreach ($list_corps as $value_corps) {
                $test_corps = $this->input->post($value_corps->code_corps);

                if (isset($test_corps)) {
                    $ligne_test = $this->corps_grade->get_corps_grade_by_id($test_corps, $id);
                    if (count($ligne_test) != 0) {
                        $id_corps_grade = $ligne_test[0]->id_corps_grade;
                        $code_corps = $ligne_test[0]->code_corps;


                    } else {
                        $id_corps_grade = Null;
                        $code_corps = $test_corps;

                    }
                    $this->corps_grade->id_corps_grade = $id_corps_grade;
                    $this->corps_grade->code_corps = $code_corps;
                    $this->corps_grade->code_grade = $id;
                    $this->corps_grade->etat_corps_grade = "1";
                    $this->corps_grade->save();
                }

            }


        /*$grade_array = $this->grade->get_active_data();
        foreach ($grade_array as $value_grade) {
            $test_grade = $this->input->post($value_grade->nom_grade);
            if (isset($test_grade)) {


            $this->corps_grade->id_corps_grade = Null;
            $this->corps_grade->code_corps = $id;
            $this->corps_grade->code_grade = $test_grade;
            $this->corps_grade->save();
        }}*/
echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
}

}
