<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_categorie_evaluation extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_categorie_evaluation', 'categorie_evaluation');
    }

    public function index()
    {
        $data['all_data'] = $this->categorie_evaluation->get_active_data();

        $this->load->view('V_categorie_evaluation', $data);
    }


    public function get_record()
    {
        $args = func_get_args();
        $this->categorie_evaluation->id_categorie_evaluation = $args[0];
        $this->categorie_evaluation->get_record();
        echo json_encode($this->categorie_evaluation, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    /*public function show()
    {
        $args = func_get_args();
        $id = $args[0];
        $result = $this->fonction_categorie->get_fonction_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
*/
    public function delete()
    {
        $args = func_get_args();
        $this->categorie_evaluation->id_categorie_evaluation = $args[0];
        echo json_encode($this->categorie_evaluation->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_categorie_evaluation') != '')
            $this->categorie_evaluation->id_categorie_evaluation = $this->input->post('id_categorie_evaluation');
        else
            check_unique_field('categorie_evaluation', 'code_categorie_eval', 'libelle_categorie_evaluation', $this->input->post('code_categorie_eval'), $this->input->post('libelle_categorie_evaluation'));

        $this->categorie_evaluation->code_categorie_eval = $this->input->post('code_categorie_eval');

        $this->categorie_evaluation->libelle_categorie_evaluation = $this->input->post('libelle_categorie_evaluation');

        $this->categorie_evaluation->etat_categorie_evaluation = '1';

        $result = $this->categorie_evaluation->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
