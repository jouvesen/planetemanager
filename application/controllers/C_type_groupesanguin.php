<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_type_groupesanguin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_type_groupesanguin', 'type_groupesanguin');
    }

    public function index()
    {
        $data['all_data'] = $this->type_groupesanguin->get_active_data();
        $this->load->view('V_type_groupesanguin', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->type_groupesanguin->id_gsanguin= $args[0];
        $this->type_groupesanguin->get_record();
        echo json_encode($this->type_groupesanguin, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->type_groupesanguin->id_gsanguin = $args[0];
        echo json_encode($this->type_groupesanguin->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_gsanguin') != '')
            $this->type_groupesanguin->id_gsanguin = $this->input->post('id_gsanguin');
        else
            check_unique_field('type_groupesanguin','libelle_gsanguin','description', $this->input->post('libelle_gsanguin'),$this->input->post('description'));


        $this->type_groupesanguin->libelle_gsanguin = $this->input->post('libelle_gsanguin');
        $this->type_groupesanguin->description = $this->input->post('description');
        $this->type_groupesanguin->etat_gs = '1';


        $result = $this->type_groupesanguin->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
