<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_statut_juridique extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_statut_juridique', 'statut_juridique');
    }

    public function index()
    {
        $data['all_data'] = $this->statut_juridique->get_active_data();
        $this->load->view('V_statut_juridique', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->statut_juridique->code_sj = $args[0];
        $this->statut_juridique->get_record();
        echo json_encode($this->statut_juridique, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->statut_juridique->code_sj = $args[0];
        echo json_encode($this->statut_juridique->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_sj') != '')
            $this->statut_juridique->code_sj = $this->input->post('code_sj');
        else
            check_unique_field('statut_juridique', 'libelle_sj', 'description_js', $this->input->post('libelle_sj'), $this->input->post('description_sj'));


        $this->statut_juridique->libelle_sj = $this->input->post('libelle_sj');
        $this->statut_juridique->description_sj = $this->input->post('description_sj');
        $this->statut_juridique->etat_sj = '1';
        // $this->statut_juridique->statut_statut_juridique = 1;

        $result = $this->statut_juridique->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
