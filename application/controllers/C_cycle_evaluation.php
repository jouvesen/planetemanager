<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_cycle_evaluation extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_cycle_evaluation', 'cycle_evaluation');
        $this->load->model('M_cycle', 'cycle');
        $this->load->model('M_type_evaluation', 'type_evaluation');
    }

    public function index()
    {
        $data['all_data'] = $this->cycle_evaluation->get_active_data();
        $data['all_data_cycle'] = $this->cycle->get_active_data();
        $data['all_data_type_evaluation'] = $this->type_evaluation->get_active_data();

        $this->load->view('V_cycle_evaluation', $data);
    }


    public function get_record()
    {
        $args = func_get_args();
        $this->cycle_evaluation->id_evaluation_cycle = $args[0];
        $this->cycle_evaluation->get_record();
        echo json_encode($this->cycle_evaluation, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    /*public function show()
    {
        $args = func_get_args();
        $id = $args[0];
        $result = $this->fonction_categorie->get_fonction_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
*/
    public function delete()
    {
        $args = func_get_args();
        $this->cycle_evaluation->id_evaluation_cycle = $args[0];
        echo json_encode($this->cycle_evaluation->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_evaluation_cycle') != '')
            $this->cycle_evaluation->id_evaluation_cycle = $this->input->post('id_evaluation_cycle');
        else
            check_unique_field('type_periode', 'libelle_periode', $this->input->post('libelle_periode'));

        $this->cycle_evaluation->code_cycle = $this->input->post('code_cycle');
        $this->cycle_evaluation->code_type_evaluation = $this->input->post('code_type_evaluation');
        $this->cycle_evaluation->note_max = $this->input->post('note_max');
        $this->cycle_evaluation->type_val = $this->input->post('type_val');
        $this->cycle_evaluation->nombre = $this->input->post('nombre');

        $this->cycle_evaluation->etat_evaluationcycle = '1';

        $result = $this->cycle_evaluation->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
