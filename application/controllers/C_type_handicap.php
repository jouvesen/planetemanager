<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_type_handicap extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_type_handicap', 'type_handicap');
    }

    public function index()
    {
        $data['all_data'] = $this->type_handicap->get_active_data();
        $this->load->view('V_type_handicap', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->type_handicap->id_handicap = $args[0];
        $this->type_handicap->get_record();
        echo json_encode($this->type_handicap, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->type_handicap->id_handicap = $args[0];
        echo json_encode($this->type_handicap->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_type_handicap') != '')
            $this->type_handicap->id_handicap = $this->input->post('id_handicap');
        else
            check_unique_field('type_handicap','libelle_handicap',description, $this->input->post('libelle_handicap'),$this->input->post('description'));


        $this->type_handicap->libelle_handicap = $this->input->post('libelle_handicap');
        $this->type_handicap->description = $this->input->post('description');
        $this->type_handicap->etat_handicap = '1';


        $result = $this->type_handicap->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
