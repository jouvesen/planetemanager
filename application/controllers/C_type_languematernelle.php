<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_type_languematernelle extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_type_languematernelle', 'type_languematernelle');
    }

    public function index()
    {
        $data['all_data'] = $this->type_languematernelle->get_active_data();
        $this->load->view('V_type_languematernelle', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->type_languematernelle->id_languemater = $args[0];
        $this->type_languematernelle->get_record();
        echo json_encode($this->type_languematernelle, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->type_languematernelle->id_languemater = $args[0];
        echo json_encode($this->type_languematernelle->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_languemater') != '')
            $this->type_languematernelle->id_languemater = $this->input->post('id_languemater');
        else
            check_unique_field('type_languematernelle','libelle_languemater',description, $this->input->post('libelle_languemater'),$this->input->post('description'));


        $this->type_languematernelle->libelle_languemater = $this->input->post('libelle_languemater');
        $this->type_languematernelle->description = $this->input->post('description');
        $this->type_languematernelle->etat_languemater='1';

        $result = $this->type_languematernelle->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
