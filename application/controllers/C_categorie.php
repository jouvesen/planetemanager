<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_categorie extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_categorie', 'categorie');
        $this->load->model('M_types_fonction_categorie', 'fonction_categorie');
    }

    public function index()
    {
        $data['all_data'] = $this->categorie->get_data();

        $this->load->view('V_categorie', $data);
    }
   /* public function vue(){
        $args = func_get_args();
        $id = $args[0];
        $result = $this->fonction_categorie->get_data_by_id($id);
        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);


    }*/

    public function get_record()
    {
        $args = func_get_args();
        $this->categorie->code_categorie = $args[0];
        $this->categorie->get_record();
        echo json_encode($this->categorie, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function show()
    {
        $args = func_get_args();
        $id = $args[0];
        $result = $this->fonction_categorie->get_fonction_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->categorie->code_categorie = $args[0];
        echo json_encode($this->categorie->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_categorie') != '')
            $this->categorie->code_categorie = $this->input->post('code_categorie');
        else
            check_unique_field('categorie_structure', 'libelle_categorie', $this->input->post('libelle_categorie'));

        $this->categorie->libelle_categorie = $this->input->post('libelle_categorie');

        // $this->cycle->statut_cycle = 1;

        $result = $this->categorie->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
