<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_type_periode extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_type_periode', 'type_periode');
    }

    public function index()
    {
        $data['all_data'] = $this->type_periode->get_active_data();

        $this->load->view('V_type_periode', $data);
    }


    public function get_record()
    {
        $args = func_get_args();
        $this->type_periode->code_type_periode = $args[0];
        $this->type_periode->get_record();
        echo json_encode($this->type_periode, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    /*public function show()
    {
        $args = func_get_args();
        $id = $args[0];
        $result = $this->fonction_categorie->get_fonction_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
*/
    public function delete()
    {
        $args = func_get_args();
        $this->type_periode->code_type_periode = $args[0];
        echo json_encode($this->type_periode->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_type_periode') != '')
            $this->type_periode->code_type_periode = $this->input->post('code_type_periode');
        else
            check_unique_field('type_periode', 'libelle_periode', $this->input->post('libelle_periode'));

        $this->type_periode->libelle_periode = $this->input->post('libelle_periode');

        $this->type_periode->etat_periode = '1';

        $result = $this->type_periode->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
