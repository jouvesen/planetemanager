<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_syndicat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_syndicat', 'syndicat');
    }

    public function index()
    {
        $data['all_data'] = $this->syndicat->get_data();
        $this->load->view('V_syndicat', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->syndicat->id_syndicat = $args[0];
        $this->syndicat->get_record();
        echo json_encode($this->syndicat, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->syndicat->id_syndicat = $args[0];
        echo json_encode($this->syndicat->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_syndicat') != '')
            $this->syndicat->id_syndicat = $this->input->post('id_syndicat');


        $this->syndicat->code_syndicat = $this->input->post('code_syndicat');
        $this->syndicat->nom_syndicat = $this->input->post('nom_syndicat');
        $this->syndicat->nom_sg = $this->input->post('nom_sg');
        $this->syndicat->tel_sg = $this->input->post('tel_sg');
        $this->syndicat->email_sg = $this->input->post('email_sg');
        // $this->syndicat->statut_syndicat = 1;

        $result = $this->syndicat->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
