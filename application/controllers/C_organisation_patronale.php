<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_organisation_patronale extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_organisation_patronale', 'organisation_patronale');
    }

    public function index()
    {
        $data['all_data'] = $this->organisation_patronale->get_active_data();
        $this->load->view('V_organisation_patronale', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->organisation_patronale->code_op = $args[0];
        $this->organisation_patronale->get_record();
        echo json_encode($this->organisation_patronale, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->organisation_patronale->code_op = $args[0];
        echo json_encode($this->organisation_patronale->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_op') != '')
            $this->organisation_patronale->code_op= $this->input->post('code_op');
        else
            check_unique_field('organisation_patronale','nom_op', $this->input->post('nom_op'));


        $this->organisation_patronale->nom_op = $this->input->post('nom_op');
        $this->organisation_patronale->description = $this->input->post('description');
        $this->organisation_patronale->etat_op = '1';

        // $this->organisation_patronale->statut_organisation_patronale = 1;

        $result = $this->organisation_patronale->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
