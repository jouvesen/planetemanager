<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_api extends CI_Controller
{

    public function index_get()
    {
        // Code permettant de faire fonctionner l'API
        //http://stackoverflow.com/questions/18382740/cors-not-working-php
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                header("Access-Control-Allow-Methods: OPTIONS");

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

            exit(0);
        }

        //$data['all_data'] = $this->grade->get_active_data();

        /* Partie permettant de definir l'action a effectuer par l'API
        * Il y aura donc une fonction d'affichage et une d'ajout
        */
        $arg = func_get_args();
        $nbr_argument = count($arg);
        if ($nbr_argument <= 1) {
            send_all($arg[0]);
        } elseif ($nbr_argument <= 2) {


            get_by_id($arg[0], $arg[1]);
        } else {
            echo 'trop d\'arguments.';
        }

//
//        }
    }
    // La variable keyword permet de connaitre l'action a effectuer (affichage, ajout, etc.)
    // La variable requete contient la requete de la part de l'application (par exemple un id de produit)







}
