<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_specialite extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_specialite', 'specialite');
        $this->load->model('M_cycle', 'cycle');
        $this->load->model('M_discipline', 'discipline');
        $this->load->model('M_specialite_cycle', 'specialite_cycle');
        $this->load->model('M_specialite_discipline', 'specialite_discipline');
        $this->load->model('M_specialite_matiere','specialite_matiere');
    }

    public function index()
    {
        $data['all_data'] = $this->specialite->get_active_data();
        $data['all_data_cycle'] = $this->cycle->get_active_data();
        $data['all_data_discipline'] = $this->discipline->get_active_data();
        $this->load->view('V_specialite', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->specialite->code_specialite = $args[0];
        $spec_cycle = $this->specialite_cycle->get_specialite_by_id($args[0]);
        $spec_discipline = $this->specialite_discipline->get_descipline_by_id($args[0]);
        $this->specialite->get_active_record();
        foreach ($this->specialite as $params => $value_specialite) {
            $resultat[$params] = $value_specialite;
        }

        foreach ($spec_cycle as $value)
        {
            foreach ($value as $nom => $p)
            {

                if ($nom == "nom_cycle")
                {
                    $resultat["$p"] = 1;
                }


            }
        }

        foreach ($spec_discipline as $valuediscilpline) {
            foreach ($valuediscilpline as $nom => $p) {

                if ($nom == "code_discipline") {
                    $resultat["$p"] = 1;
                }


            }
        }


        echo json_encode($resultat, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
    public function show(){

        $args = func_get_args();
        $id = $args[0];

        $result = $this->specialite_matiere->get_data_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

    }

    public function delete()
    {
        $args = func_get_args();
        $this->specialite->code_specialite = $args[0];
        $this->specialite_cycle->code_specialite = $args[0];
        $this->specialite_cycle->delete();
        echo json_encode($this->specialite->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_specialite') != '')

            $this->specialite->code_specialite = $this->input->post('code_specialite');
        else
            check_unique_field('param_specialite', 'nom_specialite', 'code_spec', 'description', $this->input->post('nom_specialite'), $this->input->post('code_spec'),
                $this->input->post('description'));

        $this->specialite->nom_specialite = $this->input->post('nom_specialite');
        $this->specialite->code_spec = $this->input->post('code_spec');
        $this->specialite->description = $this->input->post('description');
        $this->specialite->etat_specialite = '1';

        $result = $this->specialite->save();

        $id = $result['id'];
        $this->specialite_cycle->inactive_entries($id);
        $data_cycle = $this->cycle->get_active_data();
        foreach ($data_cycle as $value_cycle) {
            $test_param = $this->input->post($value_cycle->nom_cycle);
            if (isset($test_param)) {
                $ligne_test = $this->specialite_cycle->get_specialite_cycle_by_id($test_param, $id);
                if (count($ligne_test) != 0) {
                    $code_spec_cycle = $ligne_test[0]->code_spec_cycle;

                } else {
                    $code_spec_cycle = Null;
                }

                $this->specialite_cycle->code_spec_cycle = $code_spec_cycle;
                $this->specialite_cycle->code_specialite = $id;
                $this->specialite_cycle->code_cycle = $value_cycle->code_cycle;
                $this->specialite_cycle->etat_specialite_cycle = '1';
                $this->specialite_cycle->save();
            }

        }

        $this->specialite_discipline->inactive_entries($id);
        $data_discipline = $this->discipline->get_active_data();
        foreach ($data_discipline as $value_discipline) {
            $test_discipline = $this->input->post($value_discipline->code_discipline);
            if (isset($test_discipline)) {
                $ligne_tests = $this->specialite_discipline->get_specialite_discipline_by_id($test_discipline, $id);
                if (count($ligne_tests) != 0) {
                    $code_spm = $ligne_tests[0]->code_spm;

                } else {
                    $code_spm = Null;
                }
                $this->specialite_discipline->code_spm = $code_spm;
                $this->specialite_discipline->code_specialite = $id;
                $this->specialite_discipline->id_discipline = $value_discipline->id_discipline;
                $this->specialite_discipline->etat_specialite_discipline = '1';
                $this->specialite_discipline->save();
            }

        }


        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
