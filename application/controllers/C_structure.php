<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_structure extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_structure', 'structure');
    }

    public function index()
    {
        $data['all_data'] = $this->structure->get_active_data();
        $this->load->view('V_structure', $data);

    }

    public function get_record()
    {
        $args = func_get_args();
        $this->structure->code_str = $args[0];
        $this->structure->get_record();

        echo json_encode($this->structure, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->structure->code_str = $args[0];
        echo json_encode($this->structure->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_str') != '')
            $this->structure->code_str = $this->input->post('code_str');
        else
            check_unique_field('structure','libelle_structure', $this->input->post('libelle_structure'));

        $data_struct_api = get_api_data('http://localhost/planetemanager/api/simenv1/cycle');
        $this->structure->libelle_structure = $this->input->post('libelle_structure');
        $this->structure->date_creation_str = $this->input->post('date_creation_str');
        $this->structure->email_str = $this->input->post('email_str');
        $this->structure->tel_str = $this->input->post('tel_str');
        $this->structure->etat_str = $this->input->post('etat_str');
        $this->structure->date_fermuture = $this->input->post('date_fermuture');
        $this->structure->arrete_ferme= $this->input->post('arrete_ferme');

        // $this->structure->id_saisie = 1; declar� en int
        //$this->structure->date_insert=$this->input->post('date_insert'); declar� en timestamp

        $result = $this->structure->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
