<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_menu_etablissement extends CI_Controller
{
    public $url_api = "http://codeco.simendev.com/";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_cycle', 'cycle');
    }

    public function index()
    {
        $url_ia = $this->url_api_ia_list();
        $data['ia_value'] = get_api_data($url_ia, $records = "resultat");
        $data['ief'] = get_api_data($this->url_api_ief_list(), $records = "resultat");
        $data['commune'] = get_api_data($this->url_api_commune_list(), $records = "resultat");

        $data['cycle'] = $this->cycle->get_active_data();
        $this->load->view("menu_etablissement/V_menu_etablissement", $data);
    }

    public function url_api_ia_list($requete = "")
    {
        if ($requete == "") {

            return $this->url_api . 'list-ia';
        } else {
            return $this->url_api . 'req-one-ia/' . $requete;
        }
    }

    public function url_api_ief_list($requete = "")
    {
        if ($requete == "") {
            return $this->url_api . 'list-ief';
        } else {
            return $this->url_api . 'req-one-ief/' . $requete;
        }
    }

    public function url_api_commune_list($requete = "")
    {
        if ($requete == "") {
            return $this->url_api . 'list-commune';
        } else {
            return $this->url_api . 'get-data-commune/' . $requete;
        }

    }

    public function get_relaod_data_ia()
    {
        $args = func_get_args();


        $id_ia = $args[0];

        $ia = get_api_data($this->url_api_ia_list($id_ia), $records = "resultat");

        $donnees = $this->return_data($ia, "id_ia");


        $this->load->view("menu_etablissement/V_header_dashbord", $donnees);

    }

    public function return_data($classe, $id)
    {
        $result = array();
        if ($classe != 'Pas de résultat') {
            foreach ($classe as $param => $value) {

                switch ($value->code_cycle) {
                    case "9": {
                        $result[0]['id_ief'] = $value->$id;
                        $result[0]['nom_cycle'] = "Prescolaire";
                        $result[0]['image_cycle'] = "ion-university";
                        if ($value->statut == "Public") {
                            $result[0]['presc_pub'] = $value->nbr_etablissement;
                        } elseif ($value->statut == "Privé") {
                            $result[0]['presc_priv'] = $value->nbr_etablissement;
                        } else {
                            $result[0]['presc_comm_ass'] = $value->nbr_etablissement;
                        }

                        break;

                    }
                    case "10": {
                        $result[1]['id_ief'] = $value->$id;
                        $result[1]['nom_cycle'] = "Elementaire";
                        $result[1]['image_cycle'] = "ion-university";
                        if ($value->statut == "Public") {
                            $result[1]['elem_pub'] = $value->nbr_etablissement;
                        } elseif ($value->statut == "Privé") {
                            $result[1]['elem_priv'] = $value->nbr_etablissement;
                        } else {
                            $result[1]['elem_comm_ass'] = $value->nbr_etablissement;
                        }

                        break;

                    }
                    case "11": {
                        $result[2]['id_ief'] = $value->$id;
                        $result[2]['nom_cycle'] = "Moyen";
                        $result[2]['image_cycle'] = "ion-university";
                        if ($value->statut == "Public") {
                            $result[2]['moy_pub'] = $value->nbr_etablissement;
                        } elseif ($value->statut == "Privé") {
                            $result[2]['moy_priv'] = $value->nbr_etablissement;
                        } else {
                            $result[2]['moy_comm_ass'] = $value->nbr_etablissement;
                        }

                        break;

                    }
                    case "12": {
                        $result[3]['id_ief'] = $value->$id;
                        $result[3]['nom_cycle'] = "Secondaire";
                        $result[3]['image_cycle'] = "ion-university";
                        if ($value->statut == "Public") {
                            $result[3]['sec_pub'] = $value->nbr_etablissement;
                        } elseif ($value->statut == "Privé") {
                            $result[3]['sec_priv'] = $value->nbr_etablissement;
                        } else {
                            $result[3]['sec_comm_ass'] = $value->nbr_etablissement;
                        }
                        break;


                    }
                    case "13": {
                        $result[4]['id_ief'] = $value->$id;
                        $result[4]['nom_cycle'] = "Moyen Secondaire";
                        $result[4]['image_cycle'] = "ion-university";
                        if ($value->statut == "Public") {
                            $result[4]['moy_sec_pub'] = $value->nbr_etablissement;
                        } elseif ($value->statut == "Privé") {
                            $result[4]['moy_sec_priv'] = $value->nbr_etablissement;
                        } else {
                            $result[4]['moy_sec_com_ass'] = $value->nbr_etablissement;
                        }

                        break;

                    }
                    case "14": {
                        $result[5]['id_ief'] = $value->$id;
                        $result[5]['nom_cycle'] = "Daara";
                        $result[5]['image_cycle'] = "ion-university";
                        if ($value->statut == "Public") {
                            $result[5]['daara_pub'] = $value->nbr_etablissement;
                        } elseif ($value->statut == "Privé") {
                            $result[5]['daara_priv'] = $value->nbr_etablissement;
                        } else {
                            $result[5]['daara_com_ass'] = $value->nbr_etablissement;
                        }
                        break;

                    }
                }
            }
        }

        $donnees['cycle'] = $result;
        return $donnees;
    }

    public function get_relaod_data_ief()
    {
        $args = func_get_args();

        if (count($args) == 0) return;
        $id_ief = $args[0];


        $array_ief = get_api_data($this->url_api_ief_list($id_ief), $records = "resultat");
        $donnees = $this->return_data($array_ief, "id_ief");

        $this->load->view("menu_etablissement/V_header_dashbord", $donnees);

    }

    public function get_relaod_data_commune()
    {
        $args = func_get_args();
        if (count($args) == 0) return;


        $id_commune = $args[0];

        $array_commune = get_api_data($this->url_api_ief_list($id_commune), $records = "resultat");


        $donnees = $this->return_data($array_commune, "id_commune");

        $this->load->view("menu_etablissement/V_header_dashbord", $donnees);

    }

    public function delete()
    {
        $args = func_get_args();
        $this->cycle->code_cycle = $args[0];
        echo json_encode($this->cycle->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }



}
