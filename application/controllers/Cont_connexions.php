<?php
class Cont_connexions extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();	
	    //initialisation de la session	
		$this->load->library('session');
		// $this->load->model('Global_bdd');
		//
		$this->load->model('M_personnel_etablissement');
		$this->load->model('M_table_param');
		$this->load->model('connexions_model');		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->library('javascript');
		$this->load->model('sys/M_sys_role', 'role');
		//$this->var['css'] = array();
		//	$this->var['js'] = array();
		
		$datas_auth_adm = array(
			'topto'			=> '',
		   	'is_linked'  	=> 'mat',	   
	   		'sama_title'	=> 'FCL Dotations'
	    );
	   	 $this->session->set_userdata($datas_auth_adm);
		 //
		 $this->folder_view 	= 'news';
	}
//se connecter
	public function verif_connexion()
	{       
        //si l'url contient deja index.php		 
//		 $pos = strpos($_SERVER['REQUEST_URI'], 'index.php/');
//		 if ($pos !== false) {
			$suite_req=site_url();
//		} 
//		else {
//			$suite_req='index.php/';
//		}

		$elemen_cours = $this->M_table_param->get_annne_encours();
		$data['connexions_item'] = $this->connexions_model->test_connexion($elemen_cours['annee_cours']);
 //var_dump($data['connexions_item']);break;
		
		/*$this->form_validation->set_rules('g-recaptcha-response','Captcha','callback_recaptcha');*/
//		if(empty( $data['connexions_item']["page_bidar_"]))
//		{
//		  header("Location:".$suite_req."sign-in?erreur=login");
//		}
//		else {
		
		/*$data['connexions_item'] = $this->connexions_model->test_connexion();*/
			if (empty($data['connexions_item'])) 
			// Il n'y a aucun utilisateur avec ces donn�es (ou login et/ou mot de passe incorrects)
			{
				//on log les donn�es begin
					$the_data = array(
						'ip' 			=> $_SERVER['REMOTE_ADDR'] ,
						'navigateur' 	=> $_SERVER['HTTP_USER_AGENT'],
						'login' 		=> str_replace("'","",$this->input->post('username'))
							
					);
			
			////$this->Global_bdd->insert_one_key("z_log_error_connexions", $the_data);
							
				//on log les erreurs
				header("Location:".$suite_req."sign-in?erreur=login");
			}
			else{  
				//var_dump($data['connexions_item']);
				//login et mot de passe corrects
				//enregistrement des donn�es de l'utilisateur dans la session

				//$ens_element = $this->M_personnel_etablissement->get_data_liste_by_ens($data['connexions_item']['ien']);
				$datas_user = array(
					'lfc_jafr12_s'=> array(
						 
						   'nom'		=> $data['connexions_item']['id_bid'],
						   'id'			=> $data['connexions_item']['page_bidar_'],
						   'ien'		=> $data['connexions_item']['ien'],
						   'profil'     => $data['connexions_item']['id_profil'],
							//'ans'      => $elemen_cours['annee_cours'],
							//'code_str'      => $data['connexions_item']['code_str'],
							'libelle_annee'      => $elemen_cours['libelle_annee'],
						  // 'statut'     => $data['connexions_item']['statut'],
						   'logged_in' 	=> TRUE
						)
					);

				//donn�es en session
				$this->session->set_userdata($datas_user);	
				$this->session->set_userdata('ans',$elemen_cours['annee_cours']);
				$this->session->set_userdata('code_str',$data['connexions_item']['code_str']);
				$this->session->set_userdata('code_str',$data['connexions_item']['code_str']);
				//$this->session->set_userdata('libelle_structure', $data['connexions_item']['libelle_structure']);
				$this->session->set_userdata('id_ens', $data['connexions_item']['id_ens']);
				//on log les donn�es begin : Enregistrement des donnees de l'utilisateur dans la table z_connexions ip, navigateur, profil, nom 
				$the_data = array( 
					'ip' 			=> $_SERVER['REMOTE_ADDR'] 				,
					'page_bid' 		=> $data['connexions_item']['id_bid']	,
					'profil_id' 	=> $data['connexions_item']['id_profil'],
					'navigateur' 	=> $_SERVER['HTTP_USER_AGENT']			,
					'login'         => $data['connexions_item']['email']	,			
					'sens' 			=> 'IN'
				);
		////$this->Global_bdd->insert_one_key("z_connexions", $the_data); // BBBBBBBBBBBBBBBBBBBBBBBBBBBB
				
				$data['username']	= $data['connexions_item']['id_bid'];
				$data['email']		= $data['connexions_item']['email'];


				$this->session->set_flashdata('username', $data['connexions_item']['id_bid']);
				$this->session->set_flashdata('email', $data['connexions_item']['email']);
				//BAKS Recuperation des roles
				$id_profil = $data['connexions_item']['id_type_profil'];

				$tab_mrole = array();   ///Tableau des roles des menus
				$tab_smrole = array();  ///Tableau des roles des sous menus
				$cur_menu = '';

				$tab_role = $this->role->get_conn_roles($id_profil);

				foreach ($tab_role as $val) {
					///Tableau des droits sur les menus
					if ($cur_menu != $val->mcode) {
						$tab_mrole[$val->mcode] = 1;
						$cur_menu = $val->mcode;
					}

					//Tableau des droits sur les sous menus
					//On ne recup�re que les valeurs positives
					if ($val->d_read != '-1') {
						$tab_smrole[$val->smcode]['d_read'] = $val->d_read;
					}
					if ($val->d_add != '-1') {
						$tab_smrole[$val->smcode]['d_add'] = $val->d_add;
					}
					if ($val->d_upd != '-1') {
						$tab_smrole[$val->smcode]['d_upd'] = $val->d_upd;
					}
					if ($val->d_del != '-1') {
						$tab_smrole[$val->smcode]['d_del'] = $val->d_del;
					}
				}

				///Chargement des donn�es de la tableau $data
				$data['menu_roles'] = $tab_mrole;
				$data['smenu_roles'] = $tab_smrole;

				//Mise des donn�es en session
				$this->session->set_userdata('menu_roles', $data['menu_roles']);
				$this->session->set_userdata('smenu_roles', $data['smenu_roles']);
					header("Location:".$suite_req."front-office");
	
			}
			
		///}
	}
	
	public function recaptcha($str='')
	{
		$google_url="https://www.google.com/recaptcha/api/siteverify";
		$secret='6LeelCsUAAAAAFB3Xm73h1WHtXA5y4BIdBZgWOw0';
		$ip=$_SERVER['REMOTE_ADDR'];
		$url=$google_url."?secret=".$secret."&response=".$str."&remoteip=".$ip;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		//curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // a enlever lors du deploiement en ligne laisser si en localhost
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // a enlever lors du deploiement en ligne laisser si en localhost
		$res = curl_exec($curl);
		curl_close($curl);
		$res= json_decode($res, true);
		//reCaptcha success check
		if($res['success'])
		{
			// echo " trouver les gars ";
		  return TRUE;
		}
		else
		{
		  $this->form_validation->set_message('recaptcha', 'The reCAPTCHA field is telling me that you are a robot. Shall we give it another try?');
		  return FALSE;
		}
	}
		//se_deconnecter
	public function se_deconnecter()
	{
		// Test sur les param�tres d'URL qui permettront d'identifier un "contexte" de d�connexion
		$tab_data_ses = $this->session->all_userdata();
				
		$this->session->sess_destroy();	// destruction des donnees de la session
				
		@$page_bidar =  @$tab_data_ses['lfc_jafr12_s']['page_bidar'];
		@$profil_id = @$tab_data_ses['lfc_jafr12_s']['profil'];
		@$login = @$tab_data_ses['lfc_jafr12_s']['login'];
		//var_dump($tab_data_ses);
				
		$the_data = array(
			'ip' 			=> $_SERVER['REMOTE_ADDR'] ,
			'navigateur' 	=> $_SERVER['HTTP_USER_AGENT'],
			'page_bid' 		=> $page_bidar,
			'profil_id' 	=> $profil_id,
			'login' 		=> $login,
			'sens' 			=> 'OUT'
		);
		//$this->Global_bdd->insert_one_key("z_connexions", $the_data);
		//$this->session->sess_destroy();	
		@header("Location:".site_url()."sign-in");
	}

}
