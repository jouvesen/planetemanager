<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_pays extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_pays', 'pays');
    }

    public function index()
    {
        $data['all_data'] = $this->pays->get_active_data();
        $this->load->view('V_pays', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->pays->id_pays = $args[0];
        $this->pays->get_record();
        echo json_encode($this->pays, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->pays->id_pays = $args[0];
        echo json_encode($this->pays->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('id_pays') != '')
            $this->pays->id_pays = $this->input->post('id_pays');
        else
            check_unique_field('pays','code_pays', $this->input->post('code_pays'));


        $this->pays->code_pays = $this->input->post('code_pays');
        $this->pays->fr = $this->input->post('fr');
        $this->pays->en = $this->input->post('en');
        $this->pays->etat_pays = '1';
        // $this->pays->statut_pays = 1;

        $result = $this->pays->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
