<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_types_fonctions extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_types_fonctions', 'types_fonctions');
        $this->load->model('M_categorie', 'categorie');
        $this->load->model('M_types_fonction_categorie', 'types_fonction_categorie');

    }

    public function index()
    {
        $data['all_data'] = $this->types_fonctions->get_active_data();
        $data['data_categorie'] = $this->categorie->get_data();

        $this->load->view('V_types_fonctions', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->types_fonctions->code_fonction = $args[0];
        $categorie = $this->types_fonction_categorie->get_fonction_by_id($args[0]);
        $this->types_fonctions->get_active_record();
        foreach ($this->types_fonctions as $params => $value_fonct) {
            $resultat[$params] = $value_fonct;
        }


        foreach ($categorie as $param => $value) {

            foreach ($value as $nom => $p) {
                if ($nom == "libelle_categorie") {
                    $resultat[$p] = 1;
                }


            }

        }


        echo json_encode($resultat, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function delete()
    {
        $args = func_get_args();
        $this->types_fonctions->code_fonction = $args[0];
        $this->types_fonction_categorie->code_fonction = $args[0];
        $this->types_fonction_categorie->delete();
        echo json_encode($this->types_fonctions->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_fonction') != '')
            $this->types_fonctions->code_fonction = $this->input->post('code_fonction');


        $this->types_fonctions->nom_fonction = $this->input->post('nom_fonction');
        $this->types_fonctions->etat_fonction = '1';
        $this->types_fonctions->description = $this->input->post('description');
        // $this->types_fonctions->etat_fonction = 1;

        $result = $this->types_fonctions->save();
        $id = intval($result['id']);
        $categorie_array = $this->categorie->get_data();

        $this->types_fonction_categorie->inactive_entries($id);
        foreach ($categorie_array as $value_categorie) {
            $test_categorie = $this->input->post($value_categorie->libelle_categorie);
            //$donnees = $this->types_fonction_categorie->get_fonction_by_id($id);
            if (isset ($test_categorie)) {
                $ligne_test = $this->types_fonction_categorie->get_fonction_categorie_by_id($test_categorie, $id);
                if (count($ligne_test) != 0) {
                    $id_fonction_categorie = $ligne_test[0]->id_fonction_categorie;
                    $code_structure = $ligne_test[0]->code_structure;


                } else {
                    $id_fonction_categorie = null;
                    $code_structure = $test_categorie;
                }
                $this->types_fonction_categorie->id_fonction_categorie = $id_fonction_categorie;
                $this->types_fonction_categorie->code_structure = $code_structure;
                $this->types_fonction_categorie->id_fonction = $id;
                $this->types_fonction_categorie->etat_fonction_categorie = '1';
                $this->types_fonction_categorie->save();
            }



        }
        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
}
