<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_annee_scolaire extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_annee_scolaire', 'annee_scolaire');
    }

    public function index()
    {
        $data['all_data'] = $this->annee_scolaire->get_active_data();

        $this->load->view('V_annee_scolaire', $data);
    }


    public function get_record()
    {
        $args = func_get_args();
        $this->annee_scolaire->code_annee = $args[0];
        $this->annee_scolaire->get_record();
        echo json_encode($this->annee_scolaire, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    /*public function show()
    {
        $args = func_get_args();
        $id = $args[0];
        $result = $this->fonction_categorie->get_fonction_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
*/
    public function delete()
    {
        $args = func_get_args();
        $this->annee_scolaire->code_annee = $args[0];
        echo json_encode($this->annee_scolaire->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_annee') != '')
            $this->annee_scolaire->code_annee = $this->input->post('code_annee');
        else
            check_unique_field('param_annee_scolaire', 'libelle_annee', 'annee_cours', 'etat_annee', $this->input->post('libelle_annee'), $this->input->post('annee_cours'),
                $this->input->post('etat_annee'));

        $this->annee_scolaire->libelle_annee = $this->input->post('libelle_annee');
        $this->annee_scolaire->annee_cours = $this->input->post('annee_cours');
        $this->annee_scolaire->etat_en_cours = $this->input->post('etat_annee');

        $this->annee_scolaire->statut_annee = '1';

        $result = $this->annee_scolaire->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
