<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_corps extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_corps', 'corps');
        $this->load->model('M_grade', 'grade');
        $this->load->model('M_corps_grade', 'corps_grade');
    }

    public function index()
    {
        $data['all_data'] = $this->corps->get_active_data();
        $data['all_data_grade'] = $this->grade->get_active_data();
        $this->load->view('V_corps', $data);
    }
    public function get_record()
    {
        $args = func_get_args();
        $this->corps->code_corps = $args[0];
        $lst_grade = $this->corps_grade->get_corps_by_id($args[0]);
        $this->corps->get_active_record();
        foreach ($this->corps as $params => $value) {
            $resultat[$params] = $value;
        }


        foreach ($lst_grade as $values) {
            foreach ($values as $params=>$k) {

                if ($params == "code_grade") {
                    $resultat[$k] = 1;
                }


            }
        }


        echo json_encode($resultat, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }


    public function show(){

        $args = func_get_args();
        $id = $args[0];

        $result = $this->corps_grade->get_corps_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

    }

    public function delete()
    {
        $args = func_get_args();
        $this->corps->code_corps = $args[0];
        echo json_encode($this->corps->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
    public function save()
    {
        if ($this->input->post('code_corps') != '')
            $this->corps->code_corps = $this->input->post('code_corps');
        else
            check_unique_field($this->corps->get_db_table(), 'nom_corps', 'code_grade', $this->input->post('nom_corps'), $this->input->post('code_grade'));

        $this->corps->nom_corps = $this->input->post('nom_corps');

        $this->corps->etat_corps = '1';
        $this->corps->description = $this->input->post('description');

        $result = $this->corps->save();

        $id = $result['id'];



        //save checkbox
        $list_grade= $this->grade->get_active_data();
        //suprimer toute entr�es de la base avec un code_corps coché
        $this->corps_grade->inactive_entries_corps($id);
        if(count($list_grade)!=0)
            foreach ($list_grade as $value_grade) {
                $test_grade = $this->input->post($value_grade->code_grade);

                if (isset($test_grade)) {
                    $ligne_teste = $this->corps_grade->get_corps_grade_by_id($id, $test_grade);
                    if (count($ligne_teste) != 0) {
                        $id_corps_grade = $ligne_teste[0]->id_corps_grade;
                        $code_grade = $ligne_teste[0]->code_grade;



                    } else {
                        $id_corps_grade = Null;
                        $code_grade = $test_grade;


                    }
                    $this->corps_grade->id_corps_grade = $id_corps_grade;
                    $this->corps_grade->code_grade = $code_grade;
                    $this->corps_grade->code_corps = $id;
                    $this->corps_grade->etat_corps_grade = "1";
                    $this->corps_grade->save();
                }

            }




        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }






}
