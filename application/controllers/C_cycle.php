<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_cycle extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_cycle', 'cycle');
        $this->load->model('M_specialite_cycle','specialite_cycle');
    }

    public function index()
    {
        $data['all_data'] = $this->cycle->get_active_data();

        $this->load->view('V_cycle', $data);
    }

    public function get_record()
    {
        $args = func_get_args();
        $this->cycle->code_cycle = $args[0];
        $this->cycle->get_record();
        foreach ($this->cycle as $param => $value) {
            $resultat[$param] = $value;
        }
        echo json_encode($resultat, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }
    public function show(){

        $args = func_get_args();
        $id = $args[0];

        $result = $this->specialite_cycle->get_data_by_id($id);

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

    }

    public function delete()
    {
        $args = func_get_args();
        $this->cycle->code_cycle = $args[0];
        echo json_encode($this->cycle->fake_delete(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

    public function save()
    {
        if ($this->input->post('code_cycle') != '')
            $this->cycle->code_cycle = $this->input->post('code_cycle');
        else
            check_unique_field('param_cycle','nom_cycle', $this->input->post('nom_cycle'));

        $this->cycle->nom_cycle = $this->input->post('nom_cycle');
        $this->cycle->statut_cycle = '1';
        $this->cycle->description = $this->input->post('description');
       //$this->cycle->statut_cycle = 1;

        $result = $this->cycle->save();

        echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
    }

}
