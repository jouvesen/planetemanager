<?php

class M_type_equipement extends  MY_Model{

    public $id_type_equipement;
    public $libelle_type_equipement;
    public $etat_type_equipement;

    public function get_db_table()
    {
       return 'ephy_type_equipement';
    }

    public function get_db_table_pk()
    {
        return 'id_type_equipement';
    }

}