<?php

class M_type_evaluation extends MY_Model
{

    public $code_type_evaluation;
    public $type_evaluation;
    public $etat_etype_evaluation;

    public function get_db_table()
    {
        return 'type_evaluation';
    }

    public function get_db_table_pk()
    {
        return 'code_type_evaluation';
    }

    public function get_db_table_etat()
    {
        return 'etat_etype_evaluation';
    }

}