<?php

class M_discipline_matiere extends MY_Model
{

    public $id_matieredisc;
    public $id_matiere;
    public $id_discipline;

    public function get_db_table_pk()
    {
        return 'id_matieredisc';
    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("libelle_matiere,libelle_discipline")
            ->from($this->get_db_table())
            ->join('param_discipline', 'param_discipline.id_discipline=param_discipline_matiere.id_discipline', 'left')
            ->join('param_matiere', 'param_matiere.id_matiere=param_discipline_matiere.id_matiere', 'left')
            ->where('param_discipline_matiere.id_discipline', $requete)
            ->get()
            ->result();

    }

    public function get_db_table()
    {
        return 'param_discipline_matiere';
    }

    public function get_discipline_by_id($requete)
    {
        return $this->db->select("libelle_matiere,libelle_discipline")
            ->from($this->get_db_table())
            ->join('param_discipline', 'param_discipline.id_discipline=param_discipline_matiere.id_discipline', 'left')
            ->join('param_matiere', 'param_matiere.id_matiere=param_discipline_matiere.id_matiere', 'left')
            ->where('param_matiere.id_matiere', $requete)
            ->where('param_matiere.etat_matiere', '1')
            ->get()
            ->result();

    }




}