<?php

class M_categorie extends MY_Model
{

    public $code_categorie;
    public $libelle_categorie;

    public function get_db_table()
    {
        return 'categorie_structure';
    }

    public function get_db_table_pk()
    {
        return 'code_categorie';
    }

    public function clean_data()
    {

            return $this->db->select("code_categorie as ID, libelle_categorie as Libelle")
                ->from($this->get_db_table())
                ->get()
                ->result();



    }
    public function get_data_by_id($requete)
    {
        return $this->db->select("code_categorie as ID, libelle_categorie as Libelle")
            ->from($this->get_db_table())
            ->where($this->get_db_table_pk(), $requete)
            ->get()
            ->result();
    }



}
