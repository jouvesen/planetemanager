<?php

class M_type_handicap extends  MY_Model{

    public $id_handicap;
    public $libelle_handicap;
    public $etat;
    public $description;



    public function get_db_table()
    {
        return 'type_handicap';
    }

    public function get_db_table_pk()
    {
        return 'id_handicap';
    }
    public function get_db_table_etat()
    {
        return 'etat_handicap';
    }

}