<?php
Class M_programmes extends MY_Model {

    public $id_programme;
    public $code_programme;
    public $code_section;
    public $code_type_serie;
    public $libelle_programme;
    public $etat_programme;

    public function get_db_table(){
        return 'programmes';
    }
    public function get_db_table_pk(){
        return 'id_programme';
    }
    public function get_db_table_etat(){
        return 'etat_programme';
    }

    public function get_serie_by_id($requete){
        return $this->db->select("*")
            ->from($this->get_db_table())
            ->join('param_type_serie', 'param_type_serie.code_type_serie=programmes.code_type_serie')
            ->where("programmes.id_programme", $requete)
            ->where("programmes.etat_programme", '1')
            ->get()
            ->result();
    }
    public function get_active_data()
    {
        return $this->db->select('*')
                ->from($this->get_db_table())
                ->join('param_section', 'param_section.code_section=programmes.code_section')
                ->join('param_type_serie', 'param_type_serie.code_type_serie=programmes.code_type_serie')
                ->get()
                ->result();
    }
    public function get_section_by_id($requete)
    {
        return $this->db->select("param_section.libelle_section,param_section.code_section as code_section")
            ->from($this->get_db_table())
            ->join('param_section', 'param_section.code_section=programmes.code_section', 'left')
            ->where('programmes.id_programme', $requete)
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();


}
    public function get_programme_by_section_serie($requete1,$requete2)
    {
        $results = $this->db->select("*")
            ->from($this->get_db_table())
            ->join('param_section','param_section.code_section=programmes.code_section','left')
            ->join('param_type_serie','param_type_serie.code_type_serie=programmes.code_type_serie','left')
            ->where('programmes.code_section' , $requete1)
            ->where('programmes.code_type_serie', $requete2)
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();
        return $results;
    }
    public function get_programme_by_section($requete){
        $results=$this->db->select("*")
        ->from($this->db->table())
            ->join('param_section', 'param_section.code_section=programmes.code_section', 'left')
            ->where('programmes.code_section', $requete)
            ->where($this->get_db_table_etat(), '1')
        ->get()
        ->result() ;
        return $results;
    }
    public function get_programme_by_serie($requete){
        $results=$this->db->select("*")
            ->from($this->db->table())
            ->join('param_type_serie', 'param_type_serie.code_type_serie=programmes.code_type_serie')
            ->where('programmes.code_type_serie', $requete)
            ->where($this->get_db_table_etat(), '1')

            ->get()
            ->result();
        return $results;

    }


}
