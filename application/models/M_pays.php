<?php
class M_pays extends  MY_Model{

public $id_pays;
public $code_pays;
public $fr;
public $en;
public $etat_pays;

public function get_db_table()
{
return 'pays';
}

public function get_db_table_pk()
{
return 'id_pays';
}
public function get_db_table_etat()
{
return 'etat_pays';
}

}