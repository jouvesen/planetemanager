<?php

class M_specialite_cycle extends MY_Model
{


    public $code_spec_cycle;
    public $code_cycle;
    public $code_specialite;
    public $etat_specialite_cycle;

    public function get_db_table_pk()
    {
        return 'code_spec_cycle';
    }

    public function clean_data()
    {

        return $this->db->select("nom_cycle as Cycle, nom_specialite as Specialite, code_spec as Code")
            ->from($this->get_db_table())
            ->join('param_cycle', 'param_specialite_cycle.code_cycle=param_cycle.code_cycle', 'left')
            ->join('param_specialite', 'param_specialite.code_specialite=param_specialite.code_specialite', 'left')
            ->get()
            ->result();


    }

    public function get_db_table()
    {
        return 'param_specialite_cycle';
    }

    public function get_data_by_id($requete)
    {
        return $this->db->select(" param_cycle.code_cycle, nom_cycle as Cycle, nom_specialite , code_spec as Code")
            ->from($this->get_db_table())
            ->join('param_cycle', 'param_specialite_cycle.code_cycle=param_cycle.code_cycle', 'left')
            ->join('param_specialite', 'param_specialite_cycle.code_specialite=param_specialite.code_specialite', 'left')
            ->where('param_specialite_cycle.code_cycle', $requete)
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();
    }

    public function get_db_table_etat()
    {
        return 'etat_specialite_cycle';
    }

    public function get_specialite_by_id($requete)
    {
        return $this->db->select("param_cycle.code_cycle as code_cycle, nom_cycle, nom_specialite , code_spec ")
            ->from($this->get_db_table())
            ->join('param_cycle', 'param_specialite_cycle.code_cycle=param_cycle.code_cycle', 'left')
            ->join('param_specialite', 'param_specialite_cycle.code_specialite=param_specialite.code_specialite', 'left')
            ->where('param_specialite_cycle.code_specialite', $requete)
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();
    }

    public function inactive_entries($id)
    {
        $sql = "UPDATE param_specialite_cycle set etat_specialite_cycle ='-1' WHERE code_specialite='$id'";
        $this->db->query($sql);
    }

    public function get_specialite_cycle_by_id($requete1, $requete2)
    {
        $results = $this->db->select("*")
            ->from($this->get_db_table())
            ->join('param_cycle', 'param_cycle.code_cycle=param_specialite_cycle.code_cycle', 'inner')
            ->join('param_specialite', 'param_specialite.code_specialite=param_specialite_cycle.code_specialite', 'inner')
            ->where('param_cycle.code_cycle', $requete1)
            ->where('param_specialite.code_specialite', $requete2)
            ->where($this->get_db_table_etat(), '-1')
            ->get()
            ->result();
        return $results;
    }

}