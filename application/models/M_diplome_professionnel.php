<?php
class M_diplome_professionnel extends  MY_Model{

public $code_diplome_pro;
public $libelle_diplome_pro;
public $desc_diplome_pro;
public $etat_diplome_pro;

public function get_db_table()
{
return 'diplome_professionnel';
}

public function get_db_table_pk()
{
return 'code_diplome_pro';

}

    public function get_db_table_etat()
    {
        return 'etat_diplome_pro';
}

}