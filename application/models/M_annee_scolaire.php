<?php

class M_annee_scolaire extends MY_Model
{


    public $code_annee;
    public $annee_cours;
    public $libelle_annee;
    public $etat_en_cours;
    public $statut_annee;

    public function get_db_table_pk()
    {
        return 'code_annee';
    }

    public function get_annee_scolaire_by_id($requete)
    {
        return $this->db->select("annee_cours, libelle_annee, etat_en_cours")
            ->from($this->get_db_table())
            ->where('code_annee', $requete)
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();
    }

    public function get_db_table()
    {
        return 'param_annee_scolaire';
    }

    /* public function get_data_by_id($requete)
     {
         return $this->db->select("libelle_fonction as Fonction")
             ->from($this->get_db_table())
             ->join('types_fonctions', 'types_fonctions.id_fonction=type_fonction_categorie.id_fonction', 'left')
             ->join('categorie_structure', 'categorie_structure.code_categorie=type_fonction_categorie.code_structure', 'left')
             ->where('categorie_structure.code_categorie', $requete)
             ->where('categorie_structure.etat_categorie', '1')
             ->get()
             ->result();
     }


    */

    public function get_db_table_etat()
    {
        return 'statut_annee';
    }



}