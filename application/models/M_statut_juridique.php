<?php
class M_statut_juridique extends  MY_Model{

public $code_sj;
public $libelle_sj;
    public $description_sj;
public $etat_sj;

public function get_db_table()
{
return 'statut_juridique';
}

public function get_db_table_pk()
{
return 'code_sj';
}

    public function get_db_table_etat()
    {
        return 'etat_sj';
    }

}