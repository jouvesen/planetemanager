<?php

class M_type_acte extends  MY_Model{

    public $id_type_acte;
    public $libelle_type_acte;
    public $etat_type_acte;
    public $description;

    public function get_db_table()
    {
        return 'type_acte';
    }

    public function get_db_table_pk()
    {
        return 'id_type_acte';
    }

    public function get_db_table_etat()
    {
        return 'etat_type_acte';
    }

}