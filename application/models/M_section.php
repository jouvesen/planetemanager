<?php

class M_section extends  MY_Model{

    public $code_section;
    public $codesection;
    public $code_cycle;
    public $libelle_section;
    public $effectif_enseignant;
    public $ordre_section;
    public $etat_section;

    public function delete()
    {
        $this->db->where($this->get_db_table_pk(), $this->{$this->get_db_table_pk()});
        $this->db->update($this->get_db_table(), array($this->get_db_table_etat() => '-1'));

        if ($this->db->trans_status() === FALSE) {
            $status = 'error';
            $result = 'Error! ID [' . $this->{$this->get_db_table_pk()} . '] not found';
        } else {
            $status = 'success';
            $result = 'Suppression effectuée avec succées.';
        }

        $d = array();
        $d['status'] = $status;
        $d['message'] = $result;

        return $d;
    }

    public function get_db_table_pk()
    {
        return 'code_section';
    }

    public function get_db_table()
    {
        return 'param_section';
    }

    public function get_db_table_etat()
    {
        return 'etat_section';
    }
    public function clean_data()
    {
        if ($this->get_db_table_etat()) {
            return $this->db->select("code_section as ID, codesection, libelle_section as libelle, ordre_section as ordre,nom_cycle as nom")
                ->from($this->get_db_table())
                ->join('param_cycle', 'param_cycle.code_cycle=param_section.code_cycle', 'left')
                ->where($this->get_db_table_etat(), '1')
                ->get()
                ->result();
        } else {
            $this->get_active_data();
        }

    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("nom_cycle as nom ,libelle_section as libelle , ordre_section as ordre ")
            ->from($this->get_db_table())
            ->join('param_cycle', 'param_cycle.code_cycle=param_section.code_cycle', 'left')
            ->where($this->get_db_table_etat(), '1')
            ->where($this->get_db_table_pk(), $requete)
            ->get()
            ->result();
    }

}