<?php

class M_type_groupesanguin extends  MY_Model{

    public $id_gsanguin;
    public $libelle_gsanguin;
    public $etat_gs;
    public $description;


    public function get_db_table()
    {
        return 'type_groupesanguin';
    }

    public function get_db_table_pk()
    {
        return 'id_gsanguin';
    }

    public function get_db_table_etat()
    {
        return 'etat_gs';
    }

}