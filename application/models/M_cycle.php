<?php

class M_cycle extends  MY_Model{

    public $code_cycle;
    public $nom_cycle;
    public $statut_cycle;
    public $description;

    public function get_db_table()
    {
        return 'param_cycle';
    }

    public function get_db_table_pk()
    {
        return 'code_cycle';
    }

    public function get_db_table_etat()
    {
        return 'statut_cycle';
    }

    public function clean_data()
    {
        if ($this->get_db_table_etat()) {
            return $this->db->select("code_cycle as ID, nom_cycle as nom, Description,")
                ->from($this->get_db_table())
                ->where($this->get_db_table_etat(), '1')
                ->get()
                ->result();
        } else {
            $this->get_active_data();
        }



}

    public function get_data_by_id($requete)
    {
        return $this->db->select("code_cycle as ID,nom_cycle as nom,  Description")
            ->from($this->get_db_table())
            ->where($this->get_db_table_etat(), '1')
            ->where($this->get_db_table_pk(), $requete)
            ->get()
            ->result();
    }
}