<?php
class M_structure extends  MY_Model{

    public $code_str;
    public $libelle_structure;
    public $daate_creation_str;
    public $email_str;
    public $tel_str;
    public $etat_str;
    public $date_fermeture;
    public $arrete_ferme;
    public $id_saisie;
    public $date_insert;

    public function get_db_table()
    {
        return 'structure';
    }

    public function get_db_table_pk()
    {
        return 'code_str';
    }
    public function get_db_table_etat()
    {
        return 'etat_str';
    }
    public function clean_data()
    {
        if ($this->get_db_table_etat()) {
            return $this->db->select("code_str , libelle_structure as Libelle, date_creation_str ,email_str ,tel_str ,date_fermeture,arrete_ferme,id_saisie,date_insert")
                ->from($this->get_db_table())
                ->where($this->get_db_table_etat(), '1')
                ->get()
                ->result();
        } else {
            $this->get_active_data();
        }


    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("code_str , libelle_structure , date_creation_str  ,email_str  ,tel_str ,date_fermeture,arrete_ferme,id_saisie,date_insert")
            ->from($this->get_db_table())
            ->where($this->get_db_table_etat(), '1')
            ->where($this->get_db_table_pk(), $requete)
            ->get()
            ->result();
    }
    /*public function get_active_data(){
        return $this->db->select("code_str , libelle_structure , date_creation_str  ,email_str  ,tel_str ,date_fermeture,arrete_ferme,date_insert")
            ->from($this->get_db_table())
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();

    }*/


}