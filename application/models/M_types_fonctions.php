<?php

class M_types_fonctions extends MY_Model
{

    public $code_fonction;
    public $nom_fonction;
    public $description;
    public $etat_fonction;

    public function delete()
    {
        $this->db->where($this->get_db_table_pk(), $this->{$this->get_db_table_pk()});
        $this->db->update($this->get_db_table(), array($this->get_db_table_etat() => '-1'));

        if ($this->db->trans_status() === FALSE) {
            $status = 'error';
            $result = 'Error! ID [' . $this->{$this->get_db_table_pk()} . '] not found';
        } else {
            $status = 'success';
            $result = 'Suppression effectuée avec succées.';
        }

        $d = array();
        $d['status'] = $status;
        $d['message'] = $result;

        return $d;
    }

    public function get_db_table_pk()
    {
        return 'code_fonction';
    }

    public function get_db_table()
    {
        return 'param_fonction';
    }

    public function get_db_table_etat()
    {
        return 'etat_fonction';
    }

    public function clean_data()
    {
        if ($this->get_db_table_etat()) {
            return $this->db->select("code_fonction as ID, nom_fonction as Libellé, description as Description")
                ->from($this->get_db_table())
                ->where($this->get_db_table_etat(), '1')
                ->get()
                ->result();
        } else {
            $this->get_active_data();
        }

    }

    public function get_active1_data()
    {
        return $this->db->select("code_fonction as ID, nom_fonction as Fonction, code_categorie as ID_Categorie, libelle_categorie as Categorie, description as Description")
            ->from($this->get_db_table())
            ->join('types_fonctions', 'param_fonction.code_fonction=types_fonctions.id_fonction', 'left')
            ->join('categorie_structure', 'categorie_structure.code_categorie=types_fonctions.code_structure', 'left')
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();
    }


}