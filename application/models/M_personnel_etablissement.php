<?php 
	Class M_personnel_etablissement extends MY_Model {
		public $id_ens;
		public $matricule_ens;
		public $ien_ens;
		public $prenom_ens;
		public $nom_ens;
		public $sexe_ens;
		public $email_ens;

		public function get_db_table(){
			return 'epeda_personnel_etablissement';
		}
    	public function get_db_table_pk(){
    		return 'id_ens';
		}
		
		public function get_data_liste($anscours,$etat){
		    $sql = "SELECT e.*,s.nom_specialite,t.libelle_type_personnel, es.etat_affectation FROM ".$this->get_db_table()." AS e
	            LEFT OUTER JOIN param_specialite AS s
    	        ON e.code_specialite = s.code_specialite
		        INNER JOIN epad_enseignants_structure AS es	ON es.code_str=".$this->session->code_str." and es.id_ens=e.id_ens AND es.annee_entree_str<='$anscours' AND es.annee_sortie_str>'$anscours' and es.etat_prise='$etat'
    	        LEFT OUTER JOIN param_type_personnel AS t
    	        ON es.id_type_personnel = t.id_type_personnel
		        ";
		
		    $query = $this->db->query($sql);
		    return $query->result();
		}
		public function get_data_liste_by_id($anscours,$id){
		$sql = "SELECT e.*,s.nom_specialite,t.libelle_type_personnel FROM ".$this->get_db_table()." AS e
	            LEFT OUTER JOIN param_specialite AS s
    	        ON e.code_specialite = s.code_specialite
		        INNER JOIN epad_enseignants_structure AS es
		        ON es.code_str=".$this->session->code_str." and es.id_ens=e.id_ens AND es.annee_entree_str<='$anscours' AND es.annee_sortie_str>'$anscours' and es.id_ens='$id'
		        LEFT OUTER JOIN param_type_personnel AS t
    	        ON es.id_type_personnel = t.id_type_personnel
		        ";

		$query = $this->db->query($sql);
		return $query->row_array();
		}
		public function get_data_liste_by_ens($id){
			$sql = "SELECT e.*,s.nom_specialite FROM ".$this->get_db_table()." AS e

	            LEFT OUTER JOIN param_specialite AS s
    	        ON e.code_specialite = s.code_specialite where e.ien_ens='$id'
    	        ";

			$query = $this->db->query($sql);
			return $query->row_array();
		}

		public function get_liste_etab_ens($id_ens, $anscours)
		{
			$sql = "SELECT e.*, s.code_str, s.libelle_structure FROM epad_enseignants_structure AS e
					INNER JOIN structure AS s
					ON s.code_str=e.code_str
					LEFT OUTER JOIN param_type_personnel AS t
    	        	ON e.id_type_personnel = t.id_type_personnel
					WHERE e.id_ens='$id_ens' AND e.annee_entree_str<='$anscours' AND e.annee_sortie_str>'$anscours'";

			$query = $this->db->query($sql);
			return $query->result();
		}



		
	}
 ?>