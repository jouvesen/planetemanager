<?php
Class M_type_serie extends MY_Model {

    public $code_type_serie;
    public $libelle_serie;
    public $etat_serie;

    public function get_db_table(){
        return 'param_type_serie';
    }
    public function get_db_table_pk(){
        return 'code_type_serie';
    }
    public function get_db_table_etat(){
        return 'etat_serie';
    }


    public function get_serie_by_id($requete){
        return $this->db->select(" libelle_serie ")
            ->from($this->get_db_table())

            ->join('programmes', 'programmes.code_type_serie=param_type_serie.code_type_serie', 'left')
            ->where('param_type_serie.code_type_serie', $requete)
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();
    }


}
