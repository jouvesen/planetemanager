<?php

class M_grade extends MY_Model
{

    public $code_grade;
    public $nom_grade;
    public $description;
    public $etat_grade;

    public function clean_data()
    {
        if ($this->get_db_table_etat()) {
            return $this->db->select("code_grade as ID, nom_grade as nom")
                ->from($this->get_db_table())
                ->where($this->get_db_table_etat(), '1')
                ->get()
                ->result();
        } else {
            $this->get_active_data();
        }

    }

    public function get_db_table_etat()
    {
        return 'etat_grade';
    }

    public function get_db_table()
    {
        return 'param_grade';
    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("code_grade as ID, nom_grade as nom")
            ->from($this->get_db_table())
            ->where($this->get_db_table_etat(), '1')
            ->where($this->get_db_table_pk(), $requete)
            ->get()
            ->result();
    }

    public function get_db_table_pk()
    {
        return 'code_grade';
    }

}