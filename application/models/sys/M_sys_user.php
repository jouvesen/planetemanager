<?php 
class M_sys_user extends CI_model 
{
	public function get_user_liste( /*$code_str*/)
	{

		return $this->db->select("usr.id as id_user, CONCAT(UPPER(pr.prenom_ens), ' ', UPPER(pr.nom_ens)) AS prenom_nom,
								usr.email, ssp.libelle_type_profil AS profil, usr.ien")
			->from($this->get_db_table() . ' as usr')
			->join('epad_enseignants_structure as ens', 'usr.code_str = ens.code_str', 'left')
			->join('epeda_personnel_etablissement as pr', 'pr.id_ens = ens.id_ens', 'left')
			->join('sys_type_profil as ssp', 'ssp.id_type_profil = usr.id_profil', 'left')
			->get()
			->result();

		/*		$sql = "SELECT
                            usr.id as id_user, CONCAT(UPPER(ens.prenom_ens), ' ',UPPER(ens.nom_ens)) AS prenom_nom,
                            usr.email,
                            pr.libelle_type_profil AS profil,
                            usr.ien
                        FROM
                            sys_niits usr
                        INNER JOIN sys_type_profil pr ON (pr.id_type_profil = usr.id_profil)";
                       WHERE usr.code_str = '$code_str'";

                $query = $this->db->query($sql);*/

	}

	public function get_db_table(){
		return 'sys_niits';
	}
}