<?php

class M_cycle_evaluation extends MY_Model
{

    public $id_evaluation_cycle;
    public $code_cycle;
    public $code_type_evaluation;
    public $note_max;
    public $type_val;
    public $nombre;
    public $etat_evaluationcycle;

    public function get_db_table_pk()
    {
        return 'id_evaluation_cycle';
    }

    public function get_db_table_etat()
    {
        return 'etat_evaluationcycle';
    }


    public function get_active_data()
    {
        $results = $this->db->select("id_evaluation_cycle as ID, nom_cycle as Cycle, type_evaluation, note_max, type_val, nombre")
            ->from($this->get_db_table())
            ->join('param_cycle', 'evaluation_cycle.code_cycle=param_cycle.code_cycle', 'left')
            ->join('type_evaluation', 'evaluation_cycle.code_type_evaluation=type_evaluation.code_type_evaluation', 'left')
            ->where('etat_evaluationcycle', '1')
            ->get()
            ->result();
        return $results;
    }

    public function get_cycle_eval($request)
    {
        $results = $this->db->select("*")
            ->from($this->get_db_table())
            ->join('param_cycle', 'evaluation_cycle.code_cycle=param_cycle.code_cycle', 'left')
            ->join('type_evaluation', 'evaluation_cycle.code_type_evaluation=type_evaluation.code_type_evaluation', 'left')
            ->where('etat_evaluationcycle', '1')
            ->where('evaluation_cycle.code_cycle', $request)
            ->get()
            ->result();
        $results = reset($results);
        if ($results == null)
            $this->{$this->get_db_table_pk()} = null;
        else
            foreach ($results as $param => $value) {
                $this->{$param} = $value;
            }
    }

    public function get_db_table()
    {
        return 'evaluation_cycle';

    }
}