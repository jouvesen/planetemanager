<?php

class M_type_periode extends MY_Model
{

    public $code_type_periode;
    public $libelle_periode;
    public $etat_periode;

    public function get_db_table()
    {
        return 'type_periode';
    }

    public function get_db_table_pk()
    {
        return 'code_type_periode';
    }

    public function get_db_table_etat()
    {
        return 'etat_periode';
    }

}