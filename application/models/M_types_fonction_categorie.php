<?php

class M_types_fonction_categorie extends MY_Model
{


    public $id_fonction_categorie;
    public $id_fonction;
    public $code_structure;
    public $etat_fonction_categorie;

    public function get_db_table_pk()
    {
        return 'id_fonction_categorie';
    }

    public function clean_data()
    {

        return $this->db->select("libelle_fonction as Fonction, libelle_categorie as Categorie")
            ->from($this->get_db_table())
            ->join('types_fonctions', 'types_fonctions.id_fonction=type_fonction_categorie.id_fonction', 'left')
            ->join('categorie_structure', 'categorie_structure.code_categorie=type_fonction_categorie.code_structure', 'left')
            ->get()
            ->result();


    }

    public function get_db_table()
    {
        return 'types_fonctions';
    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("nom_fonction as Fonction")
            ->from($this->get_db_table())
            ->join('param_fonction', 'param_fonction.code_fonction=types_fonctions.id_fonction', 'left')
            ->join('categorie_structure', 'categorie_structure.code_categorie=types_fonctions.code_structure', 'left')
            ->where('categorie_structure.code_categorie', $requete)
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();
    }

    public function get_db_table_etat()
    {
        return 'etat_fonction_categorie';
    }

    public function get_fonction_by_id($requete)
    {
        $results = $this->db->select("code_structure, libelle_categorie, types_fonctions.id_fonction, nom_fonction")
            ->from($this->get_db_table())
            ->join('param_fonction', 'param_fonction.code_fonction=types_fonctions.id_fonction', 'left')
            ->join('categorie_structure', 'categorie_structure.code_categorie=types_fonctions.code_structure', 'left')
            ->where('types_fonctions.id_fonction', $requete)
            ->where($this->get_db_table_etat(), '1')
            ->get()
            ->result();
        return $results;
    }

    public function get_fonction_categorie_by_id($requete1, $requete2)
    {
        $results = $this->db->select("id_fonction_categorie, code_structure, libelle_categorie, types_fonctions.id_fonction, nom_fonction")
            ->from($this->get_db_table())
            ->join('param_fonction', 'param_fonction.code_fonction=types_fonctions.id_fonction', 'inner')
            ->join('categorie_structure', 'categorie_structure.code_categorie=types_fonctions.code_structure', 'inner')
            ->where('types_fonctions.code_structure', $requete1)
            ->where('types_fonctions.id_fonction', $requete2)
            ->where($this->get_db_table_etat(), '-1')
            ->get()
            ->result();
        return $results;
    }

    public function inactive_entries($id)
    {
        $sql = "UPDATE types_fonctions set etat_fonction_categorie='-1' WHERE id_fonction='$id'";
        $this->db->query($sql);
    }



}