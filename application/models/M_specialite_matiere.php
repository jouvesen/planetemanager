<?php
 class M_specialite_matiere extends MY_MODEL
{
 public $code_spm;
 public $code_specialite;
 public $id_matiere;
 public $etat_matiere;


     public function get_db_table_pk()
     {
         return 'code_spm';
     }

     public function get_data_by_id($requete)
     {
         return $this->db->select("nom_specialite , libelle_matiere")
             ->from($this->get_db_table())
             ->join('param_specialite', 'specialite_matiere.code_specialite=param_specialite.code_specialite', 'left')
             ->join('param_matiere', 'specialite_matiere.id_matiere=param_matiere.id_matiere', 'left')
             ->where('specialite_matiere.code_specialite', $requete)
             ->where($this->get_db_table_etat(), '1')
             ->get()
             ->result();


     }

     public function get_db_table()
     {
         return 'specialite_matiere';
     }

     public function get_db_table_etat()
     {
         return 'etat_matiere';
     }
 }