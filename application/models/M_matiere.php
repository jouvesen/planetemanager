<?php

class M_matiere extends MY_Model
{

    public $id_matiere;
    public $libelle_matiere;
    public $etat_matiere;
    public $description_matiere;

    public function get_db_table_pk()
    {
        return 'id_matiere';
    }


    public function get_db_table()
    {
        return 'param_matiere';
    }

    public function get_db_table_etat()
    {
        return 'etat_matiere';
    }


}