<?php

Class M_table_param extends CI_Model
{

    public function check_unique_field($table, $col_name, $val_to_search)
    {
        $row = $this->db->select($col_name)
            ->from($table)
            ->where($col_name, trim($val_to_search))
            ->get()
            ->result();

        if(!empty($row))
        {
            $d = array();
            $d['status'] = 'error';
            $d['message'] = "La valeur ".$val_to_search." existe déjà.";
            echo json_encode($d, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
            exit();
        }
    }





    // Liste des section par niveau d'enseignement
    public function get_list_section_by_cycle($cycle)
    {
        $sql = 'SELECT * FROM param_section
	            where code_cycle=? order by ordre_section';

        $query = $this->db->query($sql, array($cycle));
        return $query->result();
    }


    // Liste des serie
    public function get_list_serie_by_cycle($cycle)
    {
        $sql = "SELECT * FROM param_type_serie where etat_serie=1 AND code_type_serie IN (select code_type_serie from param_cycle_serie where code_cycle='$cycle')";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_disciple_by_specialite($code_specialite)
    {
        $sql = "SELECT *
					FROM param_specialite_discipline p
					INNER JOIN param_discipline as d
					ON p.code_specialite=? AND
					p.etat_discipline_specialite=? AND d.id_discipline=p.id_discipline";

        $query = $this->db->query($sql, array($code_specialite, '1'));
        return $query->result();
    }


    public function get_list_fonction()
    {
        $sql = "SELECT *
					FROM param_type_personnel p
					WHERE p.etat_type_personnel=?";

        $query = $this->db->query($sql, array('1'));
        return $query->result();
    }


    public function get_annne_encours()
    {
        $sql = "SELECT *
					FROM param_annee_scolaire
					where etat_en_cours=?";

        $query = $this->db->query($sql, array(1));
        return $query->row_array();
    }

    public function get_annne_archive($etat)
    {
        $sql = "SELECT *
					FROM param_annee_scolaire
					where etat_en_cours=?";

        $query = $this->db->query($sql, array($etat));
        return $query->result();
    }

    public function get_cycle_str($code_str)
    {
        $sql = "SELECT c.code_cycle, c.nom_cycle, s.libelle_structure
					FROM  param_cycle_structure as ps
					INNER JOIN param_cycle as c ON c.code_cycle=ps.code_cycle
					INNER JOIN structure as s ON ps.code_str=ps.code_str
					where ps.code_str=?";

        $query = $this->db->query($sql, array($code_str));
        return $query->row_array();
    }

    // Liste des serie


// Liste des section par niveau d'enseignement
    public function get_section_by_id($code_section)
    {
        $sql = 'SELECT * FROM param_section where code_section=?';

        $query = $this->db->query($sql, array($code_section));

        return $query->row_array();
    }

}

?>