<?php

class M_categorie_evaluation extends MY_Model
{
    public $id_categorie_evaluation;
    public $code_categorie_eval;
    public $libelle_categorie_evaluation;
    public $etat_categorie_evaluation;

    public function get_db_table_pk()
    {
        return 'id_categorie_evaluation';
    }

    public function get_categorie_evaluation_by_id($request)
    {
        return $this->db->select("id_categorie_evaluation as ID, code_categorie_eval as CODE, libelle_categorie_evaluation as Libellé_Categorie")
            ->from($this->get_db_table())
            ->where($this->get_db_table_etat(), '1')
            ->where("id_categorie_evaluation", $request)
            ->get()
            ->result();
    }

    public function get_db_table()
    {
        return 'categorie_evaluation';
    }

    public function get_db_table_etat()
    {
        return 'etat_categorie_evaluation';
    }
}