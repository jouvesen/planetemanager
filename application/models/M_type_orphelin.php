<?php
class M_type_orphelin extends  MY_Model{

    public $id_orphelin;
    public $libelle_orphelin;
    public $etat_orphelin;


    public function get_db_table()
    {
        return 'type_orphelin';
    }

    public function get_db_table_pk()
    {
        return 'id_orphelin';
    }

    public function get_db_table_etat()
    {
        return 'etat_orphelin';
    }

}