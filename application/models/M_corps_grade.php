<?php

class M_corps_grade extends MY_Model
{


    public $id_corps_grade;
    public $code_corps;
    public $code_grade;
    public $etat_corps_grade;

    public function get_db_table_pk()
    {
        return 'id_corps_grade';
    }

    public function get_db_table_etat()
    {
        return 'etat_corps_grade';
    }

    public function clean_data()
    {

        return $this->db->select("nom_corps as Corps, nom_grade as Grade")
            ->from($this->get_db_table())
            ->join('corps', 'corps.code_corps=corps_grade.code_corps', 'left')
            ->join('grade', 'grade.code_grade=grade.code_grade', 'left')
            ->get()
            ->result();


    }

    public function get_db_table()
    {
        return 'corps_grade';
    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("nom_corps as Corps, nom_grade")
            ->from($this->get_db_table())
            ->join('corps', 'corps.code_corps=corps_grade.code_corps', 'left')
            ->join('param_grade', 'param_grade.code_grade=corps_grade.code_grade', 'left')
            ->where('corps_grade.code_corps', $requete)
            ->where('etat_corps_grade', '1')
            ->get()
            ->result();
    }

    public function get_grade_by_id($requete)
    {
        return $this->db->select("*")
                ->from($this->get_db_table())
            ->join('corps', 'corps.code_corps=corps_grade.code_corps')
            ->join('param_grade', 'param_grade.code_grade=corps_grade.code_grade')
            ->where('corps_grade.code_grade', $requete)
            ->where("etat_corps_grade", "1")
                ->get()
                ->result();
    }

    public function get_corps_by_id($requete)
    {
        return $this->db->select("*")
            ->from($this->get_db_table())
            ->join('corps', 'corps.code_corps=corps_grade.code_corps', 'left')
            ->join('param_grade', 'param_grade.code_grade=corps_grade.code_grade', 'left')
            ->where('corps_grade.code_corps', $requete)
            ->where('etat_corps_grade', '1')
            ->get()
            ->result();
    }

    public function get_corps_grade_by_id($id_corps, $id_grade)
    {
        return $this->db->select("*")
            ->from($this->get_db_table())
            ->join('corps', 'corps.code_corps=corps_grade.code_corps', 'left')
            ->join('param_grade', 'param_grade.code_grade=corps_grade.code_grade', 'left')
            ->where('corps_grade.code_corps', $id_corps)
            ->where('corps_grade.code_grade', $id_grade)
            ->where('etat_corps_grade', '-1')
            ->get()
            ->result();
    }


    public function inactive_entries_corps($id)
    {
        $sql = "UPDATE corps_grade set etat_corps_grade='-1' WHERE code_corps='$id'";
        $this->db->query($sql);
    }

    public function inactive_entries_grade($id)
    {
        $sql = "UPDATE corps_grade set etat_corps_grade='-1' WHERE code_grade='$id'";
        $this->db->query($sql);
    }

}