<?php
class M_diplome_academique extends  MY_Model{

public $code_diplome_ac;
public $libelle_diplome_ac;
public $desc_diplome_ac;
public $etat_diplome_ac;

public function get_db_table()
{
return 'diplome_academique';
}

public function get_db_table_pk()
{
return 'code_diplome_ac';
}

    public function get_db_table_etat()
    {
        return 'etat_diplome_ac';
    }

}