<?php

class M_discipline_cycle extends MY_Model
{

    public $id_discipline_cycle;
    public $id_discipline;
    public $code_cycle;

    public function get_db_table_pk()
    {
        return 'id_discipline_cycle';
    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("libelle_discipline,nom_cycle")
            ->from($this->get_db_table())
            ->join('param_cycle', 'param_cycle.code_cycle=discipline_cycle.code_cycle', 'left')
            ->join('param_discipline', 'param_discipline.id_discipline=discipline_cycle.id_discipline', 'left')
            ->where('discipline_cycle.code_cycle', $requete)
            ->get()
            ->result();


    }

    public function get_db_table()
    {
        return 'discipline_cycle';
    }

    public function get_discipline_by_id($requete)
    {
        return $this->db->select("libelle_discipline, nom_cycle")
            ->from($this->get_db_table())
            ->join('param_cycle', 'param_cycle.code_cycle=discipline_cycle.code_cycle', 'left')
            ->join('param_discipline', 'param_discipline.id_discipline=discipline_cycle.id_discipline', 'left')
            ->where('param_cycle.statut_cycle', '1')
            ->where('param_discipline.id_discipline', $requete)
            ->get()
            ->result();


    }



}