<?php

class M_corps extends  MY_Model{

public $code_corps;
public $nom_corps;
    public $description;
public $etat_corps;

public function get_db_table()
{
return 'corps';
}

public function get_db_table_pk()
{
return 'code_corps';
}

    public function get_db_table_etat()
    {
        return 'etat_corps';
    }
}