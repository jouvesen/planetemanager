<?php
class M_type_regroupement extends  MY_Model{

    public $id_regroupement;
    public $regroupement;
    public $ordre;
    public $statut_type_regroupement;


    public function get_db_table()
    {
        return 'type_regroupement';
    }

    public function get_db_table_pk()
    {
        return 'id_regroupement';
    }

    public function get_db_table_etat()
    {
        return 'statut_type_regroupement';
    }

}