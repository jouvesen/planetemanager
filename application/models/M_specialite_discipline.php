<?php

class M_specialite_discipline extends MY_Model
{


    public $code_spm;
    public $id_discipline;
    public $code_specialite;
    public $etat_specialite_discipline;

    public function get_db_table_pk()
    {
        return 'code_spm';
    }
    public function get_db_table_etat()
    {
        return 'etat_specialite_discipline';
    }

    public function clean_data()
    {

        return $this->db->select("libelle_discipline as Discipline, nom_specialite as Specialite, code_spec as Code")
            ->from($this->get_db_table())
            ->join('param_discipline', 'param_specialite_discipline.id_discipline=param_discipline.id_discipline', 'left')
            ->join('param_specialite', 'param_specialite.code_specialite=param_specialite.code_specialite', 'left')
            ->get()
            ->result();


    }

    public function get_db_table()
    {
        return 'param_specialite_discipline';
    }

    public function get_descipline_by_id($requeste)
    {

        return $this->db->select("libelle_discipline, nom_specialite, code_spec, code_discipline")
            ->from($this->get_db_table())
            ->join('param_discipline', 'param_specialite_discipline.id_discipline=param_discipline.id_discipline', 'left')
            ->join('param_specialite', 'param_specialite.code_specialite=param_specialite.code_specialite', 'left')
            ->where('param_specialite.code_specialite', $requeste)
            ->where('param_specialite.etat_specialite', '1')
            ->get()
            ->result();


    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("nom_discipline as Discipline, nom_specialite as Specialite, code_spec as Code")
            ->from($this->get_db_table())
            ->join('param_discipline', 'param_specialite_specialite.id_discipline=param_discipline.id_discipline', 'left')
            ->join('param_specialite', 'param_specialite.code_specialite=param_specialite.code_specialite', 'left')
            ->where('param_discipline.id_discipline', $requete)
            ->where('param_discipline.etat_discipline', '1')
            ->get()
            ->result();
    }
    public function inactive_entries($id)
    {
        $sql = "UPDATE param_specialite_discipline set etat_specialite_discipline ='-1' WHERE code_specialite='$id'";
        $this->db->query($sql);
    }

    public function get_specialite_discipline_by_id($requete1, $requete2)
    {
        $results = $this->db->select("*")
            ->from($this->get_db_table())
            ->join('param_discipline', 'param_discipline.id_discipline=param_specialite_discipline.id_discipline', 'inner')
            ->join('param_specialite', 'param_specialite.code_specialite=param_specialite_discipline.code_specialite', 'inner')
            ->where('param_discipline.id_discipline', $requete1)
            ->where('param_specialite.code_specialite', $requete2)
            ->where($this->get_db_table_etat(), '-1')
            ->get()
            ->result();
        return $results;
    }


}