<?php

class M_specialite extends  MY_Model{

    public $code_specialite;
    public $nom_specialite;
    public $code_spec;
    public $description;
    public $etat_specialite;

    public function delete()
    {
        $this->db->where($this->get_db_table_pk(), $this->{$this->get_db_table_pk()});
        $this->db->update($this->get_db_table(), array($this->get_db_table_etat() => '-1'));

        if ($this->db->trans_status() === FALSE) {
            $status = 'error';
            $result = 'Error! ID [' . $this->{$this->get_db_table_pk()} . '] not found';
        } else {
            $status = 'success';
            $result = 'Suppression effectuée avec succées.';
        }

        $d = array();
        $d['status'] = $status;
        $d['message'] = $result;

        return $d;
    }

    public function get_db_table_pk()
    {
        return 'code_specialite';
    }

    public function get_db_table()
    {
        return 'param_specialite';
    }

    public function get_db_table_etat()
    {
        return 'etat_specialite';
    }

    public function clean_data()
    {
        if ($this->get_db_table_etat()) {
            return $this->db->select("code_specialite as ID, nom_specialite as nom, code_spec ")
                ->from($this->get_db_table())
                ->where($this->get_db_table_etat(), '1')
                ->get()
                ->result();
        } else {
            $this->get_active_data();
        }

    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("code_specialite as ID, nom_specialite as nom, code_spec")
            ->from($this->get_db_table())
            ->where($this->get_db_table_etat(), '1')
            ->where($this->get_db_table_pk(), $requete)
            ->get()
            ->result();
    }
}