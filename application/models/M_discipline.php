<?php
class M_discipline extends  MY_Model{

public $id_discipline;
public $code_discipline;
public $libelle_discipline;
public $couleur_discipline;
public $composition_matiere;
public $description;
public $etat_discipline;

public function get_db_table()
{
return 'param_discipline';
}

public function get_db_table_pk()
{
    return 'id_discipline';
}

    public function get_db_table_etat()
    {
        return 'etat_discipline';
}
    public function clean_data()
    {
        if ($this->get_db_table_etat()) {
            return $this->db->select("id_discipline as ID,code_discipline as code,libelle_discipline as libelle,couleur_discipline as couleur, composition_matiere")
                ->from($this->get_db_table())
                ->where($this->get_db_table_etat(), '1')
                ->get()
                ->result();
        } else {
            $this->get_active_data();
        }

    }

    public function get_data_by_id($requete)
    {
        return $this->db->select("id_discipline as ID,code_discipline as code,libelle_discipline as libelle,couleur_discipline as couleur, composition_matiere")
            ->from($this->get_db_table())
            ->where($this->get_db_table_etat(), '1')
            ->where($this->get_db_table_pk(), $requete)
            ->get()
            ->result();
    }

}