-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: planete_management
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorie_evaluation`
--

DROP TABLE IF EXISTS `categorie_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie_evaluation` (
  `id_categorie_evaluation` int(11) NOT NULL AUTO_INCREMENT,
  `code_categorie_eval` varchar(2) NOT NULL,
  `libelle_categorie_evaluation` varchar(250) NOT NULL,
  `etat_categorie_evaluation` enum('1','-1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_categorie_evaluation`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie_evaluation`
--

LOCK TABLES `categorie_evaluation` WRITE;
/*!40000 ALTER TABLE `categorie_evaluation` DISABLE KEYS */;
INSERT INTO `categorie_evaluation` VALUES (1,'AC','Académique','1'),(2,'GR','Groupée','1'),(3,'IN','Individuelle','1'),(4,'AJ','ajout nouvelle categorie','-1');
/*!40000 ALTER TABLE `categorie_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorie_structure`
--

DROP TABLE IF EXISTS `categorie_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie_structure` (
  `code_categorie` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_categorie` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`code_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie_structure`
--

LOCK TABLES `categorie_structure` WRITE;
/*!40000 ALTER TABLE `categorie_structure` DISABLE KEYS */;
INSERT INTO `categorie_structure` VALUES (1,'centrale'),(2,'deconcentre'),(3,'etablissement');
/*!40000 ALTER TABLE `categorie_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corps`
--

DROP TABLE IF EXISTS `corps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corps` (
  `code_corps` int(100) NOT NULL AUTO_INCREMENT,
  `nom_corps` varchar(255) NOT NULL,
  `etat_corps` int(1) NOT NULL DEFAULT '1',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_corps`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corps`
--

LOCK TABLES `corps` WRITE;
/*!40000 ALTER TABLE `corps` DISABLE KEYS */;
INSERT INTO `corps` VALUES (-99,'Indetermine',1,' '),(1,'I',1,NULL),(2,'IS',1,NULL),(3,'ID',1,NULL),(4,'IA',1,NULL),(5,'IAS',1,NULL),(6,'IAD',1,NULL),(7,'MON',1,NULL),(8,'MEM',1,NULL),(9,'VEN',1,' vendredi'),(10,'MC',1,NULL),(11,'MOSA',1,NULL),(12,'IEE',1,NULL),(13,'PEM',1,NULL),(14,'Contractuel',1,NULL),(15,'Vacataire',1,NULL),(16,'PCEM',1,NULL),(17,'PES',1,NULL),(18,'ETP',1,NULL),(19,'AC',1,NULL),(20,'AET',1,NULL),(21,'CAC',1,NULL),(22,'CED',1,NULL),(23,'CEP',1,NULL),(24,'CPSC',1,NULL),(25,'EPS',1,NULL),(26,'EPSD',1,NULL),(27,'IAEE',1,NULL),(28,'IAEPJS',1,NULL),(29,'IEPS',1,NULL),(30,'IEPSD',1,NULL),(32,'MAEMS',1,NULL),(33,'MAEPS',1,NULL),(34,'MEPS',1,NULL),(35,'MEPSD',1,NULL),(36,'MEAM',1,NULL),(37,'METPD',1,NULL),(39,'MP',1,NULL),(40,'MPD',1,NULL),(41,'PCEMG',1,NULL),(42,'PCEMGD',1,NULL),(43,'PEAM',1,NULL),(44,'PEAMD',1,NULL),(45,'PEMD',1,NULL),(46,'PEPS',1,NULL),(47,'PESD',1,NULL),(48,'PETPD',1,NULL),(49,'PTTTD',1,NULL),(50,'PSCD',1,NULL),(51,'PSY CONS',1,NULL),(52,'TMD',1,NULL),(53,'IngÃ©nieurs',1,NULL),(54,'Medecin',1,NULL),(55,'Educateur Prescolaire',1,NULL),(56,'Inspecteur',1,NULL),(57,'Agent Administratif',1,NULL),(58,'SecrÃ©taire',1,NULL),(59,'Agent Municipal',1,NULL),(60,'Agent non fonctionnairel',1,NULL),(61,'ASSISTANTE SOCIALE',1,NULL),(62,'ATA',1,NULL),(63,'ATOPM',1,NULL),(64,'Autres',1,NULL),(65,'BibliothÃ©caire',1,NULL),(66,'CE',1,NULL),(67,'Chauffeur',1,NULL),(68,'CTR',1,NULL),(69,'GARDIEN',1,NULL),(70,'IAEN',1,NULL),(71,'IEEA',1,NULL),(72,'IEN',1,NULL),(73,'INFIRMIER',1,NULL),(74,'IP1',1,NULL),(75,'IPS',1,NULL),(76,'ITE',1,NULL),(77,'MET',1,NULL),(78,'N.E',1,NULL),(79,'PCMG',1,NULL),(80,'PEA',1,NULL),(81,'PESPl',1,NULL),(82,'SAGE FEMME D\'ETAT',1,NULL),(83,'IPCE',1,NULL),(84,'IP2',1,NULL),(85,'IP3',1,NULL),(86,'PETP',1,NULL),(87,'METP',1,NULL),(88,'AT',1,NULL),(89,'PC',1,NULL),(90,'IAEPS',1,NULL),(91,'PESTP',1,NULL),(92,'Prof: Assistant',1,NULL),(93,'Prof: Maitre Assistant',1,NULL),(94,'Prof: Prof. Titulaire',1,NULL),(95,'Prof: A1-PES',1,NULL),(96,'Prof: Maitre Assistant Agrégé',1,NULL),(97,'Prof: Prof. Technique',1,NULL),(98,'Prof: Assistant de Recherche',1,NULL),(99,'Prof : Maitre Assistant de Recherche',1,NULL),(100,'Prof: Charge de Recherche',1,NULL),(101,'Prof: Directeur de Recherche',1,NULL),(102,'Militaire',1,NULL),(103,'PATS',1,NULL),(104,'Prof: Maitre assistant Stagiaire',1,NULL),(105,'MC1',1,NULL),(106,'MC2',1,NULL),(107,'MC3',1,NULL),(108,'MC4',1,NULL),(109,'MC5',1,NULL),(110,'MC6',1,NULL),(111,'MC7',1,NULL),(112,'MC8',1,NULL),(113,'MC9',1,NULL),(114,'MC10',1,NULL),(115,'MC11',1,NULL),(116,'CPSCD',1,NULL),(117,'IAD2',1,NULL),(118,'IAEED',1,NULL),(119,'ID2',1,NULL),(120,'IEPJS',1,NULL),(121,'IEPJSD',1,NULL),(122,'Mo DÃ?Â©c',1,NULL),(123,'PAEPSD',1,NULL),(124,'PCD',1,NULL),(125,'PEPSD',1,NULL),(126,'PETTD',1,NULL),(127,'VE11',1,NULL),(128,'Conseiller en planification',1,NULL),(129,'CCC',1,NULL),(130,'MC12',1,NULL),(131,'Prof. Certifié',1,NULL),(132,'MC13',1,NULL),(133,'MC14',1,NULL),(134,'MC15',1,NULL),(135,'MC16',1,NULL),(136,'MC17',1,NULL),(137,'IEMS',1,NULL),(138,'MC18',1,NULL),(139,'MC19',1,NULL),(140,'MC20',1,NULL),(141,'MC21',1,NULL),(142,'Analyste Programmeur',1,NULL),(143,'Economiste',1,NULL),(171,'test ',-1,' tester l\'ajout avec mis à jour '),(172,'tester',1,'  tester l\'ajout avec mis à jour '),(173,'tester',1,'  tester l\'ajout avec mis à jour '),(174,'tester',1,'  tester l\'ajout avec mis à jour '),(175,'test1',1,' tester l\'ajout avec mis à jour'),(176,'test1',1,' tester l\'ajout avec mis à jour'),(177,'test ',1,' tevsdvuk b k'),(178,'test1',-1,' tester l\'ajout avec mis à jour'),(179,'test ',-1,'  tester l\'ajout avec mis à jour'),(180,'test ',-1,'  tester l\'ajout avec mis à jour'),(181,'test ',-1,'  tester l\'ajout avec mis à jour'),(182,'test ',1,'  tester l\'ajout avec mis à jour');
/*!40000 ALTER TABLE `corps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corps_grade`
--

DROP TABLE IF EXISTS `corps_grade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corps_grade` (
  `id_corps_grade` int(11) NOT NULL AUTO_INCREMENT,
  `code_corps` int(100) NOT NULL,
  `code_grade` int(11) NOT NULL,
  `etat_corps_grade` enum('1','-1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_corps_grade`),
  UNIQUE KEY `code_corps_2` (`code_corps`,`code_grade`),
  KEY `code_corps` (`code_corps`),
  KEY `code_grade` (`code_grade`),
  CONSTRAINT `corps_grade_ibfk_1` FOREIGN KEY (`code_grade`) REFERENCES `param_grade` (`code_grade`),
  CONSTRAINT `corps_grade_ibfk_2` FOREIGN KEY (`code_corps`) REFERENCES `corps` (`code_corps`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corps_grade`
--

LOCK TABLES `corps_grade` WRITE;
/*!40000 ALTER TABLE `corps_grade` DISABLE KEYS */;
INSERT INTO `corps_grade` VALUES (1,19,-99,'1'),(2,20,-99,'1'),(3,57,-99,'1'),(6,59,-99,'1'),(7,60,-99,'1'),(8,-99,-99,'-1'),(9,-99,1,'-1'),(10,-99,38,'1'),(11,-99,39,'1'),(12,-99,2,'-1'),(13,-99,29,'1'),(14,-99,30,'1'),(15,-99,31,'1'),(16,-99,32,'1'),(17,-99,21,'1'),(18,-99,22,'1'),(19,-99,23,'1'),(20,9,-99,'-1'),(21,9,1,'1'),(22,9,19,'1'),(23,9,27,'1'),(24,9,28,'1');
/*!40000 ALTER TABLE `corps_grade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diplome_academique`
--

DROP TABLE IF EXISTS `diplome_academique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diplome_academique` (
  `code_diplome_ac` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_diplome_ac` varchar(100) NOT NULL,
  `desc_diplome_ac` varchar(250) NOT NULL,
  `etat_diplome_ac` enum('1','-1') NOT NULL,
  PRIMARY KEY (`code_diplome_ac`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diplome_academique`
--

LOCK TABLES `diplome_academique` WRITE;
/*!40000 ALTER TABLE `diplome_academique` DISABLE KEYS */;
INSERT INTO `diplome_academique` VALUES (1,'BFEM','Brevet de Fin d\'Etude Moyenne','1'),(2,'BAC','BaccalaurÃ?Â©at','1'),(3,'Licence','Licence','1'),(4,'MaÃ?Â®trise','MaÃ?Â®trise','1'),(5,'Autres','Autres','1'),(6,'Doctorat','Doctorat','1'),(7,'CEPE','Certificat d\'Etudes Primaires ElÃ?Â©mentaire','1'),(8,'DUEL','DiplÃ?Åœme Universitaire d\'Etude LittÃ?Â©raire','1'),(9,'DUES','DiplÃ?Åœme Universitaire d\'Etude Scientifique','1'),(10,'DEUG','DiplÃ?Åœme d\'Etude Universitaire GÃ?Â©nÃ?Â©rale','1'),(11,'DEA','DiplÃ?Åœme d\'Ã?Â©tude approfondie','1'),(12,'CAP','Certificat dAptitude PÃ?Â©dagogique','1'),(13,'BEP','Brevet d\'Etude Professionnelle','1'),(14,'BT','Brevet Technique','1'),(15,'BTS','Brevet Technique','1'),(16,'DUT','DiplÃ?Åœme Universitaire de Technologie','1'),(17,'DST','DiplÃ?Åœme SupÃ?Â©rieur de Technologie','1'),(18,'Master','Master','1');
/*!40000 ALTER TABLE `diplome_academique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diplome_professionnel`
--

DROP TABLE IF EXISTS `diplome_professionnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diplome_professionnel` (
  `code_diplome_pro` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_diplome_pro` varchar(100) NOT NULL,
  `desc_diplome_pro` varchar(250) NOT NULL,
  `etat_diplome_pro` enum('1','-1') NOT NULL,
  PRIMARY KEY (`code_diplome_pro`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diplome_professionnel`
--

LOCK TABLES `diplome_professionnel` WRITE;
/*!40000 ALTER TABLE `diplome_professionnel` DISABLE KEYS */;
INSERT INTO `diplome_professionnel` VALUES (-99,'Aucune','Pas de diplome','1'),(1,'CEAP','certificat d aptitude pedagogique','1'),(2,'CAP','certificat d aptitude pedagogique','1'),(3,'CAMES','','1'),(4,'CAES','','1'),(5,'CAEM','','1'),(6,'CAE CEM','','1'),(7,'CAEMTP','','1'),(8,'CAMEPS','','1'),(9,'CAEMTF','','1'),(10,'CAPEPS','','1'),(11,'CAEAM','','1'),(13,'AGREG','','1'),(14,'CAPES','','1'),(15,'CAIEE','','1'),(16,'CAEEFS','','1'),(17,'CAESTP','','1'),(18,'CAEP','','1'),(19,'DFESA','','1'),(20,'CAIS','','1'),(21,'CAEP','','1'),(22,'DMP','','1'),(23,'CAFPC','','1');
/*!40000 ALTER TABLE `diplome_professionnel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discipline_cycle`
--

DROP TABLE IF EXISTS `discipline_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discipline_cycle` (
  `id_discipline_cycle` int(11) NOT NULL AUTO_INCREMENT,
  `id_discipline` int(11) NOT NULL,
  `code_cycle` int(11) NOT NULL,
  PRIMARY KEY (`id_discipline_cycle`),
  KEY `id_discipline` (`id_discipline`),
  KEY `code_cycle` (`code_cycle`),
  CONSTRAINT `discipline_cycle_ibfk_1` FOREIGN KEY (`code_cycle`) REFERENCES `param_cycle` (`code_cycle`),
  CONSTRAINT `discipline_cycle_ibfk_2` FOREIGN KEY (`id_discipline`) REFERENCES `param_discipline` (`id_discipline`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discipline_cycle`
--

LOCK TABLES `discipline_cycle` WRITE;
/*!40000 ALTER TABLE `discipline_cycle` DISABLE KEYS */;
INSERT INTO `discipline_cycle` VALUES (4,1,1),(5,1,2),(6,1,1),(7,1,2),(8,1,3),(9,6,3),(10,7,3),(11,7,4),(12,7,3),(13,7,4),(14,3,2),(15,3,3),(16,1,1),(17,1,2),(18,1,3);
/*!40000 ALTER TABLE `discipline_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `epad_enseignants_structure`
--

DROP TABLE IF EXISTS `epad_enseignants_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epad_enseignants_structure` (
  `id_enseignant_structure` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_type_personnel` int(11) DEFAULT NULL,
  `id_ens` int(11) NOT NULL,
  `code_str` int(11) NOT NULL,
  `annee_entree_str` int(4) unsigned NOT NULL,
  `annee_sortie_str` int(10) NOT NULL DEFAULT '9999',
  `date_os` date DEFAULT NULL,
  `date_prise_service` date DEFAULT NULL,
  `numero_os` varchar(250) DEFAULT NULL,
  `cessation_service` date DEFAULT NULL,
  `os_cessation_service` varchar(100) DEFAULT NULL,
  `Date_conf` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_sortie` date DEFAULT NULL,
  `etat_affectation` enum('1','-1') NOT NULL DEFAULT '1',
  `etat_prise` enum('0','1','-1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_enseignant_structure`),
  UNIQUE KEY `enseignants_structure_unique` (`id_ens`,`code_str`,`annee_entree_str`),
  KEY `enseignants_structure_code_str` (`code_str`),
  KEY `enseignants_structure_enseignant` (`id_ens`),
  KEY `id_type_personnel` (`id_type_personnel`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `epad_enseignants_structure`
--

LOCK TABLES `epad_enseignants_structure` WRITE;
/*!40000 ALTER TABLE `epad_enseignants_structure` DISABLE KEYS */;
INSERT INTO `epad_enseignants_structure` VALUES (1,1,1,1,2017,9999,NULL,NULL,NULL,NULL,NULL,'2017-09-05 17:39:36',NULL,'1','1'),(2,1,1,2,2017,9999,'2017-09-07','2017-09-07','ohàhhàhhiii',NULL,NULL,'0000-00-00 00:00:00','0000-00-00','1','1'),(3,1,88,1,2017,9999,'2017-09-06','2017-09-07','12568972255',NULL,NULL,'0000-00-00 00:00:00','0000-00-00','1','1'),(4,1,90,1,2017,9999,'2017-09-19','2017-09-19','pkpgkp=gkg',NULL,NULL,'0000-00-00 00:00:00','0000-00-00','1','1'),(5,1,91,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(6,3,92,1,2017,9999,'2017-09-19','2017-09-19','12568972255',NULL,NULL,'0000-00-00 00:00:00','0000-00-00','1','1'),(7,1,325,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','','0000-00-00 00:00:00','0000-00-00','1','0'),(8,1,493,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(9,1,494,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(10,1,496,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(11,1,497,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(12,1,498,1,2017,9999,'2017-09-08','2017-09-08','12568972255',NULL,NULL,'0000-00-00 00:00:00','0000-00-00','1','1'),(13,1,499,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(14,1,500,1,2017,9999,'2017-09-07','2017-09-07','juhuhhhh',NULL,NULL,'0000-00-00 00:00:00','0000-00-00','1','0'),(15,1,501,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(16,1,502,1,2017,9999,'2017-09-08','2017-09-08','12568972255',NULL,NULL,'0000-00-00 00:00:00','0000-00-00','1','1'),(17,1,512,1,2016,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(18,1,503,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(19,1,504,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(20,1,520,1,2017,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(21,1,505,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(22,1,506,1,2015,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(23,1,524,1,2017,9999,'0000-00-00','0000-00-00','','0000-00-00','0','0000-00-00 00:00:00','0000-00-00','1','0'),(24,1,521,1,2017,9999,'0000-00-00','0000-00-00','',NULL,NULL,'2017-09-08 16:48:24',NULL,'-1','1'),(25,1,523,1,2017,9999,'0000-00-00','2017-09-08','',NULL,NULL,'2017-09-08 16:50:17',NULL,'-1','1');
/*!40000 ALTER TABLE `epad_enseignants_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `epeda_personnel_etablissement`
--

DROP TABLE IF EXISTS `epeda_personnel_etablissement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epeda_personnel_etablissement` (
  `id_ens` int(11) NOT NULL,
  `matricule_ens` varchar(20) NOT NULL,
  `ien_ens` varchar(255) NOT NULL,
  `prenom_ens` varchar(255) NOT NULL,
  `nom_ens` varchar(255) NOT NULL,
  `sexe_ens` enum('H','F') NOT NULL,
  `date_nais` varchar(255) NOT NULL,
  `lieu_nais` varchar(250) NOT NULL,
  `numero_autorise` varchar(100) DEFAULT NULL,
  `profil_aca` varchar(255) DEFAULT NULL,
  `profil_pro` varchar(255) DEFAULT NULL,
  `code_specialite` int(11) DEFAULT NULL,
  `corps_ens` varchar(20) NOT NULL,
  `grade_ens` varchar(20) NOT NULL,
  `date_entree_enseignement` varchar(50) DEFAULT NULL,
  `date_entree_fonction` varchar(50) DEFAULT NULL,
  `CNI_ens` bigint(20) DEFAULT NULL,
  `situation_matri_ens` varchar(20) DEFAULT NULL,
  `tel_ens` int(11) DEFAULT NULL,
  `email_ens` int(11) DEFAULT NULL,
  `etat_ens` int(1) NOT NULL,
  PRIMARY KEY (`id_ens`),
  UNIQUE KEY `enseignants_ien_ens` (`ien_ens`),
  UNIQUE KEY `enseignants_matricule` (`matricule_ens`),
  KEY `code_specialite` (`code_specialite`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `epeda_personnel_etablissement`
--

LOCK TABLES `epeda_personnel_etablissement` WRITE;
/*!40000 ALTER TABLE `epeda_personnel_etablissement` DISABLE KEYS */;
INSERT INTO `epeda_personnel_etablissement` VALUES (1,'1234546','123456','bakary','Sane','H','','',NULL,'','',1,'','','','',0,'',0,0,0),(2,'665174/G','P000002','BAYE MOR','FAYE','H','1984-10-21','Dakar',NULL,'2','1',9,'','','0000-00-00',NULL,1,'Celibataire',779493637,0,0),(3,'647560/J','P000003','CHRISTINE ANGE COLETTE','DIATTA','F','1976-03-26','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775503398,0,0),(4,'636619/F','P000004','FATOUMATA','DEMBELE','F','1982-04-02','Dakar',NULL,'-99','-99',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(5,'386285/B','P000005','MAMADOU','DIALLO','H','1961-03-05','Dakar',NULL,'-99','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',775486653,0,0),(6,'170802031/R','P000006','MARIAMA','DIOUF','F','1983-04-25','Dakar',NULL,'-99','-99',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775125669,0,0),(7,'645871/E','P000007','MARIEME ','DIALLO','F','1983-03-28','Dakar',NULL,'-99','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776950062,0,0),(8,'516514/B','P000008','MOUHAMADOU','BOP','H','1966-08-05','Dakar',NULL,'-99','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',775506071,0,0),(9,'602481/K','P000009','OUMAR','DIOP','H','1968-12-05','Dakar',NULL,'-99','-99',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(10,'381327/L','P000010','ALIOU','FAYE','H','1959-02-06','Dakar',NULL,'-99','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(11,'607352/M','P000011','AMINATA','MBAYE','F','1975-02-10','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775138017,0,0),(12,'630600/C','P000012','AMINATA','SENE','F','1976-07-23','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775603976,0,0),(13,'677346/A','P000013','ASTOU','DIBA','F','1979-08-22','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(14,'657260/L','P000014','DABA','SOURANG','F','1984-09-07','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(15,'685004/A','P000015','EL HADJI','NIANG','H','1982-07-01','Dakar',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(16,'607329/C','P000016','FANDIGUE','GUEYE','F','1970-08-24','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774118139,0,0),(17,'652660/C','P000017','FATMA','FALL','F','1974-01-30','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(18,'667405/B','P000018','KHADY','SANE','F','1976-09-04','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(19,'670252/I','P000019','MAMADOU','NDOYE','H','1972-06-15','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(20,'628630/I','P000020','MAMADOU HADI','GAYE','H','1973-01-10','Dakar',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(21,'684935/I','P000021','NOGAYE','THIOUNE','F','1980-10-04','Dakar',NULL,'10','1',9,'','','0000-00-00',NULL,2,'Celibataire',777393129,0,0),(22,'645158/C','P000022','ROKHAYA','SOW','F','1968-02-15','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(23,'511227/B','P000023','AMINATA','BAKHOUM ','F','1964-10-27','Dakar',NULL,'-99','-99',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(24,'684116/D','P000024','AWA','SAKHO','F','1982-11-16','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775272663,0,0),(25,'652313/I','P000025','BABACAR','SALL','H','1977-02-09','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(26,'656918/I','P000026','BOURSINE','DIOME','H','1980-10-10','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',777578081,0,0),(27,'678029/Z','P000027','CHEIKHOUNA','FAYE','H','1980-05-08','Dakar',NULL,'4','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(28,'629312/I','P000028','FATOU','SECK','F','1974-03-15','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'Celibataire',776026891,0,0),(29,'633772/D','P000029','IBRA','DIOP','H','1975-03-14','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(30,'675044/D','P000030','KATY','KONATE','F','1978-01-10','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(31,'662211/Z','P000031','KHADY','TRAORE','F','1981-02-13','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(32,'645934/C','P000032','KHADY','DIOP','F','1971-04-13','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(33,'673454/A','P000033','LALA','SANE','F','1971-10-28','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Veuve',774400175,0,0),(34,'368113/B','P000034','MARIAMA','DIAGNE','F','1957-08-19','Dakar',NULL,'1','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',771791600,0,0),(35,'653644/B','P000035','NDEYE YACINE','THIOBANE','F','1976-08-24','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(36,'667394/I','P000036','PAPA BABACAR','NDIAYE','H','1985-06-10','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(37,'622119/C','P000037','ROKHYA','GUEYE','F','1969-06-03','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776591744,0,0),(38,'373719/P','P000038','YANKHOBA','NDIAYE','H','1957-12-31','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(39,'220152008/J','P000039','BERTRAND NDOFFENE','SAGNE','H','1988-11-20','Dakar',NULL,'3','-99',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(40,'604905/D','P000040','COURA YAGO','DIOP','F','1969-07-11','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779228540,0,0),(41,'386149/E','P000041','GANNA OWENS','NDIAYE','F','1959-12-17','Dakar',NULL,'19','2',9,'','','0000-00-00',NULL,2,'Veuve',0,0,0),(42,'518574/J','P000042','MAGATTE FALL','DIENE','F','1970-06-07','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776337281,0,0),(43,'600091/H','P000043','MAIMOUNA','BALDE','F','1970-08-12','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',6597700,0,0),(44,'674399/Z','P000044','MARIETOU','GUENE','F','1971-03-11','Dakar',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775226692,0,0),(45,'658541/G','P000045','MOUSCOUTA','SOUARE','F','1978-01-05','Dakar',NULL,'10','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775603387,0,0),(46,'602583/H','P000046','MOUSSA','NDOYE','H','1974-04-28','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775562268,0,0),(47,'140107018/D','P000047','NDEYE ARAME','KANE','F','1986-09-29','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(48,'640611/D','P000048','NDEYE GNAGNE','FAYE','F','1972-05-13','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775211362,0,0),(49,'659254/J','P000049','OUSMANE KECOUTA','SAMBOU','H','1979-11-13','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776576954,0,0),(50,'622933/D','P000050','SALIMATA','GUEYE','F','1972-04-16','Dakar',NULL,'2','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',776548249,0,0),(51,'658138/C','P000051','SEYNI','BADJI','H','1970-02-15','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(52,'514140/K','P000052','TAFSIR MACTAR','DIOUF','H','1969-02-27','Dakar',NULL,'1','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(53,'629116/G','P000053','ABDOULAYE','DIOUF','H','1971-03-04','Dakar',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',773057420,0,0),(54,'','P000054','ADAMA','GUEYE','F','1986-09-25','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775089022,0,0),(55,'190303034/M','P000055','AIDA','SENE','F','1987-01-03','Dakar',NULL,'3','-99',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775984932,0,0),(56,'140101014/B','P000056','AMY NDOYE','FALL','F','1983-02-13','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779126921,0,0),(57,'220152003/E','P000057','EL HADJI SOULEYMANE','DIALLO','F','1986-08-15','Dakar',NULL,'2','-99',9,'','','0000-00-00',NULL,1,'Celibataire',773019839,0,0),(58,'667398/E','P000058','FATOU KINE ','DICKO','F','1982-11-30','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775042615,0,0),(59,'684615/H','P000059','FATOU SY','BA','F','1977-11-04','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',774577571,0,0),(60,'653635/D','P000060','KHADY SY','FALL','F','1972-05-16','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779996614,0,0),(61,'674432/Z','P000061','MAIRAME','GAYE','F','1972-08-05','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775741501,0,0),(62,'653646/D','P000062','MARIAMA','NDIAYE','F','1969-01-06','Dakar',NULL,'8','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776094438,0,0),(63,'667396/G','P000063','NDEYE','DOGUE','F','1974-02-02','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775180222,0,0),(64,'634233/E','P000064','OUMAR','DIOUF','H','1973-01-01','Dakar',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',775868754,0,0),(65,'684932/F','P000065','SALIMATA','WADE','F','1983-12-13','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772305017,0,0),(66,'509404/F','P000066','SOULEYMANE','TALL','H','1962-03-10','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',775241679,0,0),(67,'626260/N','P000067','ABDOULAYE','LO','H','1972-10-19','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',773638295,0,0),(68,'688365/D','P000068','ABDOULAYE','DANSOKO','H','1972-08-20','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',706254363,0,0),(69,'507122/K','P000069','ABOBIKIRIE','BA','H','1961-05-20','Dakar',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',775434163,0,0),(70,'602431/F','P000070','ADAMA','SECK','H','1970-11-15','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774368723,0,0),(71,'633087/G','P000071','ADAMA','THIAM','H','1973-01-28','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775770577,0,0),(72,'633800/B','P000072','ASSANE','FALL','H','1973-01-18','Dakar',NULL,'-99','-99',-99,'','','0000-00-00',NULL,1,'Celibataire',778016349,0,0),(73,'690332/E','P000073','FATOU','CISSE','F','1971-11-25','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777821127,0,0),(74,'669648/A','P000074','KHADIDIA','GUEYE','F','1988-01-15','Dakar',NULL,'2','2',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(75,'514343/F','P000075','MAMADOU','SY','H','1966-01-06','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',771872628,0,0),(76,'601364/F','P000076','MAMADOU ALIOU','DIOUF','H','1973-01-17','Dakar',NULL,'2','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776576363,0,0),(77,'646043/I','P000077','MAME AMINATA','DIOP','F','1975-06-01','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775405467,0,0),(78,'636594/I','P000078','MARIETOU','SARR','F','1985-04-19','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776159049,0,0),(79,'662217/F','P000079','NDIENGHA','SAMB','F','1980-09-21','Dakar',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(80,'518638/A','P000080','SERIGNE BAMBA','SYLLA','H','1966-03-31','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,1,'Celibataire',770663721,0,0),(81,'511323/A','P000081','SERIGNE MOHAMED DIARRA','DIOP','H','1968-09-05','Dakar',NULL,'1','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(82,'385 577/E','P000082','SOKHNA ASTOU SECK HANE','DIOUF','F','1959-10-05','Dakar',NULL,'19','2',-99,'','','0000-00-00',NULL,2,'MariÃ©( e )',775355055,0,0),(83,'130110053/D','P000083','AMINTA BAL','HANE','F','1974-02-20','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775584665,0,0),(84,'662958/J','P000084','FATOU BA','LACOUTURE','F','1976-02-17','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774200600,0,0),(85,'653878/E','P000085','HELENE MARIE','BAKHOUM ','F','1977-09-25','Dakar',NULL,'1','1',-99,'','','0000-00-00',NULL,2,'MariÃ©( e )',778110177,0,0),(86,'604555/E','P000086','ADAMA','THIOUNE','F','1974-03-12','Dakar','','1','1',9,'','','12/06/2000','12/06/2010',2,'MariÃ?Â©(e)',775324675,0,0),(87,'683824/I','P000087','AÃCHATOU IBRAHIMA','KANE','F','1986-03-13','Dakar',NULL,'1','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',776298471,0,0),(88,'090601063/S','P000088','AMADOU MOUSTAPHA','NIANG','H','1966-10-26','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',772040691,0,0),(89,'505061/O','P000089','ASSANE','GUEYE','H','1905-05-13','',NULL,'-99','-99',16,'','','0000-00-00',NULL,0,'Celibataire',0,0,0),(90,'519147/I','P000090','EL IBRAHIMA','KASSE','H','1968-07-14','MbackÃ©',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',767587454,0,0),(91,'663078/D','P000091','FATIMATOU BINTOU ','DIAW','F','1971-09-15','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774558389,0,0),(92,'512373/G','P000092','FATOU','WANGARA','F','1967-06-16','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,2,'MariÃ©( e )',775576783,0,0),(93,'666533/A','P000093','JOSEPHINE','GOUDIABY','F','1971-05-18','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777853252,0,0),(94,'660065/A','P000094','MAÃRAMA','DIALLO','F','1979-08-02','GayÃ©',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774394643,0,0),(95,'601315/Z','P000095','MAODO','MBAYE','H','1967-08-28','Dakar',NULL,'3','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',770315316,0,0),(96,'673047/A','P000096','MARIETOU','DIOP','F','1974-11-14','Dakar',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775470872,0,0),(97,'653791/E','P000097','MODOU','NDIAYE','H','1973-01-08','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',770263279,0,0),(98,'190107004/L','P000098','NDEYE AMINATA ','GAYE','F','1988-03-07','Mbour',NULL,'2','-99',9,'','','0000-00-00',NULL,2,'Celibataire',777598928,0,0),(99,'150201030/J','P000099','NDEYE FATOU','FAYE','F','0301/1982','Bambey',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779355487,0,0),(100,'170603020/Q','P000100','NDEYE YACINE ','SEYE','F','1989-10-08','Tivaouane',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773727563,0,0),(101,'629528/B','P000101','OUSMANE','SENE','H','1980-09-21','Ngothie',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774242420,0,0),(102,'631865/C','P000102','SALIMATA','BODIAN','F','10/001/1977','Makhamouda',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775843202,0,0),(103,'201503005/A','P000103','SAWDIATOU','LY','F','1992-07-30','ThiÃšs',NULL,'3','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',771123301,0,0),(104,'632109/E','P000104','SEYNABOU','SECK','F','1979-10-19','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',771618029,0,0),(105,'600383/H','P000105','ADAMA','BA','H','1973-09-15','Thieppe',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(106,'600347/Z','P000106','ASSANE','BOYE','H','1974-01-23','Dakar',NULL,'2','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775566357,0,0),(107,'389402/B','P000107','BALSO GAKOU','GUEYE','F','1960-08-02','Dakar',NULL,'1','2',-99,'','','0000-00-00',NULL,2,'MariÃ©( e )',776327454,0,0),(108,'636608/E','P000108','FATIMATA','DIALLO','F','1981-09-06','Dagana',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775618920,0,0),(109,'501147/B','P000109','KHADY','SYLLA','F','1964-05-06','Dakar',NULL,'-99','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(110,'170107001/M','P000110','KHADY','BASSE','F','1988-03-13','Faoye',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',777850713,0,0),(111,'500351/F','P000111','MAMADOU HAMATH','THIAM','H','1961-07-11','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776513815,0,0),(112,'140101066/E','P000112','MAME BIGUE','SYLLA','F','1985-02-15','Diourbel',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776820568,0,0),(113,'663479/C','P000113','NDEYE FATMA','GUEYE','F','1974-10-10','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774545113,0,0),(114,'662209/L','P000114','RAMATOULAYE TAINE','GUEYE','F','1980-05-29','Saint-Louis',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(115,'20152006/H','P000115','SAWDIATA','SIMAL','F','1987-08-15','',NULL,'3','-99',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(116,'516005/E','P000116','SEYNABOU ','DIOP','F','1967-05-10','Rufisque',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774259976,0,0),(117,'667395/H','P000117','FATOU KINE','NDIAYE','F','1980-05-09','Kaolack',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',771041010,0,0),(118,'382624/K','P000118','AMADOU','FALL','H','1959-05-23','DAKAR',NULL,'1','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(119,'649462/K','P000119','FATOU','DIOH','F','1983-12-05','DAKAR',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(120,'140107038/F','P000120','MAMADOU','LO','F','1972-03-13','PIKINE',NULL,'1','1',16,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(121,'660449/I','P000121','MAMADOU','BA','H','1984-08-07','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(122,'180107001/N','P000122','NDEYE KHARDIATA MARIEME','SENE','F','1984-06-27','DAKAR',NULL,'2','-99',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(123,'638157/H','P000123','SAM','DABO','H','1971-10-13','DAKAR',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(124,'674116/C','P000124','SEYNABOU','GUEYE','F','1986-12-16','PIKINE',NULL,'2','2',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(125,'512485/E','P000125','SIDY','NDIAYE','H','1967-02-23','DAKAR',NULL,'1','2',-99,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(126,'671717/M','P000126','ABDOURAHMANE','FAYE','H','1967-06-10','Pikine',NULL,'1','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(127,'385498/C','P000127','BOURAMA','SOW','H','00/00/1959','Ouonck',NULL,'-99','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',773705035,0,0),(128,'110107019/Z','P000128','FATMA','THIOMBANE','F','1975-09-14','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',778348526,0,0),(129,'663976/E','P000129','FATOU','DIOP','F','1981-12-11','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'Celibataire',776132444,0,0),(130,'664938/J','P000130','FATOU  DIENG','SOW','F','1980-11-11','GuÃ©diawaye',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774563553,0,0),(131,'642675/Z','P000131','LAYTY NGUENAR','NDIAYE','F','1977-10-05','Ndiagagnao',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',770406070,0,0),(132,'654756/C','P000132','MAME  DIARRA','BODIAN','F','1984-06-30','Suelle',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772608364,0,0),(133,'671732/F','P000133','MARIAMA','TALL','F','1979-01-24','GuÃ©diawaye',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',775768457,0,0),(134,'636530/G','P000134','MARIAMA','DIAGNE','F','1974-02-11','Yoff',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775731063,0,0),(135,'621773/B','P000135','MOMAR','NIANG','H','1974-01-06','Pikine',NULL,'2','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775427390,0,0),(136,'641912/G','P000136','MOUSSA','SECK','H','1979-08-18','Guediawaye',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774549052,0,0),(137,'692001/B','P000137','NAFI','MBODJI','F','1983-09-09','Fatick',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775216622,0,0),(138,'669751/F','P000138','OUMY ELIMANE','NDIAYE','F','1982-11-10','Thiaroye/Mer',NULL,'1','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',773581142,0,0),(139,'667506/D','P000139','PROSPER HAMADE','FAYE','H','1984-08-07','Fatick',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',776117504,0,0),(140,'673048/B','P000140','SARATA','CISSE','F','1982-10-26','ThiÃ©s',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775071000,0,0),(141,'663024/A','P000141','SOULEYMANE','BA','H','1971-04-30','Donaye',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779355002,0,0),(142,'659691/L','P000142','AÃSSATOU','DIOUF','F','1967-03-23','Kaolack',NULL,'2','2',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(143,'645455/C','P000143','AISSATOU WADE','GUEYE','F','1981-02-08','',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(144,'638802/A','P000144','COUMBA','SY','F','1970-09-18','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(145,'609050/T','P000145','DJIB','FAYE','H','1975-11-08','Pout Diack',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775447286,0,0),(146,'633988/C','P000146','ELHADJ ','BADJI','H','1972-01-21','Thionk Essyl',NULL,'2','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779338067,0,0),(147,'610764/Z','P000147','ELHADJ BENFA','GUEYE','H','1969-07-16','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',776453206,0,0),(148,'678030/J','P000148','ELISABETH','SOW','F','1985-11-20','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773051242,0,0),(149,'389403/C','P000149','FANTA','GUEYE','F','1959-04-01','Dakar',NULL,'21','2',-99,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(150,'637772/H','P000150','FANTA','COULIBALY','F','1976-08-15','Yeumbeul',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777023975,0,0),(151,'689322/D','P000151','IBRAHIMA KHALIL ','GUEYE','H','1988-12-13','Dakar',NULL,'-99','-99',16,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(152,'180107002/M','P000152','KHADY  DIOUF','SENE','F','1986-08-23','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'Celibataire',772025203,0,0),(153,'516064/L','P000153','KHAR','SOUMARE','F','1967-01-01','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777044979,0,0),(154,'600173/I','P000154','MAIMOUNA','TALL','F','1974-12-10','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776191777,0,0),(155,'648227/C','P000155','NAFISSATOU','THIAM','F','1979-01-01','Ndande',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775645158,0,0),(156,'663025/Z','P000156','NDIAYA','SEYE','F','1969-11-11','Guinaw rails',NULL,'1','1',16,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(157,'634579/Z','P000157','SALIMATA','TALL','F','1971-11-23','Halwar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(158,'677354D','P000158','AISSATOU','DIARRA','F','1986-02-12','Ziguinchor',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777568771,0,0),(159,'110503132/H','P000159','ASSANATOU','SABALY','F','1983-01-13','Velingara',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774157113,0,0),(160,'512477/B','P000160','AWA','NDAW','F','1964-11-03','Dakar',NULL,'21','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776673538,0,0),(161,'627497/I','P000161','CHEIKH OUMY','WADE','H','1973-10-10','ThiÃšs',NULL,'-99','-99',9,'','','0000-00-00',NULL,1,'Celibataire',776503813,0,0),(162,'667399D','P000162','DJIBY','GAYE','H','1975-09-27','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',771578097,0,0),(163,'10008038/C','P000163','ELH ALASSANE','DIOP','H','1975-02-16','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',777809637,0,0),(164,'674788/D','P000164','FRANCOISE','MENDY','F','1978-03-09','Guediawaye',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773734163,0,0),(165,'626251/L','P000165','IBRAHIMA','SARR','H','1970-04-14','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(166,'514965/G','P000166','MAHAMADOU','NIANG','H','1966-03-07','Pikine',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(167,'644848/F','P000167','MAIMOUNA','DIARRA','F','1976-04-04','Kaolack',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777062867,0,0),(168,'600336/Z','P000168','MOHAMED KHASSALY','SALL','H','1970-08-25','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776582323,0,0),(169,'389407/G','P000169','NDEYE FATOU','NDIAYE','F','1961-12-25','St Louis',NULL,'21','2',-99,'','','0000-00-00',NULL,2,'MariÃ©( e )',774300778,0,0),(170,'653766/C','P000170','SERIGNE SAMBA','MAR','H','1980-04-07','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776139486,0,0),(171,'670004/E','P000171','SOKHNA LENA','DIOUF','F','1976-01-25','Richardtoll',NULL,'2','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',776152740,0,0),(172,'651453/Z','P000172','SOUKHAYE','NDIAYE','F','1971-07-01','Ziguinchor',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774047650,0,0),(173,'644734/B','P000173','ABDOU','KANDJI','H','1972-07-09','Tivaoune',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775098676,0,0),(174,'660066/Z','P000174','ADAMA','SARR','F','1980-01-09','Pikine',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(175,'633986/A/ABSENT','P000175','ADOLPHE','SAGNA','H','27/01/0978','Djibonker',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(176,'632440/E','P000176','AISSATA','BA','F','1979-10-24','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',707863702,0,0),(177,'652319/H','P000177','ALASSANE','NDAO','H','1974-01-25','Keur NdÃ©nÃ© Ndao',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774572205,0,0),(178,'517874/F','P000178','AMADOU','GAYE','H','1970-01-01','Dagalma',NULL,'20','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775379815,0,0),(179,'630544/B','P000179','CHEIKH MAMINA','BADJI','H','1978-06-05','Thiobon',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',776481345,0,0),(180,'609348/H','P000180','FATOU','NDIAYE','F','1965-08-15','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775776068,0,0),(181,'685123/A','P000181','FATOU ','DRAME','F','1989-01-08','Pikine',NULL,'2','2',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',772674124,0,0),(182,'131003097/F','P000182','HABIB','FAYE','H','1980-03-11','Diass',NULL,'2','-99',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776497556,0,0),(183,'140107032L','P000183','MAGUATTE','SY','F','1973-11-26','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772161023,0,0),(184,'652961/A','P000184','MAIMOUNA','DIATTA','F','1978-05-13','Mlomp',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775241703,0,0),(185,'389969/H','P000185','MAME KHARDIATA','TOURE','F','1958-03-30','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,2,'MariÃ©( e )',776208842,0,0),(186,'120107032/J','P000186','MARIAMA','KOUYATE','F','1974-06-09','Keur Madiabel',NULL,'1','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',779358780,0,0),(187,'693810/G','P000187','NDEYE KATY','CAMARA','F','1984-01-02','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774189924,0,0),(188,'668864/B','P000188','NDEYE SOCE','NDAW','F','1981-01-04','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',762763593,0,0),(189,'650775/D','P000189','ROKHAYA','NIANG','F','1977-12-26','Bargny',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776563414,0,0),(190,'670082/E','P000190','YACINE ','DIOP','F','1975-06-22','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772728895,0,0),(191,'518576/H','P000191','MALICK','DIENG','H','1970-03-27','Pikine',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(192,'513242/G','P000192','ABDOU SALAM','DIALLO','H','1963-06-15','Alwar',NULL,'1','2',-99,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(193,'500230/F','P000193','AIDA ','NDIAYE','F','1960-03-01','Tivaouane',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(194,'651233/Z','P000194','AMINATA','SANE','F','1974-04-27','BALINGORE',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(195,'637352/J','P000195','FATOU','CAMARA','F','1968-06-10','Pout',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(196,'644658/C','P000196','KHADIDIATOU','DIENG','F','1976-08-03','Louga',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(197,'668868/B','P000197','MAMADOU','SENE','H','1978-12-30','Fatick',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(198,'110301269/A','P000198','MarÃšme','MBENGUE','F','1975-02-17','GuinguinÃ©o',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(199,'606547/D','P000199','MARIAMA','FAYE','F','1971-03-01','FATICK',NULL,'2','2',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(200,'517605/Z','P000200','MARIE THERESE','MANSAL','F','1974-04-24','Ziguinchor',NULL,'1','2',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(201,'515067/H','P000201','MODOU KANE','NIANG','H','1972-06-18','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(202,'604843/C','P000202','MOUSSA','SARR','H','1975-02-05','Bakhdar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(203,'170107006/H','P000203','NAFISATOU','GUEYE','F','1990-01-04','Dakar',NULL,'1','1',16,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(204,'644711/A','P000204','NDEYE ANTA','NDIAYE','F','1980-06-02','Keur Moussa',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(205,'668099/H','P000205','OUSMANE','NIANG','H','1984-03-14','LOUGUE TOROBE',NULL,'2','1',16,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(206,'638886/E','P000206','SALY','DIANKO','F','1977-07-02','DassilamÃ©',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(207,'657294/K','P000207','AMINATA','DRAME','F','1974-10-31','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(208,'140107022/K','P000208','BINETOU','LOUM','F','1987-08-15','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(209,'665275/E','P000209','ELHADJI ABIBOU','SALL','H','1971-05-05','DIMISKHA',NULL,'1','1',16,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(210,'626500/E','P000210','HAWO','GUEYE','F','1975-06-09','NDIOUM',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(211,'674401/B','P000211','MARIEME BARO','DIARRA','F','1985-10-09','CambÃ©rÃšne',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(212,'120107022/I','P000212','ROKHAYA','NDIAYE','F','1975-12-30','dakar',NULL,'2','-99',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(213,'669750/G','P000213','SEYNABOU','KA','F','1984-03-07','Mabo',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(214,'130303088/H','P000214','ABDOURAHMANE','FEDIOR','H','1984-09-16','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',773298066,0,0),(215,'669186/J','P000215','ADJARATOU SODA','DIAGNE','F','1987-05-07','Dakar',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774517649,0,0),(216,'220152007/I','P000216','AICHATOU','SECK','F','1991-11-05','SÃ©bikhotane',NULL,'2','-99',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773727291,0,0),(217,'632217/C','P000217','AMINATA','FALL','F','1972-08-04','Kaolack',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774394296,0,0),(218,'517889/B','P000218','ASSANE','SEMBENE','H','1970-11-11','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',777239373,0,0),(219,'660861/C','P000219','BODE','SANKHE','F','1979-12-31','Touba',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776439826,0,0),(220,'632420/C','P000220','DAOUDA','ANNE','H','1967-12-06','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',777131032,0,0),(221,'694538/I','P000221','DIEYNABOU','SEYDI','F','1984-03-18','Sare Samboudiang',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772275601,0,0),(222,'645870/F','P000222','MAME MBEUGUE','THIOUNE','F','1975-08-28','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775329398,0,0),(223,'641295/E','P000223','MOMAR LYSSA','NDIAYE','H','1972-10-11','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775509900,0,0),(224,'684613/S','P000224','MOUSSA','AW','H','1988-11-18','Pikine',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',773723728,0,0),(225,'604865/C','P000225','PAPA BAIDY','SALL','H','1966-02-15','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',771813698,0,0),(226,'664655/B','P000226','PAPE SAMBA','NDAO','H','1988-12-03','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776959520,0,0),(227,'668660/Z','P000227','TACKO','BA','F','1981-04-18','Tiguere YÃ©nÃ©',NULL,'1','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',773810501,0,0),(228,'NI','P000228','YVES LEYE','CAMARA','H','1984-05-19','Dakar',NULL,'2','-99',9,'','','0000-00-00',NULL,1,'Celibataire',779265751,0,0),(229,'601482/I','P000229','ABDOULAYE','FALL','H','1971-05-17','Tambacounda',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(230,'602006/B','P000230','ABDOULAYE','DIASSE','H','1974-04-07','Kaolack',NULL,'4','15',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(231,'601479/A','P000231','ALLE','DIOUF','H','1965-03-09','ThiÃšs',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(232,'517730/G','P000232','AMADOU','NDOUR','H','1967-03-03','NguÃ©khokh',NULL,'2','15',16,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(233,'386543/C','P000233','ASTOU','BADIANE','F','1960-04-15','',NULL,'1','1',-99,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(234,'511315/B','P000234','CHEIKH AMALAH','KEITA','H','1963-03-23','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(235,'630121/C','P000235','CHEIKH MBACKE','SECK','H','1978-03-07','Nguidile',NULL,'1','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(236,'633364/E','P000236','CHEIKHNA SAAD BOUH','SECK','H','1971-02-02','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(237,'514264/H','P000237','DIARY','BA','F','1964-03-07','Dakar',NULL,'2','15',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(238,'514185/J','P000238','DINE','DIOP','H','1968-09-10','GuinguinÃ©o',NULL,'2','15',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(239,'383348/I','P000239','ELIMANE','MBOW','H','1959-09-13','Kaolack',NULL,'2','2',16,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(240,'514878/A','P000240','FARBA','SENGHOR','H','1964-08-25','Mbassis',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(241,'510236/A','P000241','ISMAILA  ','DIALLO','H','1962-05-25','Dakar',NULL,'11','15',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(242,'376005/C','P000242','KHADY','BOYE','F','1958-10-10','Fatick',NULL,'7','19',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(243,'651773/A','P000243','KHADY','KEBE','F','1964-02-02','Coki',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(244,'516449/A','P000244','MAKHTAR','GUEYE','H','19//01/1968','Pikine',NULL,'-99','15',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(245,'516466/F','P000245','MAMADOU','MBAYE','H','1965-03-02','Pikine',NULL,'21','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(246,'602857/B','P000246','MOR   ','DIAGNE','H','1974-04-10','Ndjiliwene',NULL,'2','15',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(247,'517837/A','P000247','SOULEYMANE','NDIAYE','H','1970-11-22','Tambacounda',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(248,'691818/Q','P000248','AMADOU','WADE','H','1974-06-07','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775022964,0,0),(249,'668867/A','P000249','ASSIETOU','SENE','F','1976-10-18','GuinguinÃ©o',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(250,'667401/B','P000250','AWA','DIOP','F','1970-02-24','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776564256,0,0),(251,'513229/B','P000251','BARKA','CAMARA','H','1965-03-15','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775000017,0,0),(252,'683850/B','P000252','BINTOU','KOITE','F','1984-03-13','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776099245,0,0),(253,'652178/A','P000253','FATOUMATA BINTOU','DIEYE','F','1978-12-25','Kayar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775203557,0,0),(254,'504317/Z','P000254','LIBASSE','DIOP','H','1962-12-18','Dakar',NULL,'20','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',775567264,0,0),(255,'602.579/A','P000255','MAMADOU AMADOU','DIAW','H','1972-03-12','FondÃ© Elimane',NULL,'8','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775677313,0,0),(256,'644885/A','P000256','NGOR','NDIAYE','H','1978-05-10','Diohine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774360927,0,0),(257,'642.596/B','P000257','THIERNO  ABDOURAHMANE','DIALLO','H','1978-11-01','Pikine',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',775524188,0,0),(258,'691100/C','P000258','AHMADOU','LO','H','1982-03-16','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(259,'514901/B','P000259','ALIOU','DIENG','H','1966-03-26','Pikine',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',775550284,0,0),(260,'649716/A','P000260','SAOUDIATOU','DIEME','F','1972-04-04','Kandiou',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(261,'693480/D','P000261','AMINATA','DIOP','F','1978-09-11','Thies',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776510318,0,0),(262,'519177/L','P000262','ARONA','NDIAYE','H','1976-03-11','MÃ©dina Gounass',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',773023199,0,0),(263,'675863A','P000263','EL HADJI BATHIE','MBAYE','H','1971-01-14','Thies',NULL,'1','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',775533918,0,0),(264,'675054/E','P000264','JEAN MARIE','NDECKY','H','1963-12-12','Marsassoum',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775196048,0,0),(265,'633343/D','P000265','KHADY','FAYE','F','1983-06-09','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775605484,0,0),(266,'160301108/Z','P000266','SOPHIE PIERRE','NDOUR','F','1985-03-20','Ndoffane',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773661800,0,0),(267,'10600688/F','P000267','THIANE','KANE','F','1978-04-22','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775798486,0,0),(268,'602138/B','P000268','ADAMA','DIOP','H','1974-11-01','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(269,'657330/H','P000269','ADJARATOU FATOU','TOP','F','1982-06-20','Koungheul',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773064426,0,0),(270,'678028/A','P000270','ALIOUNE','DIOP','H','1984-02-10','Thies',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',770410672,0,0),(271,'667400/C','P000271','ALIOUNE','DIALLO','H','1968-03-16','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775434126,0,0),(272,'635253/H','P000272','AMADOU  MATAR','DIOP','H','1970-06-22','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775469323,0,0),(273,'664818/K','P000273','BACARY','MANGA','H','1987-06-05','Diabir Kindiong',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',771596622,0,0),(274,'500 449/D','P000274','BADARA','KEITA','H','1961-11-13','Dakar',NULL,'-99','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',770793620,0,0),(275,'604632/E','P000275','BIRANE','SECK','H','1971-05-26','Kaolack',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774487850,0,0),(276,'602338/Z','P000276','CODE','FAYE','H','1970-06-21','Ouadiour',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775055185,0,0),(277,'130106006/C','P000277','FATOU','FAYE','F','1968-03-29','Dakar',NULL,'1','-99',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(278,'220152001/C','P000278','GAISSIRY','GUEYE','F','1991-04-08','Podor',NULL,'2','-99',9,'','','0000-00-00',NULL,2,'Celibataire',770125869,0,0),(279,'654286/E','P000279','HABIB','FALL','H','1980-11-08','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776341680,0,0),(280,'671731/E','P000280','MAGATTE','MBOUP','F','1971-11-12','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773165867,0,0),(281,'688366/C','P000281','MAIMOUNA THIAM','TALL','F','1976-09-30','Diourbel',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779042031,0,0),(282,'670081/F','P000282','MAME MBAGNICK','FAYE','F','1984-12-10','Guediawaye',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774161677,0,0),(283,'220152005/G','P000283','MARIAMA','KAMARA','F','1992-05-12','GorÃ©e',NULL,'2','-99',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773068136,0,0),(284,'684936/J','P000284','NDEYE FATMA','DIALLO','F','1986-04-03','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776353969,0,0),(285,'673733/E','P000285','OUMOUL KHAIRI ','FAYE','F','1986-09-02','Pikine',NULL,'1','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',777744249,0,0),(286,'604680/L','P000286','SALAMATA','DIALLO','F','1975-03-06','Thies',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775578173,0,0),(287,'631284/F','P000287','SOKHNA AWA','GUEYE','F','1980-06-08','Guediawaye',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774215658,0,0),(288,'667 397 /F','P000288','ABDOULAYE','FALL','H','1986-03-29','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',772432202,0,0),(289,'688067/E','P000289','AISSATOU ','YATE','F','1983-12-13','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775517554,0,0),(290,'657 924 /C','P000290','AMINATA ','SALL','F','1980-07-21','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779670266,0,0),(291,'687378/A','P000291','DJINDA','KANE','H','1981-06-19','Nguidjilone',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775528453,0,0),(292,'684617/J','P000292','FATOU ','SAGNA','F','1975-03-08','Djiguinoum',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777346257,0,0),(293,'376 593 /C','P000293','HAMIDOU ','BA','H','1958-10-21','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',777936220,0,0),(294,'684931/E','P000294','MOHAMADOU ABDOULAYE','DIOP','H','1982-08-04','Yeumbeul',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',775044863,0,0),(295,'646 926 /E','P000295','SAMBA KHARY','SENE','H','1978-12-05','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776092397,0,0),(297,'381474/E','P000297','AIDA','BA','F','1958-09-21','Dakar',NULL,'2','-99',-99,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(298,'160603044/N','P000298','AISSATOU','GUEYE','F','1971-11-05','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(299,'180107003/L','P000299','DIOR','DIOP','F','1986-09-25','Mbour',NULL,'2','-99',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(300,'509236/I','P000300','LEMOU','YOUM','F','1961-09-15','Dakar',NULL,'2','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(301,'517496/J','P000301','MAIMOUNA','SECK','F','1970-10-11','Tamnbacounda',NULL,'2','2',9,'','','0000-00-00',NULL,3,'Celibataire',0,0,0),(302,'650774/C','P000302','MAREME','MBAYE','F','1974-10-30','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(303,'674743/C','P000303','NDEYE ROKHAYA','THIOUNE','F','1970-11-19','Louga',NULL,'2','1',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(304,'500212/B','P000304','ABDOU','KEBE','H','1959-01-01','Taiba Khadre',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(305,'671346/E','P000305','ABDOULAYE','MBAYE','H','1981-08-21','Pikine',NULL,'1','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',77563126,0,0),(306,'190107007/I','P000306','ADJI MAGUATTE','SOW','F','1987-04-16','Guediawaye',NULL,'3','2',9,'','','0000-00-00',NULL,2,'Celibataire',775568373,0,0),(307,'632627/F','P000307','AHMADOU MOUSTAPHA','BA','H','1966-02-20','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775432838,0,0),(308,'130106010/J','P000308','AISSATA','LO','F','1968-06-04','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',77440212,0,0),(309,'651738/J','P000309','AMINATA','CISSE','F','1976-01-26','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775795472,0,0),(310,'660647/I','P000310','ASTOU SATA POL','BEN-GELOUN','F','1978-08-27','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(311,'639295/N','P000311','DJARIATA','HANNE','F','1978-09-15','Yeumbeul',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775429750,0,0),(312,'663554/A','P000312','FAMBAYE','DIEYE','F','1976-01-05','Rao',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775573436,0,0),(313,'648327/B','P000313','FATOUMATA','SOW','F','1975-06-30','Kaolack',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773528047,0,0),(314,'644595/E','P000314','KHADY','FAYE','F','1968-09-07','Fimela',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776127302,0,0),(315,'659740/G','P000315','KHADY','AW','F','1977-10-02','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(316,'689160/L','P000316','MAKHTAR','FALL','H','1974-01-21','Louga',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774116442,0,0),(317,'514111/G','P000317','MBATHIO YARE','FALL','F','1968-01-30','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776465027,0,0),(318,'656014/D','P000318','MOUSSA','NDIAYE','H','1973-12-26','GuÃ©diawaye',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',555145009,0,0),(319,'601362/Z','P000319','NDEYE SYLLA','DIENG','F','1971-03-14','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775216048,0,0),(320,'621729/I','P000320','OUMAR','DIALLO','H','1968-08-12','Ndioum',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(321,'632561/E','P000321','OUSMANE','LY','H','1974-12-05','Medina Gounass',NULL,'1','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(322,'640140/E','P000322','OUSSEYNOU','THIOUNE','H','1975-02-10','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775553315,0,0),(323,'606430/K','P000323','ABDOULAYE','FALL','H','1963-02-14','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775509149,0,0),(324,'602003/E','P000324','AMINATA','WADE','F','1977-02-08','Diourbel',NULL,'1','1',9,'','','0000-00-00',NULL,2,'DivorcÃ©e',771796212,0,0),(325,'603589/C','P000325','CODE','DIAKHATE','H','1971-02-27','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779244419,0,0),(326,'640732/D','P000326','DIOBE','KA','F','1973-09-04','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774548289,0,0),(327,'656369/A','P000327','FAMA','MBAYE','F','1974-09-01','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773073268,0,0),(328,'632370/I','P000328','FATOU','DIALLO','F','1980-07-08','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773126304,0,0),(329,'638427/B','P000329','FATOUMATA','SENGHOR','F','1973-11-25','Mbassis',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(330,'632215/A','P000330','IBRAHIMA','NDIONE','H','1972-01-05','Poponguine',NULL,'1','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(331,'632554/A','P000331','MAMADOU','DIOUF','H','1966-08-17','Khenene',NULL,'2','2',16,'','','0000-00-00',NULL,1,'Celibataire',0,0,0),(332,'686624/D','P000332','MAMADOU DIAGNE','DIOUF','H','1976-02-10','Yeumbeul',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',704750286,0,0),(333,'220152002/D','P000333','MEDOUNE','CISSE','H','1988-01-15','Rufisque',NULL,'3','-99',9,'','','0000-00-00',NULL,1,'Celibataire',772692670,0,0),(334,'369825/E','P000334','MOR KHADY','NIANG','H','1905-05-10','Doundodji',NULL,'19','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(335,'602036/E','P000335','OUMAR','FAYE','H','1966-05-11','Dakar',NULL,'8','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775205212,0,0),(336,'659738/B','P000336','PAPE MALICK','SECK','H','1981-12-20','Guediawaye',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774324994,0,0),(337,'140301004/C','P000337','SEYNABOU','SYLLA','F','1984-10-01','ThiÃšs',NULL,'1','-99',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777039492,0,0),(338,'646544/C','P000338','SOUADOU','SONKO','F','1975-01-05','Djinone',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',770548745,0,0),(339,'674789/E','P000339','AISSATOU MAIRAME','NDIAYE','F','1981-02-22','Kaolack',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772197840,0,0),(340,'381317/M','P000340','AMADOU','SY','H','1957-07-19','Podor',NULL,'21','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',778153131,0,0),(341,'664664/Z','P000341','CHEIKH TIDIANE','MBOUP','H','1977-04-30','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779504314,0,0),(342,'653645/C','P000342','COUMBA','DIENE','F','1983-01-10','MÃ©ouane',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776473076,0,0),(343,'669752/E','P000343','DIOGOP','NDAO','F','1981-03-10','Ndiourbel Sabour',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776215230,0,0),(344,'220152011/B','P000344','DJIBRIL','LO','H','1987-01-09','MbackÃ©',NULL,'4','-99',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',763240070,0,0),(345,'649473/K','P000345','FATIMATA','DIA','F','1982-03-14','Nguidjilone',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775456549,0,0),(346,'665329/E','P000346','FATOU','GUEYE','F','1988-03-15','Kaolack',NULL,'2','2',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',775542228,0,0),(347,'383311/E','P000347','FATOU','KANE','F','1959-04-05','Kaolack',NULL,'2','2',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(348,'647314/C','P000348','IBRAHIMA','NDIONE','H','1978-08-10','Thicky',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774512203,0,0),(349,'220152010/A','P000349','JEAN MARIE','NDIAYE','H','1988-05-04','Nobandane',NULL,'4','-99',9,'','','0000-00-00',NULL,1,'Celibataire',776728334,0,0),(350,'662737/I','P000350','KHADY DIATOU','NDOUR','F','1981-01-03','KÃ©bÃ©mer',NULL,'2','2',9,'','','0000-00-00',NULL,2,'Celibataire',776055992,0,0),(351,'659741/F','P000351','MAIMOUNA','KANE','F','1976-09-18','Tamba',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776336649,0,0),(352,'615427/A','P000352','MAME ARAME','FALL','F','1967-11-14','Dakar',NULL,'8','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776156039,0,0),(353,'646437/Z','P000353','MARIAMA DIAKHATE','DIAW','F','1974-03-23','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775120156,0,0),(354,'627891/K','P000354','MARIAME','WONE','F','1972-09-09','ThiÃšs',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775473081,0,0),(355,'617772/J','P000355','NDEYE KATTY','AMINE','F','1972-12-07','Ndiaffate',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775575189,0,0),(356,'651618/K','P000356','PENDA','GAYE','F','1982-11-04','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772607179,0,0),(357,'604649/A','P000357','PENDA','GUEYE','F','1980-01-17','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774154771,0,0),(358,'130603065/L','P000358','SERIGNE MAMADOU  ','SAMB','H','1974-01-14','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',773262271,0,0),(359,'647334/E','P000359','SERIGNE MOUSTAPHA  ','DIAGNE','H','1982-10-25','Yeubeul',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',774319432,0,0),(360,'663699/C','P000360','ABABACAR','CISSE','H','1973-07-11','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776540014,0,0),(361,'604743/D','P000361','AHMED BALEIGNE','NDIAYE','H','1974-01-15','Dangalma',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',772465029,0,0),(362,'651911/G','P000362','AMINATA','NDIAYE','F','1984-05-24','Guinguineo',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777182805,0,0),(363,'688275/F','P000363','CHEIKH AHMEDOU','DIAW','H','1979-01-06','Pikine',NULL,'8','1',9,'','','0000-00-00',NULL,1,'Celibataire',779378358,0,0),(364,'508179/J','P000364','DIAKHARIA','TIMERA','H','1960-05-09','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(365,'622384/G','P000365','EL HADJI RAWANE','DIOP','H','1971-11-28','Kolda',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',778054512,0,0),(366,'626896/E','P000366','MAMADOU','DRAME','H','1968-03-18','Marsassoum',NULL,'8','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774276776,0,0),(367,'658276/H','P000367','MAMADOU MOUSTAPHA','FALL','H','1965-02-07','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779979460,0,0),(368,'631539/G','P000368','MOCTAR','NIANG','H','1974-10-05','Guede Village',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(369,'383350/Z','P000369','MOUSSA','NDIAYE','H','1957-03-19','Dakar',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(370,'603785/E','P000370','NDEYE NDOUMBE','SYLLA','F','1977-01-22','Diourbel',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779798974,0,0),(371,'500151/H','P000371','SADIO','DIAO','H','1959-03-23','Kolda',NULL,'21','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',772371247,0,0),(372,'656926/F','P000372','MAIRAME','FAYE','F','1979-12-18','Nguidjilone',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',779378939,0,0),(373,'645490/I','P000373','ABDOU','NIANG','H','1980-08-12','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(374,'644772/D','P000374','AMADOU MAKHTAR','BA','H','1971-10-06','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775324855,0,0),(375,'677 344/C','P000375','AMINATA','SALL','F','1986-06-04','Dakar',NULL,'1','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(376,'190107005/R','P000376','BINTA','HANE','F','1987-05-07','Kolda',NULL,'2','-99',9,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(377,'170403019/E','P000377','DIE','NIANE','F','12/04/1984/','Kaolack',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772466218,0,0),(378,'500204/A','P000378','FATOU','NIANG','F','1957-04-24','Dakar',NULL,'1','2',-99,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(379,'640692/C','P000379','FATOUMATA','BA','F','1982-05-07','Pire   ',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775313745,0,0),(380,'604621/E','P000380','JULIETTE','DIEME','F','1976-09-04','Batinding',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772160574,0,0),(381,'608017/H','P000381','LAMINE','SAGNA','H','1974-12-25','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(382,'628618/A','P000382','LAMINE AMADOU','DIOP','H','1969-11-14','BodÃ©',NULL,'8','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775593588,0,0),(383,'629155/L','P000383','MAMADOU','WADE','H','1969-02-03','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',773142290,0,0),(384,'667 393/J','P000384','MAMADOU','DIENG','H','1979-09-26','Dakar',NULL,'1','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(385,'668866/Z','P000385','MAME BOUSSO','DIOUF','F','1981-05-27','Kaolack',NULL,'1','1',9,'','','0000-00-00',NULL,2,'Celibataire',775448481,0,0),(386,'645924/D','P000386','MAME FAMA','SECK','F','1970-10-03','Koungueul',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775337301,0,0),(387,'641449/f','P000387','MOHAMED','SOW','H','1977-10-29','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(388,'604723/B','P000388','MOUSSA','FAYE','H','1975-08-31','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',773015966,0,0),(389,'170107009/E','P000389','SIRA','SALAM','F','1988-10-12','Yeumbeul',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773890247,0,0),(390,'130106012/E','P000390','SOKHNA KHOUMA','MBAYE','F','1980-01-28','Louga',NULL,'2','-99',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(391,'652433/A','P000391','ABDOULAYE DIEYE','NDIAYE','H','1968-10-24','Boulal',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',773295091,0,0),(392,'600200/D','P000392','AISSATOU SADIO','DIALLO','F','1967-11-10','Kolda',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777188465,0,0),(393,'637037/F','P000393','DIODO BA YAYA','NIANE','F','1982-09-18','Kanel',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775475278,0,0),(394,'605427/B','P000394','IBRAHIMA','DIOP','H','1968-10-10','Kaolack',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',774325278,0,0),(395,'607270/R','P000395','JOKEBE MARIAME','SAMBOU','F','1975-08-01','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775556932,0,0),(396,'190107003/N','P000396','MAHE','SARR','F','1988-03-03','Fatick',NULL,'2','-99',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',777810700,0,0),(397,'670080/G','P000397','MARIAME','KANE','F','1974-10-29','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'DivorcÃ©e',772244426,0,0),(398,'381535/K','P000398','MARIE ANTOINETTE','DIAGNE','F','1958-01-02','Kaolak',NULL,'2','2',-99,'','','0000-00-00',NULL,2,'MariÃ©( e )',773593563,0,0),(399,'140107001/J','P000399','NDEYE ROKHAYA','BA','F','1972-10-05','MÃ©ouane',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775895328,0,0),(400,'657340/I','P000400','NGONE','DIEYE','F','1970-11-25','MÃ©dina Gounass',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',709416990,0,0),(401,'386191/H','P000401','PAPA BOLY MBEUGUE','DIOUF','H','1962-01-07','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',772549689,0,0),(402,'639452/K','P000402','RAMATA','SOW','F','1976-04-06','GuÃ©diawaye',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775642491,0,0),(403,'651246/B','P000403','SAIDOU','NDIAYE','H','1979-08-18','DÃ©mette',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776200681,0,0),(404,'669097/K','P000404','SOKHNA AISSATOU','DIAGNE','F','1986-10-29','Pikine',NULL,'2','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',774565120,0,0),(405,'190107003/M','P000405','ABDOULAYE','BADJI','H','1989-02-11','Kartiack',NULL,'2','-99',9,'','','0000-00-00',NULL,1,'Celibataire',774408214,0,0),(406,'661939/N','P000406','AWA','NIANG','F','1977-10-08','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776467397,0,0),(407,'661917/N','P000407','FATOU','FALL','F','1986-08-08','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'Celibataire',776400707,140759,0),(408,'653767/D','P000408','HASSATOU','DIALLO','F','1977-06-12','Kolda',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775240923,0,0),(409,'383086/Z','P000409','MAGUEYE','SARR','H','1958-10-04','Mbar',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',774297254,0,0),(410,'628889/C','P000410','MAMADOU','DIALLO','H','1967-12-01','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775031437,0,0),(411,'675865/C','P000411','MARIAMA','DIOP','F','1971-09-03','Diourbel',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773151109,0,0),(412,'636446/C','P000412','SALIOU  ','GUEYE','F','1966-11-08','Dakar',NULL,'1','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',707740362,0,0),(413,'633 168/C','P000413','BADARA OULA','TAYE','H','1966-01-10','Kaolack',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775083212,0,0),(414,'684 930/D','P000414','BASSIROU','DIAO','H','1987-03-18','Kolda',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',771550646,0,0),(415,'690934/M','P000415','BOUNA','SY','H','1981-11-06','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779949161,0,0),(416,'669645//B','P000416','DIEYNABA','DIOP','F','1981-07-19','Pikine',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774371927,0,0),(417,'378 305/D','P000417','ELIMANE','DIENG','H','1905-05-09','Ngabane',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(418,'657292/M','P000418','FATIMATA','SOW','F','1976-05-16','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775426546,0,0),(419,'636 743/C','P000419','LALA','DIARRA','F','1979-01-28','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779288689,0,0),(420,'680 276/C','P000420','MAMADOU OUSMANE','DIOP','H','1974-09-15','Pikine',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',775025559,0,0),(421,'515 693/I','P000421','MOUSSA  FATOMA','BADIANE','H','1970-05-08','Boulandor',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775093930,0,0),(422,'668869/C','P000422','NDEYE AISSATOU','DIALLO','F','1980-03-02','Fimela',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775676007,0,0),(423,'685061/H','P000423','RAYMOND','DIEDHIOU','H','1977-01-11','Youtou Essoukaye',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776406171,0,0),(424,'603 840/E','P000424','ROKHAYA','YOUM','F','1976-05-04','Yeumbeul',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776589599,0,0),(425,'636 748/B','P000425','SIRA','NDIAYE','F','1979-07-04','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775637473,0,0),(426,'636 770/I','P000426','SOKHNA','SENE','F','1968-10-21','Yeumbeul',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774521646,0,0),(427,'633 633/Z','P000427','YACINE','DIAW','F','1979-01-01','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775359560,0,0),(428,'635360/K','P000428','AMADOU CAMARA','DIENE','H','1970-09-11','Yoff',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779903700,0,0),(429,'606356/H','P000429','SEYNABOU','SARR','F','1966-07-14','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773166883,0,0),(430,'645486/E','P000430','AIDA','SECK','F','1984-06-10','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774151880,0,0),(431,'653408/H','P000431','AIDA','NIANG','F','1971-03-23','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',781140892,0,0),(432,'651496/A','P000432','ALIOU','NGOM','H','1967-01-10','Bakakack',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774554372,0,0),(433,'140107042/M','P000433','AMINATA','LO','F','1988-11-21','Pikine',NULL,'1','1',16,'','','0000-00-00',NULL,2,'Celibataire',0,0,0),(434,'140601025/G','P000434','FATOU','MBODJ','F','1970-03-17','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774294278,0,0),(435,'509261/Q','P000435','IBRAHIMA','DRAME','H','1965-01-21','Mandina',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775684656,0,0),(436,'636699/C','P000436','MAGUETTE','NDIAYE','F','27/021981','MÃ©dina Gounass',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775587494,0,0),(437,'130107017/D','P000437','MAMADOU','MBAYE','H','1988-10-26','NguinguinÃ©o',NULL,'1','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',773620454,0,0),(438,'667392/K','P000438','NDEYE COUMBA','FALL','F','1987-10-05','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779037074,0,0),(439,'651452/A','P000439','OUMOU','FALL','F','1967-02-16','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775067313,0,0),(440,'685060/I','P000440','ARAME','DIENE','F','1978-09-27','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773587518,0,0),(441,'651902/I','P000441','ISMAILA FODE','DIALLO','H','1970-05-19','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',775151811,0,0),(442,'628902/A','P000442','SERIGNE TAIBA','TINE','H','1972-09-13','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',773655477,0,0),(443,'518849/A','P000443','ABDOULAYE','NDIAYE','H','1967-10-01','Pikine',NULL,'2','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775551147,0,0),(444,'675864/B','P000444','AIDA ','TRAORE','F','1983-12-06','Pikine',NULL,'2','1',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',773523168,0,0),(445,'645178/E','P000445','AMINATA','DIAGNE','F','1970-03-04','Abidjan',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775529650,0,0),(446,'660 501 /F','P000446','ARAME','DIOP','F','1983-09-12','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773064620,0,0),(447,'648326/C','P000447','CHEIKH MOUHAMADOU','MBAYE','H','1977-04-20','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',776495675,0,0),(448,'668 865 /A','P000448','DIARIYATOU','BALDE','F','1976-11-14','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775462587,0,0),(449,'100 107 030 /J','P000449','FATOU','CORREA','F','1970-06-06','St Louis',NULL,'1','-99',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772907855,0,0),(450,'660862 / D','P000450','KHOUDIA','FALL','F','1980-03-10','MÃ©khÃ©',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775574787,0,0),(451,'517650/J','P000451','MAMADOU ','SOW','H','1966-09-14','BarkÃ©dji',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775051601,0,0),(452,'653407/G','P000452','MAMADOU MOUSTAPHA ','SY','H','1968-05-01','Diourbel',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(453,' 507127 / F','P000453','OUMAR','DIA','H','1963-03-09','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',775361750,0,0),(454,'140107037 /G','P000454','ROKHIYATOU','FALL','F','1988-03-15','MÃ©dina Gounass',NULL,'1','-99',16,'','','0000-00-00',NULL,2,'MariÃ©( e )',771015760,0,0),(455,'140107015/G','P000455','SAYDINA','FALL','H','1975-04-08','Pire Goureuye',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779600540,0,0),(456,'511819/K','P000456','SOULEYMANE','DIALLO','H','1967-02-23','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',773612398,0,0),(457,'633577/A','P000457','AMADY','FALL','H','1978-08-08','Tokomac',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779462003,0,0),(458,'660423/E','P000458','DIARIOU             ','DIALLO','F','1975-11-01','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775505032,0,0),(459,'648754/D','P000459','DJIBRIL','THIAM','H','1970-06-18','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775351793,0,0),(460,'515100/H','P000460','MAPENDA','SYLLA','H','1964-02-05','Dakar',NULL,'2','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',775495869,0,0),(461,'160301037/B','P000461','NDEYE MBARKA','DIALLO','F','1982-03-13','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',779652289,0,0),(462,'646 255/F','P000462','OULEYMATOU','PENE','F','1969-03-01','Pikine',NULL,'1','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775195278,0,0),(463,'638435/E','P000463','OUSMANE','SARR','H','1982-03-26','Diouroup                          1450198200078',NULL,'2','2',16,'','','0000-00-00',NULL,0,'MariÃ©( e )',774334381,0,0),(464,'645 850/D','P000464','SAYDOU','GUISSE','H','1978-02-10','Diaba Decle',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779443596,0,0),(465,'512396/F','P000465','ALIOUNE BADARA','THIOUBE','F','1965-09-02','Pikine',NULL,'21','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(466,'672929/O','P000466','BABACAR','NGOM','H','1973-06-04','ThiarÃ©',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',765847419,0,0),(467,'516175/K','P000467','EL HADJI DJIME','SAMAKE','H','1964-05-24','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775471020,0,0),(468,'688367/B','P000468','GORA','LEYE','H','1985-12-23','Yeumbeul',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',706553799,0,0),(469,'609387/M','P000469','IBRAHIMA','DIOUF','H','1975-12-06','Diourbel',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775506389,0,0),(470,'644011/F','P000470','KHADY','SALL','F','1981-05-10','Sagatta',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',771656702,0,0),(471,'603582/J','P000471','KHALIFA ABABACAR','BA','H','1972-05-19','Mbour',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779738372,0,0),(472,'150503045/K','P000472','MAKHMOUD','DIEYE','H','1973-06-05','Guinaw-Rails',NULL,'1','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(473,'667402/A','P000473','MOUHAMADOU','SALL','H','1984-08-11','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',772365826,0,0),(474,'645171/L','P000474','PEDRO','DIENG','H','1965-02-10','Richard Toll',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',772733912,0,0),(475,'120901011/K','P000475','ABDOU LATIF','KANE','H','1979-10-06','NgÃ©oul Louta',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',779080373,0,0),(476,'661940/D','P000476','ABDOULAYE','NDIATH','H','1977-11-15','MÃ©dina',NULL,'3','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(477,'601059/C','P000477','AHMADOU','DIAGNE','H','1974-12-13','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775149473,0,0),(478,'657288/F','P000478','CHEIKH ABDOULAYE','DIAGNE','H','1983-05-07','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,1,'Celibataire',779394541,0,0),(479,'685615/G','P000479','DIAFFAR','DIABY','H','1981-02-05','Ndoffane',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779194948,0,0),(480,'638905/C','P000480','HAMED','FALL','H','1971-07-19','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776152626,0,0),(481,'519199/L','P000481','IBOU','SALL','H','1964-02-08','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',777745475,0,0),(482,'ABSENTE','P000482','KHADIDIATOU','SALL','F','1972-07-22','ThiÃšs',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',0,0,0),(483,'611031/H','P000483','LOUIS','GOMIS','H','1972-12-13','Colomba',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',775407487,0,0),(484,'602409/E','P000484','MAMADOU','NGOM','H','1973-04-12','Thiaroye Gare',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',777874998,0,0),(485,'516412/E','P000485','MAMADOU','DIAKHITE','H','1964-02-29','Dakar',NULL,'21','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779328733,0,0),(486,'601165/G','P000486','MOUHAMADOU','DIOP','H','1968-08-11','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775338624,0,0),(487,'680389/F','P000487','NDEYE YANDE','FAYE','F','1985-06-30','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',771862331,0,0),(488,'622133/E','P000488','NDIAGA','PENE','H','1969-12-04','Pikine',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',775445526,0,0),(489,'609491/F','P000489','OUSSEYNOU','NDIAYE','H','1966-06-03','Somb',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',0,0,0),(490,'684618/K','P000490','RAMATA','MBOW','F','1988-05-02','Seddo Abass',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',771509508,0,0),(491,'372776/H','P000491','SECKOU','TAMBA','H','1905-05-12','Bissine  Casamance',NULL,'19','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',775115599,0,0),(492,'602411/D','P000492','SOULEYMANE','SENE','H','1976-02-10','Yeumbeul',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775154034,0,0),(493,'389280/J','P000493','ABDOULAYE','DIAGNE','H','1959-05-03','Kaolack',NULL,'19','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',771675443,0,0),(494,'514091/P','P000494','AMADOU','NIANG','H','1968-05-31','Dakar',NULL,'1','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',706484579,0,0),(496,'667590/K','P000495','COURA','LO','F','1979-10-17','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775253010,0,0),(497,'120302065/G','P000496','FATOU ','SEYE','F','1976-08-04','ThiÃšs',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774266524,0,0),(498,'170107007/G','P000497','FATOU TALL','LO','F','1987-10-29','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772278716,0,0),(499,'673840/B','P000498','MAMA','THIOUNE','F','1968-06-28','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774520719,0,0),(500,'630990/C','P000499','MAMADOU','SY','H','1983-07-28','GuÃ©diawaye',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',779471052,0,0),(501,'641439/G','P000500','MAME   SEYNABOU','NDIAYE','F','1982-12-01','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',772042577,0,0),(502,'657257/D','P000501','MARIAMA','BA','F','1980-12-30','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',776779660,0,0),(503,'160107009/D','P000502','MARIAME','KANE','F','1971-10-10','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773258803,0,0),(504,'190107006/J','P000503','MOUHAMADOU','CISS','H','1985-04-19','Toglou SÃ?Â©rere','','3','2',9,'','','0000-00-00','0000-00-00',1,'MariÃ?Â©(e)',773722298,0,0),(505,'160107007/G','P000504','MOUHAMADOU MOUSTAPHA','DIOP','H','1984-10-26','GuÃ©diawaye',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',771047229,0,0),(506,'170107008/F','P000505','SERIGNE DJIGUEUL','NDOYE','H','1980-09-19','Rufisque',NULL,'1','1',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775069080,0,0),(507,'677349/B','P000506','ABOUBACRINE MOUSSA','DIA','H','1985-10-10','Pikine ',NULL,'2','1',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',776095156,0,0),(508,'600232/E','P000507','ADAMA','DOUCOURE','F','1971-09-26','Dakar',NULL,'2','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',708891764,0,0),(509,'631294/G','P000508','AISSATOU KANDJI','FALL','F','1974-08-31','Ziguinchor',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',771596862,0,0),(510,'607343/K','P000509','AMY COLLE','DIOP','F','1972-04-02','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775515873,0,0),(511,'150602089/K','P000510','ASTOU','SECK','F','1974-12-24','Pikine',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774188115,0,0),(512,'140107013/I','P000511','CHEIKH','DIOUF','H','1984-10-24','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,1,'Celibataire',775518244,0,0),(513,'517731/F','P000512','DAOUDA','PENE','H','1966-11-21','Dakar',NULL,'2','2',16,'','','0000-00-00',NULL,1,'MariÃ©( e )',776111328,0,0),(514,'626234/G','P000513','IBRAHIMA','SY','H','1966-11-27','Thies',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',776329255,0,0),(515,'668468/B','P000514','LENA','DIOP','F','1976-10-13','Yeumbeul',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775055035,0,0),(516,'632659/E','P000515','MAMADOU','WADE','H','1971-02-13','Dakar',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775677609,0,0),(517,'220152004/F','P000516','MAMADOU','GUEYE','H','1989-09-23','Dakar',NULL,'2','-99',9,'','','0000-00-00',NULL,1,'Celibataire',776559155,0,0),(518,'110107013/F','P000517','NDEYE AISSATOU','SIDIBE','F','1970-09-14','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',774697402,0,0),(519,'674434/B','P000518','NOGAYE','KEINDE','F','1975-08-21','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775149578,0,0),(520,'516396/J','P000519','OUSMANE','THIAM','H','1969-12-12','Diourbel','','1','2',-99,'','','0000-00-00','0000-00-00',1,'MariÃ?Â©(e)',775570789,0,0),(521,'632707/I','P000520','SAMBA DEMBA','SY','H','1974-04-22','Sinthiou Mogo',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',775193189,0,0),(522,'677445/B','P000521','AIDA','NIANG','F','1983-03-17','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',768536351,0,0),(523,'654753/Z','P000522','AMINATA','NIANG','F','1967-07-10','Dakar',NULL,'1','1',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',775016969,0,0),(524,'514655/B','P000523','BIRAME','FAYE','H','1968-09-17','Mbellacadiao',NULL,'4','2',-99,'','','0000-00-00',NULL,1,'MariÃ©( e )',775011711,0,0),(525,'190107008/H','P000524','EL HADJI ATOU','KHOULE','H','1987-05-23','Mboro',NULL,'2','-99',16,'','','0000-00-00',NULL,1,'Celibataire',773513791,0,0),(526,'632224/A','P000525','IBRAHIMA','GUEYE','H','1966-09-22','Saint-Louis',NULL,'8','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',771175318,0,0),(527,'684933/G','P000526','LOTY FALL','NDIAYE','F','1976-09-24','Pikine',NULL,'2','2',9,'','','0000-00-00',NULL,2,'MariÃ©( e )',773142324,0,0),(528,'669099/I','P000527','MAME ABDOU','NGOM','H','1983-12-31','Kaymor',NULL,'2','2',9,'','','0000-00-00',NULL,1,'MariÃ©( e )',774169014,0,0),(529,'661746/H','P000528','THIERNO ABDOUL HAMED','SOUARA','H','1972-12-12','Pikine','','2','2',9,'','','0000-00-00','0000-00-00',1,'MariÃ?Â©(e)',775619352,0,0);
/*!40000 ALTER TABLE `epeda_personnel_etablissement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluation_cycle`
--

DROP TABLE IF EXISTS `evaluation_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evaluation_cycle` (
  `id_evaluation_cycle` int(11) NOT NULL AUTO_INCREMENT,
  `code_cycle` int(11) NOT NULL,
  `code_type_evaluation` int(11) NOT NULL,
  `note_max` int(2) NOT NULL,
  `type_val` enum('D','C') CHARACTER SET latin1 NOT NULL,
  `nombre` enum('+1','1') CHARACTER SET latin1 NOT NULL,
  `etat_evaluationcycle` enum('1','-1') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_evaluation_cycle`),
  KEY `code_cycle` (`code_cycle`),
  KEY `code_type_evaluation` (`code_type_evaluation`),
  CONSTRAINT `evaluation_cycle_ibfk_1` FOREIGN KEY (`code_cycle`) REFERENCES `param_cycle` (`code_cycle`),
  CONSTRAINT `evaluation_cycle_ibfk_2` FOREIGN KEY (`code_type_evaluation`) REFERENCES `type_evaluation` (`code_type_evaluation`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluation_cycle`
--

LOCK TABLES `evaluation_cycle` WRITE;
/*!40000 ALTER TABLE `evaluation_cycle` DISABLE KEYS */;
INSERT INTO `evaluation_cycle` VALUES (1,2,2,20,'C','+1','1'),(2,2,3,60,'D','+1','1'),(3,3,2,20,'C','1','1'),(7,5,3,70,'D','1','1');
/*!40000 ALTER TABLE `evaluation_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interim`
--

DROP TABLE IF EXISTS `interim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interim` (
  `id_interim` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_sys_niits` bigint(20) NOT NULL,
  `debut_interim` datetime NOT NULL,
  `fin_interim` datetime NOT NULL,
  `id_type_profil` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_interim`),
  KEY `id_sys_type_profil` (`id_type_profil`),
  KEY `id_sys_niits` (`id_sys_niits`),
  CONSTRAINT `interim_ibfk_1` FOREIGN KEY (`id_type_profil`) REFERENCES `sys_type_profil` (`id_type_profil`),
  CONSTRAINT `interim_ibfk_2` FOREIGN KEY (`id_sys_niits`) REFERENCES `sys_niits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interim`
--

LOCK TABLES `interim` WRITE;
/*!40000 ALTER TABLE `interim` DISABLE KEYS */;
/*!40000 ALTER TABLE `interim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisation_patronale`
--

DROP TABLE IF EXISTS `organisation_patronale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisation_patronale` (
  `code_op` int(150) NOT NULL AUTO_INCREMENT,
  `nom_op` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `etat_op` char(2) DEFAULT '1',
  PRIMARY KEY (`code_op`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisation_patronale`
--

LOCK TABLES `organisation_patronale` WRITE;
/*!40000 ALTER TABLE `organisation_patronale` DISABLE KEYS */;
INSERT INTO `organisation_patronale` VALUES (1,'CNEFAAS',' testeur','1'),(2,'ONECS',NULL,'1'),(3,'UNEPLAS',NULL,'1'),(4,'Aucune',NULL,'-1');
/*!40000 ALTER TABLE `organisation_patronale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_annee_scolaire`
--

DROP TABLE IF EXISTS `param_annee_scolaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_annee_scolaire` (
  `code_annee` int(11) NOT NULL AUTO_INCREMENT,
  `annee_cours` int(4) NOT NULL,
  `libelle_annee` varchar(50) NOT NULL,
  `etat_en_cours` enum('1','-1') NOT NULL DEFAULT '-1',
  `statut_annee` enum('-1','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`code_annee`)
) ENGINE=InnoDB AUTO_INCREMENT=2019 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_annee_scolaire`
--

LOCK TABLES `param_annee_scolaire` WRITE;
/*!40000 ALTER TABLE `param_annee_scolaire` DISABLE KEYS */;
INSERT INTO `param_annee_scolaire` VALUES (2016,2016,'2016-2017','-1','1'),(2017,2017,'2017-2018','1','1'),(2018,2018,'2018-2019','-1','-1');
/*!40000 ALTER TABLE `param_annee_scolaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_cycle`
--

DROP TABLE IF EXISTS `param_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_cycle` (
  `code_cycle` int(100) NOT NULL AUTO_INCREMENT,
  `nom_cycle` varchar(255) NOT NULL,
  `statut_cycle` enum('-1','1') NOT NULL DEFAULT '1',
  `description` varchar(255) DEFAULT NULL,
  `bg` varchar(255) DEFAULT NULL,
  `image_cycle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_cycle`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_cycle`
--

LOCK TABLES `param_cycle` WRITE;
/*!40000 ALTER TABLE `param_cycle` DISABLE KEYS */;
INSERT INTO `param_cycle` VALUES (1,'Prescolaire','1','Plus petit cycle','bg-info','ion-university'),(2,'Elementaire','1','le cycle allant du CI au CM2','bg-purple','ion-university'),(3,'Moyen','1',NULL,'bg-success','ion-university'),(4,'Secondaire','1',NULL,'bg-primary','ion-university'),(5,'Moyen secondaire','1',NULL,'bg-warning','ion-university'),(6,'Daara','1','enseignement coranique',NULL,'ion_university');
/*!40000 ALTER TABLE `param_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_cycle_serie`
--

DROP TABLE IF EXISTS `param_cycle_serie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_cycle_serie` (
  `id_cycle_serie` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code_type_serie` int(11) NOT NULL,
  `code_cycle` int(100) NOT NULL,
  PRIMARY KEY (`id_cycle_serie`),
  KEY `id_cycle_serie` (`id_cycle_serie`),
  KEY `id_cycle_serie_2` (`id_cycle_serie`),
  KEY `id_cycle_serie_3` (`id_cycle_serie`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_cycle_serie`
--

LOCK TABLES `param_cycle_serie` WRITE;
/*!40000 ALTER TABLE `param_cycle_serie` DISABLE KEYS */;
INSERT INTO `param_cycle_serie` VALUES (1,-99,1),(2,-99,2),(3,-99,3),(10,3,4),(13,4,5);
/*!40000 ALTER TABLE `param_cycle_serie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_cycle_structure`
--

DROP TABLE IF EXISTS `param_cycle_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_cycle_structure` (
  `id_cycle_str` int(11) NOT NULL AUTO_INCREMENT,
  `code_cycle` int(11) NOT NULL,
  `code_str` int(150) NOT NULL,
  `date_autoriser` int(4) NOT NULL,
  `date_reconnaissance` varchar(45) NOT NULL,
  `etat_reconner` int(11) NOT NULL,
  `etat_cyc` int(1) NOT NULL,
  PRIMARY KEY (`id_cycle_str`),
  KEY `code_cycle` (`code_cycle`),
  KEY `code_str` (`code_str`),
  CONSTRAINT `param_cycle_structure_ibfk_1` FOREIGN KEY (`code_cycle`) REFERENCES `param_cycle` (`code_cycle`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_cycle_structure`
--

LOCK TABLES `param_cycle_structure` WRITE;
/*!40000 ALTER TABLE `param_cycle_structure` DISABLE KEYS */;
INSERT INTO `param_cycle_structure` VALUES (1,2,1,0,'',0,0),(2,3,2,0,'',0,0);
/*!40000 ALTER TABLE `param_cycle_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_discipline`
--

DROP TABLE IF EXISTS `param_discipline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_discipline` (
  `id_discipline` int(11) NOT NULL AUTO_INCREMENT,
  `code_discipline` varchar(250) NOT NULL,
  `libelle_discipline` varchar(250) NOT NULL,
  `couleur_discipline` varchar(15) DEFAULT NULL,
  `composition_matiere` enum('1','-1') NOT NULL DEFAULT '-1' COMMENT 'Si 1 La discipline est composé en matiére, sinon -1 pas de decomposition',
  `etat_discipline` enum('1','-1') NOT NULL DEFAULT '1',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_discipline`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_discipline`
--

LOCK TABLES `param_discipline` WRITE;
/*!40000 ALTER TABLE `param_discipline` DISABLE KEYS */;
INSERT INTO `param_discipline` VALUES (1,'FR','Francais','','-1','1',' '),(2,'MA','Mathematique','','-1','1',' '),(3,'AN','Anglais','','-1','1',' '),(4,'HG','Histoire-Geographie','','-1','1',NULL),(5,'SVT','SVT','','-1','1',NULL),(6,'AR','Arabe','','-1','1',' '),(7,'SCP','SCIENCE PHYSIQUE','','-1','1',' '),(8,'ES','Espagnol','','-1','1',NULL),(9,'EP','EPS','','-1','1',NULL),(10,'PH','Philo','','-1','1',NULL),(11,'AL','Allemand','','-1','1',NULL),(12,'PO','Portugais','','-1','1',NULL),(13,'DE','Dessin','','-1','1',NULL),(14,'EC','Economie','','-1','1',NULL),(15,'EDR','Education religieuse','','-1','1',NULL),(17,'IN','INFORMATIQUE','','-1','1',NULL),(18,'CO','COMPTABILITE','','-1','1',NULL),(19,'ECF','Economie familliale','','-1','1',NULL),(20,'IC','Instruction civique','','-1','1',NULL),(21,'TU','Turc','','-1','1',NULL),(22,'IT','Italien','','-1','1',NULL),(23,'RU','Russe','','-1','1',NULL),(24,'DR','DROIT','','-1','1',NULL),(25,'MO','MORALE','','-1','1',NULL),(27,'ECOSO','Economie  sociale','','-1','1',NULL),(29,'COR','Communication Orale','','-1','1',NULL),(30,'PRE','Production d\'Ã©crits','','-1','1',NULL),(34,'CONJ','Conjugaison','','-1','1',NULL),(35,'ORT','Orthographe','','-1','1',NULL),(36,'GRAM','Grammaire','','-1','1',NULL),(37,'LEC','Lecture','','-1','1',NULL),(38,'ECRI','Ecriture','','-1','1',NULL),(39,'GRAP','Graphisme','','-1','1',NULL),(40,'ACTN','ActivitÃ© NumÃ©rique','','-1','1',NULL),(41,'ACTG','ActivitÃ© Geometrique','','-1','1',NULL),(42,'ACTM','ActivitÃ© de Mesure','','-1','1',NULL),(43,'RSP','RÃ©solution de probleme','','-1','1',NULL),(44,'HIS','Histoire','','-1','1',NULL),(45,'GEO','Geographie','','-1','1',NULL),(46,'IST','Initiation Scientifique et technologique','','-1','1',NULL),(47,'VIVE','Vivre ensemble','','-1','1',NULL),(48,'VIVM','Vivre dans son milieu','','-1','1',NULL),(49,'ARTSC','Arts Scenique','','-1','1',NULL),(50,'ARTSP','Arts Plastique','','-1','1',NULL),(51,'EDM','Education Musicale','','-1','1',NULL),(52,'POE','poeme','','-1','1',NULL),(53,'COM','comptine','','-1','1',NULL),(54,'tt','test','vert','1','1','oseyh b'),(55,'tt','test','vert','1','1','oseyh b'),(56,'tt','test','vert','1','1','oseyh b'),(57,'tt','test','vert','1','1','oseyh b'),(58,'tt','test','vert','1','1','uthzibv uk '),(59,'tt','test','vert','1','1','uthzibv uk '),(60,'tt','test','vert','1','1','uthzibv uk '),(61,'tt','test','vert','1','1','?eordtbx '),(62,'tt','test','vert','1','1','?eordtbx '),(63,'tt7','test','orange','1','1','uybgfdsq  '),(64,'tt7','test','orange','1','1','vu   '),(65,'discipline ','dc','orange','-1','1','cqs  '),(66,'discipline ','dc','orange','-1','1','cqs  '),(67,'discipline ','dc','orange','-1','-1','cqs  '),(68,'tt','test','vert','1','-1',' yku');
/*!40000 ALTER TABLE `param_discipline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_discipline_matiere`
--

DROP TABLE IF EXISTS `param_discipline_matiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_discipline_matiere` (
  `id_matieredisc` int(11) NOT NULL AUTO_INCREMENT,
  `id_discipline` int(11) NOT NULL,
  `id_matiere` int(11) NOT NULL,
  PRIMARY KEY (`id_matieredisc`),
  KEY `id_materiel` (`id_matiere`),
  KEY `code_specialite` (`id_discipline`),
  CONSTRAINT `param_discipline_matiere_ibfk_1` FOREIGN KEY (`id_discipline`) REFERENCES `param_discipline` (`id_discipline`),
  CONSTRAINT `param_discipline_matiere_ibfk_2` FOREIGN KEY (`id_matiere`) REFERENCES `param_matiere` (`id_matiere`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_discipline_matiere`
--

LOCK TABLES `param_discipline_matiere` WRITE;
/*!40000 ALTER TABLE `param_discipline_matiere` DISABLE KEYS */;
INSERT INTO `param_discipline_matiere` VALUES (1,1,1),(2,34,1),(3,35,1),(4,36,1),(5,1,2),(6,3,2),(8,1,3),(9,3,3),(11,1,4),(12,3,4),(14,1,5),(15,3,5),(17,2,7),(18,5,7),(20,2,8),(21,5,8),(23,2,9),(24,5,9),(26,2,10),(27,5,10),(28,18,10),(29,54,10),(30,55,10),(31,61,10),(32,6,1),(33,6,1),(34,61,1),(35,61,1),(36,61,1),(37,61,1),(38,61,1),(39,61,1),(40,61,1),(41,61,1),(42,61,1),(43,61,1),(44,61,1);
/*!40000 ALTER TABLE `param_discipline_matiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_fonction`
--

DROP TABLE IF EXISTS `param_fonction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_fonction` (
  `code_fonction` int(11) NOT NULL AUTO_INCREMENT,
  `nom_fonction` varchar(255) NOT NULL,
  `etat_fonction` char(2) NOT NULL DEFAULT '1',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_fonction`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_fonction`
--

LOCK TABLES `param_fonction` WRITE;
/*!40000 ALTER TABLE `param_fonction` DISABLE KEYS */;
INSERT INTO `param_fonction` VALUES (1,'maty','-1',NULL),(3,'matar','1','testeur'),(4,'xxxx','-1',NULL),(5,'test','-1',NULL),(6,'directeur','1','chef de la structure'),(7,'test','-1','tester'),(8,'test','-1','tester'),(9,'test','-1','testeur'),(10,'test','1','tester'),(11,'test','1','test'),(12,'test','-1','tettsbv yu ');
/*!40000 ALTER TABLE `param_fonction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_grade`
--

DROP TABLE IF EXISTS `param_grade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_grade` (
  `code_grade` int(100) NOT NULL AUTO_INCREMENT,
  `nom_grade` varchar(255) NOT NULL,
  `etat_grade` enum('1','-1') DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_grade`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_grade`
--

LOCK TABLES `param_grade` WRITE;
/*!40000 ALTER TABLE `param_grade` DISABLE KEYS */;
INSERT INTO `param_grade` VALUES (-99,'Indetermine','1',' hfezaibfi '),(1,'1/1','1','test'),(2,'1/2','1',' '),(3,'1/3','1',NULL),(4,'2/3','1',NULL),(5,'2/2','1',NULL),(6,'2/3','1',NULL),(8,'3/3','1',NULL),(9,'3/2','1',NULL),(10,'4/2','1',NULL),(11,'4/3','1',NULL),(12,'P1/3','1',NULL),(13,'P2','1',NULL),(14,'P2/1','1',NULL),(15,'P2/2','1',NULL),(16,'P2/7','1',NULL),(17,'P3','1',NULL),(18,'P4','1',NULL),(19,'PCE','1',NULL),(20,'ST','1',NULL),(21,'C.EX','1',NULL),(22,'3/3','1',NULL),(23,'IPCE','1',NULL),(24,'IP2','1',NULL),(25,'IP3','1',NULL),(26,'1er Catégorie','1',NULL),(27,'2eme Catégorie','1',NULL),(28,'3eme Catégorie','1',NULL),(29,'4eme Catégorie','1',NULL),(30,'5eme Catégorie','1',NULL),(31,'6eme Catégorie','1',NULL),(32,'7eme Catégorie','1',NULL),(33,'8eme Catégorie','1',NULL),(34,'9eme Catégorie','1',NULL),(35,'10eme Catégorie','1',NULL),(36,'11eme Catégorie','1',NULL),(37,'12eme Catégorie','1',NULL),(38,'3/2','1',NULL),(39,'4/2','1',NULL),(40,'4/3','1',NULL),(41,'','',NULL),(42,'','',NULL),(43,'','',NULL),(44,'','',NULL),(45,'','',NULL),(46,'','',NULL),(47,'','',NULL),(48,'','',NULL),(49,'','',NULL),(50,'','',NULL),(51,'','-1',NULL);
/*!40000 ALTER TABLE `param_grade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_matiere`
--

DROP TABLE IF EXISTS `param_matiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_matiere` (
  `id_matiere` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_matiere` varchar(250) DEFAULT NULL,
  `etat_matiere` enum('1','-1') DEFAULT NULL,
  `description_matiere` varchar(255) NOT NULL,
  PRIMARY KEY (`id_matiere`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_matiere`
--

LOCK TABLES `param_matiere` WRITE;
/*!40000 ALTER TABLE `param_matiere` DISABLE KEYS */;
INSERT INTO `param_matiere` VALUES (1,'Dictée','1',' exercice d\'orthographe et de grammaire'),(2,'Redaction','1',' matiere d\'expression ecrite'),(3,'Redaction','1',' matiere d\'expression ecrite'),(4,'Redaction','1',' matiere d\'expression ecrite'),(5,'Redaction','1',' matiere d\'expression ecrite'),(6,'Texte Suivi de Questions','1',' matiere d\'expression ecrite'),(7,'Algebre','1',' discipline mathematique'),(8,'Algebre','1',' discipline mathematique'),(9,'Algebre','1',' discipline mathematique'),(10,'Algebre','1',' discipline mathematique');
/*!40000 ALTER TABLE `param_matiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_section`
--

DROP TABLE IF EXISTS `param_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_section` (
  `code_section` int(100) NOT NULL AUTO_INCREMENT,
  `codesection` varchar(10) DEFAULT NULL,
  `code_cycle` int(100) NOT NULL,
  `libelle_section` varchar(255) NOT NULL,
  `effectif_enseignant` enum('0','1') NOT NULL DEFAULT '0',
  `ordre_section` int(2) NOT NULL,
  `etat_section` char(2) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_section`),
  KEY `code_cycle` (`code_cycle`),
  CONSTRAINT `param_section_ibfk_1` FOREIGN KEY (`code_cycle`) REFERENCES `param_cycle` (`code_cycle`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_section`
--

LOCK TABLES `param_section` WRITE;
/*!40000 ALTER TABLE `param_section` DISABLE KEYS */;
INSERT INTO `param_section` VALUES (1,'PS',1,'Petite section','1',1,'1',NULL),(2,'MS',1,'Moyenne Section','1',2,'1',NULL),(3,'GS',1,'Grande section','1',3,'1',NULL),(4,'CI',2,'CI','1',1,'1',NULL),(5,'CP',2,'CP','1',2,'1',NULL),(6,'CE1',2,'CE1','1',3,'1',NULL),(7,'CE2',2,'CE2','1',4,'1',NULL),(8,'CM1',2,'CM1','1',5,'1',NULL),(9,'CM2',2,'CM2','1',6,'1',NULL),(10,'6e',3,'6e','1',1,'1',NULL),(11,'5e',3,'5e','1',2,'1',NULL),(12,'4e',3,'4e','1',3,'1',NULL),(13,'3e',3,'3e','1',4,'1',NULL),(14,'2nde',4,'2e','1',1,'1',NULL),(15,'1er',4,'1er','1',2,'1',NULL),(16,'Tle',4,'Terminale','1',3,'1',NULL),(17,'tt',1,'test','1',2,'-1','tettui i uqi '),(18,'tt7',2,'test4','1',7,'-1','-rfgvb uiu bo');
/*!40000 ALTER TABLE `param_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_specialite`
--

DROP TABLE IF EXISTS `param_specialite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_specialite` (
  `code_specialite` int(11) NOT NULL AUTO_INCREMENT,
  `nom_specialite` varchar(255) NOT NULL,
  `code_spec` varchar(10) NOT NULL,
  `etat_specialite` char(2) NOT NULL DEFAULT '1',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_specialite`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_specialite`
--

LOCK TABLES `param_specialite` WRITE;
/*!40000 ALTER TABLE `param_specialite` DISABLE KEYS */;
INSERT INTO `param_specialite` VALUES (1,'pays bas','PB','1',' testeur'),(2,'Mathematique','MT','1',NULL),(3,'lettre moderne','LM','1',' tester'),(4,'anglais','ANG','1',' specialiste de la matiere d\'anglais'),(5,'allemand','ALL','1',NULL),(6,'français','FR','1',NULL),(31,'test','tr','-1','tester l\'ajout avec MAJ'),(32,'test','tr','-1','tester l\'ajout avec MAJ'),(33,'test','tr','-1','tester l\'ajout avec MAJ'),(34,'test','tr','-1','tester l\'ajout avec MAJ'),(35,'test','tr','-1','tester l\'ajout avec MAJ'),(36,'test','tr','-1','tester l\'ajout avec MAJ'),(37,'test','tr','-1','tester l\'ajout avec MAJ'),(38,'test','tr','-1','tester l\'ajout avec MAJ'),(39,'test','tr','-1','tester l\'ajout avec MAJ'),(40,'test','tr','-1','b<u  ubyzqu huqbv '),(41,'test','tr','-1','ouhi iuqb pb iéaçr v'),(42,'testera','ttr','-1',' test de fonctionnement ');
/*!40000 ALTER TABLE `param_specialite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_specialite_cycle`
--

DROP TABLE IF EXISTS `param_specialite_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_specialite_cycle` (
  `code_spec_cycle` int(11) NOT NULL AUTO_INCREMENT,
  `code_cycle` int(11) NOT NULL,
  `code_specialite` int(11) NOT NULL,
  `etat_specialite_cycle` enum('1','-1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`code_spec_cycle`),
  UNIQUE KEY `code_cycle_2` (`code_cycle`,`code_specialite`),
  UNIQUE KEY `code_cycle_3` (`code_cycle`,`code_specialite`),
  KEY `code_cycle` (`code_cycle`),
  KEY `code_specialite` (`code_specialite`),
  CONSTRAINT `param_specialite_cycle_ibfk_1` FOREIGN KEY (`code_cycle`) REFERENCES `param_cycle` (`code_cycle`),
  CONSTRAINT `param_specialite_cycle_ibfk_2` FOREIGN KEY (`code_specialite`) REFERENCES `param_specialite` (`code_specialite`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_specialite_cycle`
--

LOCK TABLES `param_specialite_cycle` WRITE;
/*!40000 ALTER TABLE `param_specialite_cycle` DISABLE KEYS */;
INSERT INTO `param_specialite_cycle` VALUES (1,1,4,'1'),(2,3,4,'1'),(3,1,1,'1'),(4,2,1,'-1'),(5,3,1,'1');
/*!40000 ALTER TABLE `param_specialite_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_specialite_discipline`
--

DROP TABLE IF EXISTS `param_specialite_discipline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_specialite_discipline` (
  `code_spm` int(11) NOT NULL AUTO_INCREMENT,
  `code_specialite` int(11) NOT NULL,
  `id_discipline` int(11) DEFAULT NULL,
  `etat_specialite_discipline` enum('1','-1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`code_spm`),
  KEY `code_specialite` (`code_specialite`),
  KEY `id_discipline` (`id_discipline`),
  CONSTRAINT `param_specialite_discipline_ibfk_1` FOREIGN KEY (`code_specialite`) REFERENCES `param_specialite` (`code_specialite`),
  CONSTRAINT `param_specialite_discipline_ibfk_2` FOREIGN KEY (`id_discipline`) REFERENCES `param_discipline` (`id_discipline`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_specialite_discipline`
--

LOCK TABLES `param_specialite_discipline` WRITE;
/*!40000 ALTER TABLE `param_specialite_discipline` DISABLE KEYS */;
INSERT INTO `param_specialite_discipline` VALUES (1,40,47,'1'),(2,41,20,'1'),(3,41,41,'1'),(4,41,44,'1'),(5,41,48,'1'),(6,41,52,'1'),(7,41,58,'1'),(8,41,63,'1'),(9,41,68,'1'),(10,42,2,'1'),(11,42,30,'1'),(12,42,35,'1'),(13,42,38,'1'),(14,42,47,'1'),(15,3,1,'1'),(16,3,2,'1'),(17,4,1,'1'),(18,4,4,'1'),(19,4,5,'1'),(20,1,1,'-1'),(21,1,2,'-1'),(22,1,4,'-1'),(23,1,5,'-1'),(24,1,20,'-1'),(25,1,30,'-1'),(26,1,35,'-1'),(27,1,38,'1'),(28,1,41,'-1'),(29,1,44,'-1'),(30,1,47,'-1'),(31,1,48,'-1'),(32,1,52,'-1'),(33,1,54,'-1'),(34,1,55,'-1'),(35,1,56,'-1'),(36,1,57,'-1'),(37,1,58,'-1'),(38,1,59,'-1'),(39,1,60,'-1'),(40,1,61,'-1'),(41,1,62,'-1'),(42,1,63,'-1'),(43,1,64,'-1'),(44,1,11,'-1'),(45,1,13,'-1'),(46,1,19,'-1'),(47,1,24,'-1');
/*!40000 ALTER TABLE `param_specialite_discipline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_statut_eleve`
--

DROP TABLE IF EXISTS `param_statut_eleve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_statut_eleve` (
  `code_statut` int(2) NOT NULL AUTO_INCREMENT,
  `libelle_statut` varchar(250) NOT NULL,
  `mode_statut` enum('0','1') NOT NULL,
  PRIMARY KEY (`code_statut`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_statut_eleve`
--

LOCK TABLES `param_statut_eleve` WRITE;
/*!40000 ALTER TABLE `param_statut_eleve` DISABLE KEYS */;
INSERT INTO `param_statut_eleve` VALUES (10,'Oriente','0'),(20,'Transfert','0'),(30,'Examen','0'),(40,'Exclut','1');
/*!40000 ALTER TABLE `param_statut_eleve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_syndicat`
--

DROP TABLE IF EXISTS `param_syndicat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_syndicat` (
  `id_syndicat` int(11) NOT NULL AUTO_INCREMENT,
  `code_syndicat` varchar(50) NOT NULL,
  `nom_syndicat` varchar(250) NOT NULL,
  `nom_sg` varchar(250) NOT NULL,
  `tel_sg` varchar(50) NOT NULL,
  `email_sg` text NOT NULL,
  PRIMARY KEY (`id_syndicat`),
  UNIQUE KEY `code_syndicat` (`code_syndicat`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_syndicat`
--

LOCK TABLES `param_syndicat` WRITE;
/*!40000 ALTER TABLE `param_syndicat` DISABLE KEYS */;
INSERT INTO `param_syndicat` VALUES (1,'SUDES','Syndicat Unique et Démocratique des Enseignants du Sénégal  ','Amadou          DIAOUNE   ','775344723','sudes_sénégal@yahoo.fr'),(2,'SNEEL',' Syndicat National de l’Enseignement Elémentaire   \n   ','Cheikh Alassane  SÈNE','776420215','sneel@sentoo.sn'),(3,'UDEN','Union Démocratique des Enseignants du Sénégal     \n','Awa         WADE     ','776525575','awade7@orange.sn\nuden@orange.sn\nuden@sentoo.sn'),(4,'SYPROS','Syndicat des Professeurs du Sénégal   \n','Marieme   SAKHO DANSOKHO','776396401','sypros@orange.sn         '),(5,'FIDUEF',' Fédération Indépendante Démocratique Unitaire de l’Education et de la Formation  ','Daouda             GUEYE','774320366','dmgueye@]yahoo.fr'),(6,'SELS/ORIGINEL','Syndicat des Enseignants Libres du Sénégal/Originel    \n			','Oumar Waly ZOUMAROU','114475680','selsoriginel.orange.sn\nozoumarou@yahoo.fr'),(7,'SELS/A','Syndicat des Enseignants Libres du Sénégal/ Authentique','Abdou                   FATY','775013265','selsauthentic@yahoo.fr'),(8,'ADES','Alliance Démocratique des Enseignants du Sénégal ','Ibra Diouf    NIOKHOBAYE','','diabladiouf56@yahoo.fr'),(9,'SELS',' Syndicat des Enseignants Libres du Sénégal    \n','Souleymane         DIALLO','775625848','falldoouf@live.fr\nlucienmedanghenri@yahoo.fr'),(10,'UES','Union des Enseignants du Sénégal \n','Gougna                NIANG','776549760','uniondesenseignants_sn@yahoo.fr'),(11,'SNELAS/CNTS',' Syndicat National des Enseignants en Langue Arabe du Sénégal/CNTS   \n','Doudou                 GAYE','77 24062 2\n775578615','niang.mbake@yahoo.fr      '),(12,'SAEMSS-CUSEMS','Syndicat Autonome des Enseignants du Moyen et du Secondaire du Sénégal/CUSEMS   \n','Mamadou Lamine  DIANTE','776559419','saemss@yahoo.fr'),(13,'SDEA','Syndicat Démocratique des Enséignants en Langue Arabe    ','Abdou Karim       SAMB','775432360','abdousdea@yahoo.fr'),(14,'SNEEL/CNTS','Syndicat National de l’Enseignement Elémentaire/CNTS','Fatou Bintou         YAFFA','775333463','mamadousy61@hotmail.fr'),(15,'SNEEL/FC',' Syndicat National de l’Enseignement Elémentaire/FC','Bakhaw              DIONGUE','776415167',''),(16,'OIS','Organisation des Instituteurs du Sénégal           ','Hamidou              BA','779398277','atab.goudiaby@yahoo.fr'),(17,'CUSEMS',' Cadre Unitaire Syndical des Enseignants du Moyen et du Secondaire  ','Abdoulaye            NDOYE','775161392','cusems@yahoo.fr'),(18,'OIS/RD','Organisation des Instituteurs du Sénégal :RD                ','Magatte               DIOP','776587823','oisrd@yahoo.fr'),(19,'SYDELS','Syndicat Démocratique des Enseignants Libres du Sénégal   ','Mamadou             KANE','776504049','kanepape19@yahoo.fr'),(20,'ADEPT','Alliance pour la Défense de l’Ecole Publique et des Travailleurs','Ndiaga                 SYLLA','776522232','diekamsylla@yahoo.fr'),(21,'SYDELS/ORIGINEL','Syndicat Démocratique des Enseignants Libres du Sénégal/Originel   \n ','Alioune                GUEYE','772067714','sydelsorigine@yahoo.fr'),(22,'CDEFS','           ','NDOUR','776402696',''),(23,'SEPPI','Syndicat de l’Elémentaire, du Préscolaire et des Professions Intégrées    ','Mamadou          TAMBA','772406272  703301759','tambasilab@yahoo.fr seppisen@yahoo.fr'),(24,'SCEMES',' Syndicats des Corps Emergents de l’Enseignement du Sénégal','Gora                  MBAYE','776506958','rahimdiouf@hotmail.fr)'),(25,'ODELS','Organisation Démocratique des enseignants Libres du Sénégal','Malang              DABO','774335388','odels08@yahoo.fr  '),(26,'SEPE','Syndicat des Enseignants du Préscolaire et de l’Elémentaire  ','Hamath Suzane Camara','772348049','hamathsuzane@yahoo.fr\nsepe.sn@hotmail.fr'),(27,'ODES','Organisation Démocratique des Enseignants du Sénégal ','Ibrahima Wane','774022275','ibrahimademba01@hotmail.com     '),(28,'SIDEES','Syndicat \n','Oumar Seck','774084313\n779759737','sidees@hotmail.fr\nseckito@live.fr		'),(29,'REEL','Rassemblement des Educateurs et Enseignants Libres              ','Moussa  DIOP','775720232',''),(30,'SNELAS/FC','Syndicat National des Enseignants en Langue Arabe du Sénégal/FC','SEGNANE','706070526','segnane69@yahoo.fr'),(31,'SIENS','Syndicat des Inspectrices et Inspecteurs de l’Education Nationale du Sénégal','Samba      Diakhaté','776553647','Siens.educsenegalàyahoo.com'),(32,'SNECS','Syndicat National des Enseignants Catholiques du Sénégal','François Xavier SARR','776460176','fxsarr@yahoo.fr'),(33,'SEP','Syndicat des Enseignants Progressistes','','',''),(34,'SYCCEN','Syndicat des Corps de Contrôle de l’Education nationale','','',''),(35,'UDEPL','Union Démocratique des Enseignnants du Privé Laic','Moustapha KASSE','775344886','Kassetapha53@yahoo.fr'),(36,'STESU/CNTS','Syndicat des Travailleurs des Etablissements Scolaires et Universitaires/CNTS','','',''),(37,'SAES','Syndicat Autonome des Enseignants du Supérieur','Babacar Sadikh NDIAYE','776444804',''),(38,'OPES','Organisation pour la Promotion des Enseignants du Sénégal','Seydou Abou SY','776553727','seydouabou@yahoo.fr'),(40,'SYNAPEFS','SYNAPEFS','Aldiouma   SAGNA','772143652','sagnaaldiouma@yahoo.fr'),(41,'SNEF','SNEF','Abdoulaye   CISSE','777912073','abalaybamba@yahoo.fr'),(42,'UDEN/R','UDEN/R','Ardo FALL','','');
/*!40000 ALTER TABLE `param_syndicat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_type_personne_cycle`
--

DROP TABLE IF EXISTS `param_type_personne_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_type_personne_cycle` (
  `code_type_enseignant` int(11) NOT NULL,
  `code_cycle` int(11) NOT NULL,
  PRIMARY KEY (`code_type_enseignant`,`code_cycle`),
  KEY `code_type_enseignant` (`code_type_enseignant`,`code_cycle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_type_personne_cycle`
--

LOCK TABLES `param_type_personne_cycle` WRITE;
/*!40000 ALTER TABLE `param_type_personne_cycle` DISABLE KEYS */;
INSERT INTO `param_type_personne_cycle` VALUES (1,1),(1,2),(2,1),(2,2),(3,3),(3,4),(4,4),(4,5),(5,4),(5,5),(6,3),(6,4),(6,5),(7,3),(8,4),(8,5);
/*!40000 ALTER TABLE `param_type_personne_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_type_personnel`
--

DROP TABLE IF EXISTS `param_type_personnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_type_personnel` (
  `id_type_personnel` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelle_type_personnel` varchar(250) DEFAULT NULL,
  `etat_type_personnel` enum('-1','1') DEFAULT NULL,
  PRIMARY KEY (`id_type_personnel`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_type_personnel`
--

LOCK TABLES `param_type_personnel` WRITE;
/*!40000 ALTER TABLE `param_type_personnel` DISABLE KEYS */;
INSERT INTO `param_type_personnel` VALUES (1,'Professeur','1'),(2,'Maitre','1'),(3,'Directeur','1');
/*!40000 ALTER TABLE `param_type_personnel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `param_type_serie`
--

DROP TABLE IF EXISTS `param_type_serie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_type_serie` (
  `code_type_serie` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_serie` varchar(250) NOT NULL,
  `etat_serie` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`code_type_serie`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `param_type_serie`
--

LOCK TABLES `param_type_serie` WRITE;
/*!40000 ALTER TABLE `param_type_serie` DISABLE KEYS */;
INSERT INTO `param_type_serie` VALUES (1,'Scientifique',1);
/*!40000 ALTER TABLE `param_type_serie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pays`
--

DROP TABLE IF EXISTS `pays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pays` (
  `id_pays` int(11) NOT NULL AUTO_INCREMENT,
  `code_pays` varchar(3) NOT NULL,
  `fr` varchar(200) NOT NULL,
  `en` varchar(200) NOT NULL,
  `etat_pays` enum('-1','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_pays`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pays`
--

LOCK TABLES `pays` WRITE;
/*!40000 ALTER TABLE `pays` DISABLE KEYS */;
INSERT INTO `pays` VALUES (1,'AF','Afghanistan','Afghanistan','1'),(2,'ZA','Afrique du Sud','South Africa','1'),(3,'AL','Albanie','Albania','1'),(4,'DZ','Algérie','Algeria','1'),(5,'DE','Allemagne','Germany','1'),(6,'AD','Andorre','Andorra','1'),(7,'AO','Angola','Angola','1'),(8,'AI','Anguilla','Anguilla','1'),(9,'AQ','Antarctique','Antarctica','1'),(10,'AG','Antigua-et-Barbuda','Antigua & Barbuda','1'),(11,'AN','Antilles néerlandaises','Netherlands Antilles','1'),(12,'SA','Arabie saoudite','Saudi Arabia','1'),(13,'AR','Argentine','Argentina','1'),(14,'AM','Arménie','Armenia','1'),(15,'AW','Aruba','Aruba','1'),(16,'AU','Australie','Australia','1'),(17,'AT','Autriche','Austria','1'),(18,'AZ','Azerbaïdjan','Azerbaijan','1'),(19,'BJ','Bénin','Benin','1'),(20,'BS','Bahamas','Bahamas, The','1'),(21,'BH','Bahreïn','Bahrain','1'),(22,'BD','Bangladesh','Bangladesh','1'),(23,'BB','Barbade','Barbados','1'),(24,'PW','Belau','Palau','1'),(25,'BE','Belgique','Belgium','1'),(26,'BZ','Belize','Belize','1'),(27,'BM','Bermudes','Bermuda','1'),(28,'BT','Bhoutan','Bhutan','1'),(29,'BY','Biélorussie','Belarus','1'),(30,'MM','Birmanie','Myanmar (ex-Burma)','1'),(31,'BO','Bolivie','Bolivia','1'),(32,'BA','Bosnie-Herzégovine','Bosnia and Herzegovina','1'),(33,'BW','Botswana','Botswana','1'),(34,'BR','Brésil','Brazil','1'),(35,'BN','Brunei','Brunei Darussalam','1'),(36,'BG','Bulgarie','Bulgaria','1'),(37,'BF','Burkina Faso','Burkina Faso','1'),(38,'BI','Burundi','Burundi','1'),(39,'CI','Côte d\'Ivoire','Ivory Coast (see Cote d\'Ivoire)','1'),(40,'KH','Cambodge','Cambodia','1'),(41,'CM','Cameroun','Cameroon','1'),(42,'CA','Canada','Canada','1'),(43,'CV','Cap-Vert','Cape Verde','1'),(44,'CL','Chili','Chile','1'),(45,'CN','Chine','China','1'),(46,'CY','Chypre','Cyprus','1'),(47,'CO','Colombie','Colombia','1'),(48,'KM','Comores','Comoros','1'),(49,'CG','Congo','Congo','1'),(50,'KP','Corée du Nord','Korea, Demo. People\'s Rep. of','1'),(51,'KR','Corée du Sud','Korea, (South) Republic of','1'),(52,'CR','Costa Rica','Costa Rica','1'),(53,'HR','Croatie','Croatia','1'),(54,'CU','Cuba','Cuba','1'),(55,'DK','Danemark','Denmark','1'),(56,'DJ','Djibouti','Djibouti','1'),(57,'DM','Dominique','Dominica','1'),(58,'EG','Égypte','Egypt','1'),(59,'AE','Émirats arabes unis','United Arab Emirates','1'),(60,'EC','Équateur','Ecuador','1'),(61,'ER','Érythrée','Eritrea','1'),(62,'ES','Espagne','Spain','1'),(63,'EE','Estonie','Estonia','1'),(64,'US','États-Unis','United States','1'),(65,'ET','Éthiopie','Ethiopia','1'),(66,'FI','Finlande','Finland','1'),(67,'FR','France','France','1'),(68,'GE','Géorgie','Georgia','1'),(69,'GA','Gabon','Gabon','1'),(70,'GM','Gambie','Gambia, the','1'),(71,'GH','Ghana','Ghana','1'),(72,'GI','Gibraltar','Gibraltar','1'),(73,'GR','Grèce','Greece','1'),(74,'GD','Grenade','Grenada','1'),(75,'GL','Groenland','Greenland','1'),(76,'GP','Guadeloupe','Guinea, Equatorial','1'),(77,'GU','Guam','Guam','1'),(78,'GT','Guatemala','Guatemala','1'),(79,'GN','Guinée','Guinea','1'),(80,'GQ','Guinée équatoriale','Equatorial Guinea','1'),(81,'GW','Guinée-Bissao','Guinea-Bissau','1'),(82,'GY','Guyana','Guyana','1'),(83,'GF','Guyane française','Guiana, French','1'),(84,'HT','Haïti','Haiti','1'),(85,'HN','Honduras','Honduras','1'),(86,'HK','Hong Kong','Hong Kong, (China)','1'),(87,'HU','Hongrie','Hungary','1'),(88,'BV','Ile Bouvet','Bouvet Island','1'),(89,'CX','Ile Christmas','Christmas Island','1'),(90,'NF','Ile Norfolk','Norfolk Island','1'),(91,'KY','Iles Cayman','Cayman Islands','1'),(92,'CK','Iles Cook','Cook Islands','1'),(93,'FO','Iles Féroé','Faroe Islands','1'),(94,'FK','Iles Falkland','Falkland Islands (Malvinas)','1'),(95,'FJ','Iles Fidji','Fiji','1'),(96,'GS','Iles Géorgie du Sud et Sandwich du Sud','S. Georgia and S. Sandwich Is.','1'),(97,'HM','Iles Heard et McDonald','Heard and McDonald Islands','1'),(98,'MH','Iles Marshall','Marshall Islands','1'),(99,'PN','Iles Pitcairn','Pitcairn Island','1'),(100,'SB','Iles Salomon','Solomon Islands','1'),(101,'SJ','Iles Svalbard et Jan Mayen','Svalbard and Jan Mayen Islands','1'),(102,'TC','Iles Turks-et-Caicos','Turks and Caicos Islands','1'),(103,'VI','Iles Vierges américaines','Virgin Islands, U.S.','1'),(104,'VG','Iles Vierges britanniques','Virgin Islands, British','1'),(105,'CC','Iles des Cocos (Keeling)','Cocos (Keeling) Islands','1'),(106,'UM','Iles mineures éloignées des États-Unis','US Minor Outlying Islands','1'),(107,'IN','Inde','India','1'),(108,'ID','Indonésie','Indonesia','1'),(109,'IR','Iran','Iran, Islamic Republic of','1'),(110,'IQ','Iraq','Iraq','1'),(111,'IE','Irlande','Ireland','1'),(112,'IS','Islande','Iceland','1'),(113,'IL','Israël','Israel','1'),(114,'IT','Italie','Italy','1'),(115,'JM','Jamaïque','Jamaica','1'),(116,'JP','Japon','Japan','1'),(117,'JO','Jordanie','Jordan','1'),(118,'KZ','Kazakhstan','Kazakhstan','1'),(119,'KE','Kenya','Kenya','1'),(120,'KG','Kirghizistan','Kyrgyzstan','1'),(121,'KI','Kiribati','Kiribati','1'),(122,'KW','Koweït','Kuwait','1'),(123,'LA','Laos','Lao People\'s Democratic Republic','1'),(124,'LS','Lesotho','Lesotho','1'),(125,'LV','Lettonie','Latvia','1'),(126,'LB','Liban','Lebanon','1'),(127,'LR','Liberia','Liberia','1'),(128,'LY','Libye','Libyan Arab Jamahiriya','1'),(129,'LI','Liechtenstein','Liechtenstein','1'),(130,'LT','Lituanie','Lithuania','1'),(131,'LU','Luxembourg','Luxembourg','1'),(132,'MO','Macao','Macao, (China)','1'),(133,'MG','Madagascar','Madagascar','1'),(134,'MY','Malaisie','Malaysia','1'),(135,'MW','Malawi','Malawi','1'),(136,'MV','Maldives','Maldives','1'),(137,'ML','Mali','Mali','1'),(138,'MT','Malte','Malta','1'),(139,'MP','Mariannes du Nord','Northern Mariana Islands','1'),(140,'MA','Maroc','Morocco','1'),(141,'MQ','Martinique','Martinique','1'),(142,'MU','Maurice','Mauritius','1'),(143,'MR','Mauritanie','Mauritania','1'),(144,'YT','Mayotte','Mayotte','1'),(145,'MX','Mexique','Mexico','1'),(146,'FM','Micronésie','Micronesia, Federated States of','1'),(147,'MD','Moldavie','Moldova, Republic of','1'),(148,'MC','Monaco','Monaco','1'),(149,'MN','Mongolie','Mongolia','1'),(150,'MS','Montserrat','Montserrat','1'),(151,'MZ','Mozambique','Mozambique','1'),(152,'NP','Népal','Nepal','1'),(153,'NA','Namibie','Namibia','1'),(154,'NR','Nauru','Nauru','1'),(155,'NI','Nicaragua','Nicaragua','1'),(156,'NE','Niger','Niger','1'),(157,'NG','Nigeria','Nigeria','1'),(158,'NU','Nioué','Niue','1'),(159,'NO','Norvège','Norway','1'),(160,'NC','Nouvelle-Calédonie','New Caledonia','1'),(161,'NZ','Nouvelle-Zélande','New Zealand','1'),(162,'OM','Oman','Oman','1'),(163,'UG','Ouganda','Uganda','1'),(164,'UZ','Ouzbékistan','Uzbekistan','1'),(165,'PE','Pérou','Peru','1'),(166,'PK','Pakistan','Pakistan','1'),(167,'PA','Panama','Panama','1'),(168,'PG','Papouasie-Nouvelle-Guinée','Papua New Guinea','1'),(169,'PY','Paraguay','Paraguay','1'),(170,'NL','Pays-Bas','Netherlands','1'),(171,'PH','Philippines','Philippines','1'),(172,'PL','Pologne','Poland','1'),(173,'PF','Polynésie française','French Polynesia','1'),(174,'PR','Porto Rico','Puerto Rico','1'),(175,'PT','Portugal','Portugal','1'),(176,'QA','Qatar','Qatar','1'),(177,'CF','République centrafricaine','Central African Republic','1'),(178,'CD','République démocratique du Congo','Congo, Democratic Rep. of the','1'),(179,'DO','République dominicaine','Dominican Republic','1'),(180,'CZ','République tchèque','Czech Republic','1'),(181,'RE','Réunion','Reunion','1'),(182,'RO','Roumanie','Romania','1'),(183,'GB','Royaume-Uni','Saint Pierre and Miquelon','1'),(184,'RU','Russie','Russia (Russian Federation)','1'),(185,'RW','Rwanda','Rwanda','1'),(186,'SN','Sénégal','Senegal','1'),(187,'EH','Sahara occidental','Western Sahara','1'),(188,'KN','Saint-Christophe-et-Niévès','Saint Kitts and Nevis','1'),(189,'SM','Saint-Marin','San Marino','1'),(190,'PM','Saint-Pierre-et-Miquelon','Saint Pierre and Miquelon','1'),(191,'VA','Saint-Siège ','Vatican City State (Holy See)','1'),(192,'VC','Saint-Vincent-et-les-Grenadines','Saint Vincent and the Grenadines','1'),(193,'SH','Sainte-Hélène','Saint Helena','1'),(194,'LC','Sainte-Lucie','Saint Lucia','1'),(195,'SV','Salvador','El Salvador','1'),(196,'WS','Samoa','Samoa','1'),(197,'AS','Samoa américaines','American Samoa','1'),(198,'ST','Sao Tomé-et-Principe','Sao Tome and Principe','1'),(199,'SC','Seychelles','Seychelles','1'),(200,'SL','Sierra Leone','Sierra Leone','1'),(201,'SG','Singapour','Singapore','1'),(202,'SI','Slovénie','Slovenia','1'),(203,'SK','Slovaquie','Slovakia','1'),(204,'SO','Somalie','Somalia','1'),(205,'SD','Soudan','Sudan','1'),(206,'LK','Sri Lanka','Sri Lanka (ex-Ceilan)','1'),(207,'SE','Suède','Sweden','1'),(208,'CH','Suisse','Switzerland','1'),(209,'SR','Suriname','Suriname','1'),(210,'SZ','Swaziland','Swaziland','1'),(211,'SY','Syrie','Syrian Arab Republic','1'),(212,'TW','Taïwan','Taiwan','1'),(213,'TJ','Tadjikistan','Tajikistan','1'),(214,'TZ','Tanzanie','Tanzania, United Republic of','1'),(215,'TD','Tchad','Chad','1'),(216,'TF','Terres australes françaises','French Southern Territories - TF','1'),(217,'IO','Territoire britannique de l\'Océan Indien','British Indian Ocean Territory','1'),(218,'TH','Thaïlande','Thailand','1'),(219,'TL','Timor Oriental','Timor-Leste (East Timor)','1'),(220,'TG','Togo','Togo','1'),(221,'TK','Tokélaou','Tokelau','1'),(222,'TO','Tonga','Tonga','1'),(223,'TT','Trinité-et-Tobago','Trinidad & Tobago','1'),(224,'TN','Tunisie','Tunisia','1'),(225,'TM','Turkménistan','Turkmenistan','1'),(226,'TR','Turquie','Turkey','1'),(227,'TV','Tuvalu','Tuvalu','1'),(228,'UA','Ukraine','Ukraine','1'),(229,'UY','Uruguay','Uruguay','1'),(230,'VU','Vanuatu','Vanuatu','1'),(231,'VE','Venezuela','Venezuela','1'),(232,'VN','ViÃªt Nam','Viet Nam','1'),(233,'WF','Wallis-et-Futuna','Wallis and Futuna','1'),(234,'YE','Yémen','Yemen','1'),(235,'YU','Yougoslavie','Saint Pierre and Miquelon','1'),(236,'ZM','Zambie','Zambia','1'),(237,'ZW','Zimbabwe','Zimbabwe','1'),(238,'MK','ex-République yougoslave de Macédoine','Macedonia, TFYR','1'),(239,'abd','abdoulaye','abdulah','-1');
/*!40000 ALTER TABLE `pays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programmes`
--

DROP TABLE IF EXISTS `programmes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programmes` (
  `id_programme` int(11) NOT NULL AUTO_INCREMENT,
  `code_programme` varchar(10) NOT NULL,
  `code_section` int(11) NOT NULL,
  `code_type_serie` int(11) NOT NULL,
  `libelle_programme` varchar(255) NOT NULL,
  `etat_programme` int(1) NOT NULL,
  PRIMARY KEY (`id_programme`),
  KEY `epad_programmes_section` (`code_section`),
  KEY `epad_programmes_serie` (`code_type_serie`),
  CONSTRAINT `programmes_ibfk_1` FOREIGN KEY (`code_section`) REFERENCES `param_section` (`code_section`),
  CONSTRAINT `programmes_ibfk_2` FOREIGN KEY (`code_type_serie`) REFERENCES `param_type_serie` (`code_type_serie`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programmes`
--

LOCK TABLES `programmes` WRITE;
/*!40000 ALTER TABLE `programmes` DISABLE KEYS */;
INSERT INTO `programmes` VALUES (1,'5',3,1,'dev',1);
/*!40000 ALTER TABLE `programmes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialite_matiere`
--

DROP TABLE IF EXISTS `specialite_matiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialite_matiere` (
  `code_spm` int(11) NOT NULL AUTO_INCREMENT,
  `code_specialite` int(11) NOT NULL,
  `id_matiere` int(11) NOT NULL,
  `etat_matiere` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`code_spm`),
  KEY `id_materiel` (`id_matiere`),
  KEY `code_specialite` (`code_specialite`),
  CONSTRAINT `specialite_matiere_ibfk_1` FOREIGN KEY (`code_specialite`) REFERENCES `param_specialite` (`code_specialite`),
  CONSTRAINT `specialite_matiere_ibfk_2` FOREIGN KEY (`id_matiere`) REFERENCES `param_matiere` (`id_matiere`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialite_matiere`
--

LOCK TABLES `specialite_matiere` WRITE;
/*!40000 ALTER TABLE `specialite_matiere` DISABLE KEYS */;
/*!40000 ALTER TABLE `specialite_matiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statut_juridique`
--

DROP TABLE IF EXISTS `statut_juridique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statut_juridique` (
  `code_sj` int(100) NOT NULL AUTO_INCREMENT,
  `libelle_sj` varchar(255) DEFAULT NULL,
  `etat_sj` enum('-1','1') NOT NULL DEFAULT '1',
  `description_sj` varchar(255) NOT NULL,
  PRIMARY KEY (`code_sj`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statut_juridique`
--

LOCK TABLES `statut_juridique` WRITE;
/*!40000 ALTER TABLE `statut_juridique` DISABLE KEYS */;
INSERT INTO `statut_juridique` VALUES (1,'Individuel','1',''),(2,'GIE','1',''),(3,'ONG','1',''),(4,'Association','1',''),(5,'Evangelique','1',''),(6,'SARL','1',''),(7,'SUARL','1',''),(8,'Autre','1',''),(9,'test','-1',' description d\'un nouveau statut juridique\r\n');
/*!40000 ALTER TABLE `statut_juridique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statut_juridique_structure`
--

DROP TABLE IF EXISTS `statut_juridique_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statut_juridique_structure` (
  `code_sj` int(100) NOT NULL,
  `code_str` int(150) NOT NULL,
  `date_sjs` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code_sj`,`code_str`),
  KEY `statut_juridique_has_structure_FKIndex1` (`code_sj`),
  KEY `statut_juridique_has_structure_FKIndex2` (`code_str`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statut_juridique_structure`
--

LOCK TABLES `statut_juridique_structure` WRITE;
/*!40000 ALTER TABLE `statut_juridique_structure` DISABLE KEYS */;
INSERT INTO `statut_juridique_structure` VALUES (1,7,NULL),(8,6,NULL),(8,8,NULL),(8,9,NULL),(8,10,NULL),(8,11,NULL),(8,12,NULL),(8,13,NULL),(8,14,NULL),(8,15,NULL),(8,16,NULL),(8,17,NULL),(8,18,NULL),(8,19,NULL),(8,20,NULL),(8,21,NULL),(8,22,NULL),(8,23,NULL);
/*!40000 ALTER TABLE `statut_juridique_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structure`
--

DROP TABLE IF EXISTS `structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structure` (
  `code_str` int(150) NOT NULL,
  `libelle_structure` varchar(10) NOT NULL,
  `date_creation_str` varchar(250) NOT NULL,
  `email_str` varchar(255) NOT NULL,
  `tel_str` char(20) NOT NULL,
  `etat_str` char(5) NOT NULL,
  `date_fermeture` varchar(45) NOT NULL,
  `arrete_ferme` varchar(255) NOT NULL,
  `id_saisie` int(11) NOT NULL,
  `date_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`code_str`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structure`
--

LOCK TABLES `structure` WRITE;
/*!40000 ALTER TABLE `structure` DISABLE KEYS */;
INSERT INTO `structure` VALUES (1,'SIMEN','','','','1','','',0,'2017-09-12 19:51:24');
/*!40000 ALTER TABLE `structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `etat` enum('-1','1') NOT NULL DEFAULT '1',
  `rang` int(11) NOT NULL DEFAULT '1000',
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1,'DASH','Dashbaord','1',1000),(2,'PARAM','Parametrage','1',1000),(3,'MENET','Menu Etablissement','1',1000),(4,'SECURITE','SECURITE','1',1000);
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_niits`
--

DROP TABLE IF EXISTS `sys_niits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_niits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ien` varchar(20) NOT NULL,
  `email` varchar(35) DEFAULT NULL,
  `id_profil` int(10) unsigned NOT NULL,
  `password` varchar(45) DEFAULT 'Azertyui',
  `code_str` bigint(20) NOT NULL,
  `statut` enum('attente','actif','resilie','supprime') NOT NULL DEFAULT 'attente',
  `date_last_modif` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `code_str` (`code_str`),
  KEY `id_profil` (`id_profil`),
  CONSTRAINT `sys_niits_ibfk_1` FOREIGN KEY (`id_profil`) REFERENCES `sys_type_profil` (`id_type_profil`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_niits`
--

LOCK TABLES `sys_niits` WRITE;
/*!40000 ALTER TABLE `sys_niits` DISABLE KEYS */;
INSERT INTO `sys_niits` VALUES (1,'123456','12345@education.sn',1,'passer',1,'attente','2017-09-06 17:30:53'),(2,'P000088','P000088@education.sn',1,'passer',2,'attente','2017-09-06 17:30:53'),(3,'P000530','simen@education.sn',2,'passer',1,'attente','2017-09-06 22:30:53');
/*!40000 ALTER TABLE `sys_niits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_sous_menu`
--

DROP TABLE IF EXISTS `sys_sous_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_sous_menu` (
  `id_sous_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `etat` enum('-1','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_sous_menu`),
  KEY `id_menu` (`id_menu`),
  CONSTRAINT `sys_sous_menu_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `sys_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_sous_menu`
--

LOCK TABLES `sys_sous_menu` WRITE;
/*!40000 ALTER TABLE `sys_sous_menu` DISABLE KEYS */;
INSERT INTO `sys_sous_menu` VALUES (1,4,'USR','Utilisateur','1'),(2,4,'MENU','Menu','1'),(3,4,'LST_MENU','Liste Menu','1'),(4,4,'LST_S_MENU','Liste sous menus','1'),(5,4,'PROFIL','profil','1'),(6,2,'FCT','Fonctions','1'),(7,2,'CSTR','Categorie de structure','1'),(8,2,'SPEC','Specialite','1'),(9,2,'ANS','Année scolaire','1'),(10,2,'DISC','Discipline','1'),(11,2,'MAT','MAtière','1'),(12,2,'CATEVAL','Catégorie Evaluation','1'),(13,2,'TYPEEVAL','Type Evaluation','1'),(14,2,'TYPEPER','Type Période','1'),(15,2,'CYCLE','CYCLE','1'),(16,2,'CYCLE_EVAL','Cycle Evaluation','1'),(17,2,'CORPS','CORPS','1'),(18,2,'GRADE','GRADE','1'),(19,2,'SECTION','SECTION','1'),(20,2,'DIPAC','Diplôme Académique','1'),(21,2,'DIPPRO','Diplôme Professionnel','1'),(22,2,'ORGPAT','Organisation Patronale','1'),(23,2,'PAYS','PAYS','1'),(24,2,'STATJUR','Statut Juridique','1'),(25,2,'STRUCTURE','STRUCTURE','1'),(26,2,'LANGUEMATER','Langue Maternelle','1'),(27,2,'ORPHELIN','ORPHELIN','1'),(28,2,'REGROUPEMENT','REGROUPEMENT','1'),(29,2,'ACTE','ACTE','1'),(30,2,'GROUPESANGUIN','Groupe Sanguin','1'),(31,2,'HANDICAP','Handicape','1'),(32,2,'PROG','Programmes','1');
/*!40000 ALTER TABLE `sys_sous_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_type_action`
--

DROP TABLE IF EXISTS `sys_type_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_type_action` (
  `id_actions` int(11) NOT NULL AUTO_INCREMENT,
  `id_type_profil` int(11) unsigned NOT NULL,
  `id_sous_menu` int(11) NOT NULL,
  `d_read` enum('-1','1') NOT NULL DEFAULT '-1',
  `d_add` enum('-1','1') NOT NULL DEFAULT '-1',
  `d_upd` enum('-1','1') NOT NULL DEFAULT '-1',
  `d_del` enum('-1','1') NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id_actions`),
  KEY `id_sous_menu` (`id_sous_menu`),
  KEY `id_type_profil` (`id_type_profil`),
  KEY `id_type_profil_2` (`id_type_profil`),
  KEY `id_sous_menu_2` (`id_sous_menu`),
  CONSTRAINT `sys_type_action_ibfk_1` FOREIGN KEY (`id_type_profil`) REFERENCES `sys_type_profil` (`id_type_profil`),
  CONSTRAINT `sys_type_action_ibfk_2` FOREIGN KEY (`id_sous_menu`) REFERENCES `sys_sous_menu` (`id_sous_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_type_action`
--

LOCK TABLES `sys_type_action` WRITE;
/*!40000 ALTER TABLE `sys_type_action` DISABLE KEYS */;
INSERT INTO `sys_type_action` VALUES (6,1,3,'1','1','1','1'),(7,1,4,'1','1','1','1'),(8,1,5,'1','1','1','1'),(9,1,1,'1','1','1','1'),(10,1,2,'1','1','1','1'),(11,1,6,'1','1','1','1'),(12,1,7,'1','1','1','1'),(13,1,8,'1','1','1','1'),(14,1,9,'1','1','1','1'),(15,1,10,'1','1','1','1'),(16,1,11,'1','1','1','1'),(17,1,12,'1','1','1','1'),(18,1,13,'1','1','1','1'),(19,1,14,'1','1','1','1'),(20,1,15,'1','1','1','1'),(21,1,16,'1','1','1','1'),(22,1,17,'1','1','1','1'),(23,1,18,'1','1','1','1'),(24,1,19,'1','1','1','1'),(25,1,20,'1','1','1','1'),(26,1,21,'1','1','1','1'),(27,1,22,'1','1','1','1'),(28,1,23,'1','1','1','1'),(29,1,24,'1','1','1','1'),(30,1,25,'1','1','1','1'),(31,1,26,'1','1','1','1'),(32,1,27,'1','1','1','1'),(33,1,28,'1','1','1','1'),(34,1,29,'1','1','1','1'),(35,1,30,'1','1','1','1'),(36,1,31,'1','1','1','1'),(37,2,6,'1','-1','-1','-1'),(38,2,7,'1','-1','-1','-1'),(39,2,8,'1','-1','-1','-1'),(40,2,9,'1','-1','-1','-1'),(41,2,10,'1','-1','-1','-1'),(42,2,11,'1','-1','-1','-1'),(43,2,12,'1','-1','-1','-1'),(44,2,13,'1','-1','-1','-1'),(45,2,14,'1','-1','-1','-1'),(46,2,15,'1','-1','-1','-1'),(47,2,16,'1','-1','-1','-1'),(48,2,17,'1','-1','-1','-1'),(49,2,18,'1','-1','-1','-1'),(50,2,19,'1','-1','-1','-1'),(51,2,20,'1','-1','-1','-1'),(52,2,21,'1','-1','-1','-1'),(53,2,22,'1','-1','-1','-1'),(54,2,23,'1','-1','-1','-1'),(55,2,24,'1','-1','-1','-1'),(56,2,25,'1','-1','-1','-1'),(57,2,26,'1','-1','-1','-1'),(58,2,27,'1','-1','-1','-1'),(59,2,28,'1','-1','-1','-1'),(60,2,29,'1','-1','-1','-1'),(61,2,30,'1','-1','-1','-1'),(62,2,31,'1','-1','-1','-1'),(63,1,32,'1','1','1','1');
/*!40000 ALTER TABLE `sys_type_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_type_profil`
--

DROP TABLE IF EXISTS `sys_type_profil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_type_profil` (
  `id_type_profil` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelle_type_profil` varchar(250) NOT NULL,
  `etat` char(2) DEFAULT NULL,
  PRIMARY KEY (`id_type_profil`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_type_profil`
--

LOCK TABLES `sys_type_profil` WRITE;
/*!40000 ALTER TABLE `sys_type_profil` DISABLE KEYS */;
INSERT INTO `sys_type_profil` VALUES (1,'Administrateur','1'),(2,'Superviseur','1');
/*!40000 ALTER TABLE `sys_type_profil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_acte`
--

DROP TABLE IF EXISTS `type_acte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_acte` (
  `id_type_acte` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_type_acte` varchar(250) NOT NULL,
  `etat_type_acte` enum('1','-1') NOT NULL DEFAULT '1',
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id_type_acte`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_acte`
--

LOCK TABLES `type_acte` WRITE;
/*!40000 ALTER TABLE `type_acte` DISABLE KEYS */;
INSERT INTO `type_acte` VALUES (1,'Attestation de Service','1',''),(2,'Certification de Prise de service','1',''),(3,'Services effectues','1',''),(4,'test acte','-1','test de l\'ajout des actes');
/*!40000 ALTER TABLE `type_acte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_evaluation`
--

DROP TABLE IF EXISTS `type_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_evaluation` (
  `code_type_evaluation` int(11) NOT NULL AUTO_INCREMENT,
  `type_evaluation` varchar(250) CHARACTER SET utf8 NOT NULL,
  `etat_etype_evaluation` enum('1','-1') CHARACTER SET utf8 NOT NULL DEFAULT '1',
  PRIMARY KEY (`code_type_evaluation`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_evaluation`
--

LOCK TABLES `type_evaluation` WRITE;
/*!40000 ALTER TABLE `type_evaluation` DISABLE KEYS */;
INSERT INTO `type_evaluation` VALUES (1,'Composition','1'),(2,'Essai','1'),(3,'Devoir a faire la maison','1'),(4,'test','-1');
/*!40000 ALTER TABLE `type_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_groupesanguin`
--

DROP TABLE IF EXISTS `type_groupesanguin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_groupesanguin` (
  `id_gsanguin` int(11) NOT NULL,
  `libelle_gsanguin` varchar(3) NOT NULL,
  `etat_gs` enum('-1','1') NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id_gsanguin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_groupesanguin`
--

LOCK TABLES `type_groupesanguin` WRITE;
/*!40000 ALTER TABLE `type_groupesanguin` DISABLE KEYS */;
INSERT INTO `type_groupesanguin` VALUES (0,'Inc','1',''),(1,'O+','1','Donneur universel'),(2,'O-','1',''),(3,'A+','1','La majorité des sénégalais'),(4,'A-','1',''),(5,'B+','1',''),(6,'B-','1',''),(7,'AB+','1',''),(8,'AB-','1','receveur universel');
/*!40000 ALTER TABLE `type_groupesanguin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_handicap`
--

DROP TABLE IF EXISTS `type_handicap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_handicap` (
  `id_handicap` int(11) NOT NULL,
  `libelle_handicap` varchar(100) NOT NULL,
  `etat_handicap` enum('-1','1') CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `description` varchar(250) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_handicap`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_handicap`
--

LOCK TABLES `type_handicap` WRITE;
/*!40000 ALTER TABLE `type_handicap` DISABLE KEYS */;
INSERT INTO `type_handicap` VALUES (0,'Aucun','1',''),(5,'Moteur','1',''),(1,'Visuel','1',''),(2,'Parole','1',''),(3,'Auditif','1',''),(4,'Mental','1',''),(6,'auditif et de la parole','1',''),(7,'Autre','1','');
/*!40000 ALTER TABLE `type_handicap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_languematernelle`
--

DROP TABLE IF EXISTS `type_languematernelle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_languematernelle` (
  `id_languemater` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_languemater` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `etat_languemater` enum('-1','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_languemater`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_languematernelle`
--

LOCK TABLES `type_languematernelle` WRITE;
/*!40000 ALTER TABLE `type_languematernelle` DISABLE KEYS */;
INSERT INTO `type_languematernelle` VALUES (-99,'Inconnu','','1'),(1,'Wolof','','1'),(2,'cv','','1'),(3,'Pulhar','Langue des peulh','-1');
/*!40000 ALTER TABLE `type_languematernelle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_orphelin`
--

DROP TABLE IF EXISTS `type_orphelin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_orphelin` (
  `id_orphelin` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_orphelin` varchar(100) NOT NULL,
  `etat_orphelin` enum('-1','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_orphelin`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_orphelin`
--

LOCK TABLES `type_orphelin` WRITE;
/*!40000 ALTER TABLE `type_orphelin` DISABLE KEYS */;
INSERT INTO `type_orphelin` VALUES (0,'','-1'),(1,'De père','1'),(2,'De mère','1'),(3,'De père et de mère','1'),(4,'ksk','1'),(5,'bkkki','1'),(6,'test','1');
/*!40000 ALTER TABLE `type_orphelin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_periode`
--

DROP TABLE IF EXISTS `type_periode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_periode` (
  `code_type_periode` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_periode` varchar(250) NOT NULL,
  `etat_periode` enum('-1','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`code_type_periode`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_periode`
--

LOCK TABLES `type_periode` WRITE;
/*!40000 ALTER TABLE `type_periode` DISABLE KEYS */;
INSERT INTO `type_periode` VALUES (1,'Premier semestre','1'),(2,'Second Semestre','1'),(3,'1er composition','1'),(4,'2eme Composition','1'),(5,'3eme Composition','1'),(6,'test','-1');
/*!40000 ALTER TABLE `type_periode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_regroupement`
--

DROP TABLE IF EXISTS `type_regroupement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_regroupement` (
  `id_regroupement` int(11) NOT NULL AUTO_INCREMENT,
  `regroupement` varchar(250) DEFAULT NULL,
  `ordre` int(1) DEFAULT NULL,
  `statut_type_regroupement` enum('-1','1') NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id_regroupement`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_regroupement`
--

LOCK TABLES `type_regroupement` WRITE;
/*!40000 ALTER TABLE `type_regroupement` DISABLE KEYS */;
INSERT INTO `type_regroupement` VALUES (1,'IA',1,'-1'),(2,'IEF',2,'-1'),(3,'District',3,'-1'),(4,'iddsj',NULL,'-1'),(5,'5',NULL,'-1'),(6,'test',5,'1');
/*!40000 ALTER TABLE `type_regroupement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types_fonctions`
--

DROP TABLE IF EXISTS `types_fonctions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `types_fonctions` (
  `id_fonction_categorie` int(11) NOT NULL AUTO_INCREMENT,
  `code_structure` int(11) NOT NULL,
  `id_fonction` int(11) NOT NULL,
  `etat_fonction_categorie` enum('1','-1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_fonction_categorie`),
  UNIQUE KEY `code_structure_2` (`code_structure`,`id_fonction`),
  KEY `code_structure` (`code_structure`),
  KEY `id_fonction` (`id_fonction`),
  CONSTRAINT `types_fonctions_ibfk_1` FOREIGN KEY (`id_fonction`) REFERENCES `param_fonction` (`code_fonction`),
  CONSTRAINT `types_fonctions_ibfk_2` FOREIGN KEY (`code_structure`) REFERENCES `categorie_structure` (`code_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types_fonctions`
--

LOCK TABLES `types_fonctions` WRITE;
/*!40000 ALTER TABLE `types_fonctions` DISABLE KEYS */;
INSERT INTO `types_fonctions` VALUES (1,1,3,'-1'),(2,3,3,'1'),(3,3,6,'-1'),(4,1,6,'1');
/*!40000 ALTER TABLE `types_fonctions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-18  9:10:17
