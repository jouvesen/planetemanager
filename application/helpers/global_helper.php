<?php
//
//function btn_add_action(){
//    echo '<div class="row">
//                <div class="col-sm-12" style="margin-bottom: 30px">
//                    <button type="button" id="btn_add" class="btn btn-primary" title="Ajouter">Ajouter <span lass="m-l-5"><i
//                                class="fa fa-plus-square"></i></span></button>
//                </div>
//            </div>';
//}
//
//
//function btn_edit_action($id){
//    echo '<a href="#" class="on-default btn_edit" title="Modifier"
//          id="'.$id.'"><i class="fa fa-pencil"></i></a>&nbsp;';
//}


function btn_edit_double_action($id)
{
    foreach ($id as $d) {
        echo '<a href="#" class="on-default btn_edit" title="Modifier"
          id="' . $d . '"><i class="fa fa-pencil"></i></a>&nbsp;';
    }

}


//
//function btn_delete_action($id){
//    echo '<a href="#" class="on-default btn_delete" title="Supprimer"
//          id="'.$id.'"><i class="fa fa-trash-o" style="color:red"></i></a>&nbsp;';
//}


function btn_show_action($id){
    echo '<a href="#" class="on-default btn_show" title="Visualiser"
           id="'.$id.'"><i class="fa fa-eye" style="color:#CCCCCC"></i></a>';
}


function format_date($value){

    if($value == NULL)
        return '';
    else
        return date('d/m/Y', strtotime($value));

}

function get_api_base_url($relative_url)
{
    return "http://api.simendev.com/$relative_url";
}

/*
* @$table: 		Tableau dans lequel on fait la recherche
* @$to_find: 	Paramètre de recherche
* @$colonne:  	Colonne sur le sous tableaux
* @$cle:		La colonne du tableau associatif
*/

function multi_array_search($table, $to_find, $colonne, $cle)
{
    $val = $table[array_search($to_find, array_column($table, $colonne))][$cle];
    return $val;
}