<?php

function btn_add_action($smenu_code = '')
{
    $CI = &get_instance();
    $tab_smrole = $CI->session->smenu_roles;

    if (isset($tab_smrole[$smenu_code]['d_add'])) {
        echo '<div class="row">
                <div class="col-sm-12" style="margin-bottom: 30px">
                    <button type="button" id="btn_add" class="btn btn-primary">Ajouter <span lass="m-l-5"><i
                                class="fa fa-plus-square"></i></span></button>
                </div>
            </div>';
    }
}

function btn_detail_action()
{

    echo '<div class="row">
        <div class="col-sm-12" style="margin-bottom: 30px">
            <button type="button" id="btn_add" class="btn btn-primary">Valider mes infos</button>
        </div>
    </div>';

}

function btn_edit_action($id = '', $smenu_code = '')
{
    $CI = &get_instance();
    $tab_smrole = $CI->session->smenu_roles;
    if (isset($tab_smrole[$smenu_code]['d_upd'])) {
        echo '<a href="#" class="on-default btn_edit"
                 id="' . $id . '"><i class="fa fa-pencil"></i></a>&nbsp;';
    }
}


function btn_delete_action($id = '', $smenu_code = '')
{
    $CI = &get_instance();
    $tab_smrole = $CI->session->smenu_roles;
    if (isset($tab_smrole[$smenu_code]['d_del'])) {
        echo '<a href="#" class="on-default btn_delete"  id="' . $id . '"><i class="fa fa-trash-o" style="color:red"></i></a>&nbsp;';
    }
}


//    function btn_show_action($id='', $smenu_code='')
//    {
//        $CI = &get_instance();
//        $tab_smrole = $CI->session->smenu_roles;
//        if(isset($tab_smrole[$smenu_code]['d_read']))
//        {
//            echo '<a href="#" class="on-default btn_show" id="'.$id.'"><i class="fa fa-eye" style="color:#0080FF
//            "></i></a>';
//        }
//    }
//    if (isset($tab_smrole)) unset($tab_smrole);