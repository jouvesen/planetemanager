<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_manager
{


    public function create_url($action, $tableau, $sep)
    {
        $base_url = "http://api.simendev.com/";
        $url = $action . "?";
        foreach ($tableau as $param => $value) {

            $url = $url . "$param=$value" . $sep;
        }
        return $base_url . $url;
    }

}